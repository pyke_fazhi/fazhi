package com.zgn.fazhi.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 开启事务
 * 扫描mapper包
 */
@EnableTransactionManagement
@MapperScan("com.zgn.fazhi.mapper")
@Configuration
public class MybatisPlusConfig {



}
