package com.zgn.fazhi.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Data
@Configuration
@PropertySource(value = {"classpath:application.properties"})
@ConfigurationProperties(prefix = "aliyunlive")
public class AliYunLiveConfig {

    /**
     * 推流域名
     */
    private String pushDomain;

    /**
     * 拉流域名
     */
    private String pullDomain;

    /**
     * 直播测试appName
     */
    private String appName;

    /**
     * 直播测试streamName{直播类型}_{类型id}
     */
    private String streamName;

    /**
     * 推流鉴权url key
     */
    private String pushIdentKey;

    /**
     * 拉流鉴权url key
     */
    private String pullIdentKey;

    /**
     * 鉴权url的有效时间（秒），默认30分钟，1800秒 key
     */
    private Integer identUrlValidTime;

    /**
     * OSS-区域代码
     */
    private String regionId;

    /**
     * OSS-RAM 访问控制-人员管理-用户 AccessKey
     */
    private String accessKeyId;

    /**
     * OSS-RAM 访问控制-人员管理-用户 secret
     */
    private String secret;

}
