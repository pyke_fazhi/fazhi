package com.zgn.fazhi.realm;

import java.util.HashSet;
import java.util.Set;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.jwt.JwtToken;
import com.zgn.fazhi.mapper.RoleMapper;
import com.zgn.fazhi.mapper.UserInfoMapper;
import com.zgn.fazhi.pojo.po.Role;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.service.RoleService;
import com.zgn.fazhi.service.UserInfoService;
import com.zgn.fazhi.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * JwtRealm 只负责校验 JwtToken
 */
@Slf4j
public class JwtRealm extends AuthorizingRealm {

    @Autowired
    private RoleService roleService;

    /**
     * 限定这个 Realm 只处理我们自定义的 JwtToken
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }

    /**
     * 此处的 SimpleAuthenticationInfo 可返回任意值，密码校验时不会用到它
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken)
            throws AuthenticationException {
        log.debug("jwt认证");
        try {
            return new SimpleAuthenticationInfo(((JwtToken) authcToken).getPrincipal(), "jwtToken", getName());
        } catch (NullPointerException e) {
            throw new AccountException("JWT token为空");
        }
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        // 通过用户id获取该角色名称
        // 获取当前用户
        // 查询数据库，获取用户的角色信息
        Set<String> roles = new HashSet<>();
        log.debug("通过用户角色id查询角色名称");
        String roleName = roleService.findRnameByUid(Integer.parseInt(principals.getPrimaryPrincipal().toString()));
        log.debug("当前角色 : " + roleName);
        roles.add(roleName);
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setRoles(roles);
        return info;
    }

}