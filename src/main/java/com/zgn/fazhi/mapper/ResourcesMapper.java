package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.Resources;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourcesMapper extends BaseMapper<Resources> {
}
