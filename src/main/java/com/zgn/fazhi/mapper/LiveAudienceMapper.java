package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.LiveAudience;
import org.springframework.stereotype.Repository;

/**
 * @author ydw
 * @version 1.0
 * @Description TODO
 * @date 21/08/2021 8:36 PM
 */

@Repository
public interface LiveAudienceMapper extends BaseMapper<LiveAudience> {
}
