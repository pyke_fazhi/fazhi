package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.AppointmentConfig;
import org.springframework.stereotype.Repository;

/**
 * @author ydw
 */
@Repository
public interface AppointmentConfigMapper extends BaseMapper<AppointmentConfig> {
}
