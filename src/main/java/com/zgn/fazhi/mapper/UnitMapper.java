package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.Unit;
import org.springframework.stereotype.Repository;


@Repository
public interface UnitMapper extends BaseMapper<Unit> {
}
