package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.LiveInfo;
import com.zgn.fazhi.pojo.vo.UserLiveInfo;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ydw
 * @version 1.0
 * @Description TODO
 * @date 17/08/2021 9:18 PM
 */

@Repository
public interface LiveInfoMapper extends BaseMapper<LiveInfo> {

    /**
     * 查询所有直播间
     *
     * @return 所有直播间信息
     */
    @Select("SELECT l.id, u.name, title, info, if_live, screenshot " +
            "FROM live_info AS l INNER JOIN user_info AS u ON l.uid = u.id")
    List<UserLiveInfo> getAllLive();

}
