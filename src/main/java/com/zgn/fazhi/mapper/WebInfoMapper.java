package com.zgn.fazhi.mapper;

import com.zgn.fazhi.pojo.po.WebInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Repository
public interface WebInfoMapper extends BaseMapper<WebInfo> {

}
