package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.School;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ydw
 */
@Repository
public interface SchoolMapper extends BaseMapper<School> {

    /**
     * 通过用户id查询学校名
     * @param uid 用户id
     * @return 学校名
     */
    @Select("SELECT school_name FROM user_info AS u " +
            "INNER JOIN school AS s ON u.school_id = s.id " +
            "WHERE u.id = #{uid}")
    String getByUid(@Param("uid") Integer uid);

}
