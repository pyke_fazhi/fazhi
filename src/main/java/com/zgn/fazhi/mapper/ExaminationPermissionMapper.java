package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.ExaminationPermission;
import com.zgn.fazhi.pojo.vo.ExaPermissionVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ydw
 */
@Repository
public interface ExaminationPermissionMapper extends BaseMapper<ExaminationPermission> {

    @Select("SELECT p.id, record_id, p.school_id, school_name, p.grade_id, grade_name, p.class_id, class_name " +
            "FROM examination_permission AS p INNER JOIN school AS s ON p.school_id = s.id " +
            "INNER JOIN user_grade AS g ON p.grade_id = g.id " +
            "INNER JOIN class_info AS c ON p.class_id = c.id " +
            "WHERE p.record_id = #{rid}")
    List<ExaPermissionVo> selectVoList(@Param("rid") Integer rid);

    @Select("SELECT p.id, record_id, p.school_id, school_name, p.grade_id, grade_name, p.class_id, class_name " +
            "FROM examination_permission AS p INNER JOIN school AS s ON p.school_id = s.id " +
            "INNER JOIN user_grade AS g ON p.grade_id = g.id " +
            "INNER JOIN class_info AS c ON p.class_id = c.id " +
            "WHERE p.record_id = #{rid}")
    List<ExaPermissionVo> selectVoClassList(@Param("rid") Integer rid);

    @Select("SELECT p.id, record_id, p.school_id, school_name, p.grade_id, grade_name " +
            "FROM examination_permission AS p INNER JOIN school AS s ON p.school_id = s.id " +
            "INNER JOIN user_grade AS g ON p.grade_id = g.id " +
            "WHERE p.record_id = #{rid} AND p.class_id IS NULL")
    List<ExaPermissionVo> selectVoGradeList(@Param("rid") Integer rid);

    @Select("SELECT p.id, record_id, p.school_id, school_name " +
            "FROM examination_permission AS p INNER JOIN school AS s ON p.school_id = s.id " +
            "WHERE p.record_id = #{rid} AND p.grade_id IS NULL")
    List<ExaPermissionVo> selectVoSchoolList(@Param("rid") Integer rid);

}
