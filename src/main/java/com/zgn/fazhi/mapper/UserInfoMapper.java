package com.zgn.fazhi.mapper;

import com.zgn.fazhi.pojo.po.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.vo.UserInfoSchool;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Repository
public interface UserInfoMapper extends BaseMapper<UserInfo>{


    //查找用户信息
    @Select("select user_info.*,school.school_name,role.name_zh from user_info,school,role " +
            "where user_info.school_id=school.id and user_info.role_id=role.id")
    List<UserInfoSchool> getInfo();


    //根据身份分类
    @Select("select user_info.*,school.school_name,role.name_zh from user_info,school,role " +
            "where user_info.school_id=school.id and user_info.role_id=role.id and role_id=#{id}")
    List<UserInfoSchool> RoleInfo(@Param(value = "id") int id);


    //根据用户名显示
    @Select("select user_info.*,school.school_name,role.name_zh from user_info,school,role " +
            "where user_info.school_id=school.id and user_info.role_id=role.id and username=#{username}")
    UserInfoSchool ByName(@Param(value = "username") String username);

    /**
     * 根据用户名和密保答案查找用户id
     * @param answer 密保答案
     * @param username 用户名
     * @return uid
     */
    @Select("SELECT uid " +
            "FROM user_info, user_secret " +
            "WHERE username = #{username} AND answer = #{answer}")
    Integer retrieve(@Param(value = "answer")String answer, @Param(value = "username")String username);

    /**
     * 通过用户名查找该用户的密保问题
     * @param username 用户名
     * @return 密保问题
     */
    @Select("SELECT problem " +
            "FROM secret_guard AS g INNER JOIN user_secret AS s ON g.id = s.secret_id " +
            "INNER JOIN user_info AS u ON u.id = s.uid " +
            "WHERE username = #{username}")
    String getQuestion(@Param(value = "username") String username);

    /**
     * 动态条件查询用户id
     * @param schoolId 学校id
     * @param gradeId 年级id
     * @param classId 班级id
     * @return 用户id
     */
    @Select("SELECT id FROM user_info " +
            "WHERE 1=1 " +
            "AND IF(#{schoolId} IS NULL, 1 = 1, school_id = #{schoolId}) " +
            "AND IF(#{gradeId} IS NULL, 1 = 1, grade_id = #{gradeId}) " +
            "AND IF(#{classId} IS NULL, 1 = 1, class_id = #{classId})")
    List<Integer> getIdListDynamic(@Param("schoolId") Integer schoolId,
                                      @Param("gradeId") Integer gradeId,
                                      @Param("classId") Integer classId);

}
