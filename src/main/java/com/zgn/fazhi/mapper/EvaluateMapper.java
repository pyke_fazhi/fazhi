package com.zgn.fazhi.mapper;

import com.zgn.fazhi.pojo.po.Evaluate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Repository
public interface EvaluateMapper extends BaseMapper<Evaluate> {

    @Select("select evaluate.* from user_info,evaluate where user_info.username=#{username} and user_info.id=evaluate.uid")
    Evaluate getByName(@Param("username") String username);


}
