package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.File;
import com.zgn.fazhi.pojo.po.FileType;
import org.springframework.stereotype.Repository;

/**
 * @author ydw
 */
@Repository
public interface FileTypeMapper extends BaseMapper<FileType> {
}
