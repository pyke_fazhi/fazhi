package com.zgn.fazhi.mapper;

import com.zgn.fazhi.pojo.po.AppointmentContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Repository
public interface AppointmentContentMapper extends BaseMapper<AppointmentContent> {

    /**
     * 通过预约id查询预约地点
     * @param aid 预约id
     * @return 预约地点
     */
    @Select("SELECT v.name FROM appointment_content AS a " +
            "INNER JOIN visit_content AS v ON a.vid = v.id " +
            "WHERE aid = #{aid}")
    List<String> getContentByAid(@Param("aid") Integer aid);

}
