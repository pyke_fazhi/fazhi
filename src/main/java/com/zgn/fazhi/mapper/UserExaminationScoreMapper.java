package com.zgn.fazhi.mapper;

import com.zgn.fazhi.pojo.po.UserExaminationScore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.vo.UserExaGrade;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Repository
public interface UserExaminationScoreMapper extends BaseMapper<UserExaminationScore> {


    /**
     * 查询用户历次所有考试练习成绩
     * @param uid 用户id
     * @return 成绩表
     */
    @Select("SELECT record_id, title, achievement1 AS score, ue.start, ue.end, UNIX_TIMESTAMP(ue.end) - UNIX_TIMESTAMP(ue.start) AS time_length " +
            "FROM user_examination AS ue INNER JOIN examination_record AS r ON ue.record_id = r.id " +
            "WHERE ue.uid = #{uid}")
    List<UserExaGrade> getCurrentExaDateListByUid(@Param("uid") Integer uid);


    /**
     * 查询该次考试练习的成绩
     * @param uid 用户id
     * @param rid 开始记录id
     * @return 成绩表
     */
    @Select("SELECT record_id, title, achievement1 AS score, ue.start, ue.end, UNIX_TIMESTAMP(ue.end) - UNIX_TIMESTAMP(ue.start) AS time_length " +
            "FROM user_examination AS ue INNER JOIN examination_record AS r ON ue.record_id = r.id " +
            "WHERE ue.uid = #{uid} AND ue.record_id = #{rid}")
    List<UserExaGrade> getCurrentExaDateByUid(@Param("uid") Integer uid, @Param("rid") Integer rid);

}
