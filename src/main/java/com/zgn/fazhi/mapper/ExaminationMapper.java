package com.zgn.fazhi.mapper;

import com.zgn.fazhi.pojo.po.Examination;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.vo.ExaminationUnit;
import com.zgn.fazhi.result.Result;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Repository
public interface ExaminationMapper extends BaseMapper<Examination> {

    @Select("SELECT id FROM examination " +
            "where type_id = #{typeId} and unit_id = #{unitId} and chapter_id = #{chapterId}" +
            " ORDER BY RAND() LIMIT #{randomNum}")
    List<Integer> getRandomList(@Param("typeId") Integer typeId,
                                @Param("unitId") Integer unitId,
                                @Param("chapterId") Integer chapterId,
                                @Param("randomNum") Integer randomNum);

    @Select("SELECT id FROM examination " +
            "where type_id = #{typeId} and unit_id = #{unitId}" +
            " ORDER BY RAND() LIMIT #{randomNum}")
    List<Integer> getRandomList2(@Param("typeId") Integer typeId,
                                 @Param("unitId") Integer unitId,
                                 @Param("randomNum") Integer randomNum);

    @Select("SELECT id FROM examination " +
            "where unit_id = #{unitId}" +
            " ORDER BY RAND() LIMIT #{randomNum}")
    List<Integer> getRandomListTidNull(@Param("TypeId") Integer typeId,
                                       @Param("unitId") Integer unitId,
                                       @Param("randomNum") Integer randomNum);

    @Select("SELECT id FROM examination " +
            "where teacher_id = #{uid} and type_id = #{typeId}" +
            " ORDER BY RAND() LIMIT #{randomNum}")
    List<Integer> getRandomListByUid(@Param("uid") Integer uid,
                                     @Param("typeId") Integer typeId,
                                     @Param("randomNum") Integer randomNum);

    //通过单元id获取所有该单元所有题目
    @Select("select * from examination,examination_answer " +
            "where examination.unit_id=#{unitId} and examination.id=examination_answer.eid")
    List<ExaminationUnit> getListByUnit(@Param("unitId") Integer unitId);

    //通过章节id查找所有题目
    @Select("select * from examination,examination_answer,unit,chapter " +
            "where examination.chapter_id=#{chapterId} and examination.unit_id = chapter.unit_id and examination.id=examination_answer.eid")
    List<ExaminationUnit> getListByChapter(@Param("chapterId") Integer chapterId);
}
