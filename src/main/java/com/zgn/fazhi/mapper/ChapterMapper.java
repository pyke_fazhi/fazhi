package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.Chapter;
import org.springframework.stereotype.Repository;

@Repository
public interface ChapterMapper extends BaseMapper<Chapter> {
}
