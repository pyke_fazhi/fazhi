package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.SecretGuard;
import org.springframework.stereotype.Repository;

/**
 * @author ydw
 * @version 1.0
 * @Description TODO
 * @date 20/08/2021 11:40 AM
 */

@Repository
public interface SecretGuardMapper extends BaseMapper<SecretGuard> {
}
