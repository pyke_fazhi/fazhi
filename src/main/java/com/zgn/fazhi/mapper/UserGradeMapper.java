package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.UserGrade;
import org.springframework.stereotype.Repository;

@Repository
public interface UserGradeMapper extends BaseMapper<UserGrade> {
}
