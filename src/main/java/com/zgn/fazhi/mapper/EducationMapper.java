package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.Education;
import org.springframework.stereotype.Repository;

@Repository
public interface EducationMapper extends BaseMapper<Education> {
}
