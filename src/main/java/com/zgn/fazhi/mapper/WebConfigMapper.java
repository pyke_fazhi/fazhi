package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.WebConfig;
import org.springframework.stereotype.Repository;

@Repository
public interface WebConfigMapper extends BaseMapper<WebConfig> {
}
