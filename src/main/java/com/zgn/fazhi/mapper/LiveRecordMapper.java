package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.LiveRecord;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 21/08/2021 8:32 PM
 */

@Repository
public interface LiveRecordMapper extends BaseMapper<LiveRecord> {

    /**
     * 通过直播间id获取该直播间最后一次直播记录
     * @param lid 直播间id
     * @return 最后一次直播记录
     */
    @Select("SELECT r.id, live_start, live_end, like, live_id " +
            "FROM live_info AS l INNER JOIN live_record AS r ON l.record_id = r.id " +
            "WHERE l.id = #{liveId}")
    LiveRecord getLastRecord(@Param("liveId") Integer lid);

}
