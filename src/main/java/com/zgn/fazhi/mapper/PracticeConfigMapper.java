package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.PracticeConfig;
import org.springframework.stereotype.Repository;

/**
 * @author ydw
 */
@Repository
public interface PracticeConfigMapper extends BaseMapper<PracticeConfig> {
}
