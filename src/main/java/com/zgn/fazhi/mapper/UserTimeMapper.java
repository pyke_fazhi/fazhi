package com.zgn.fazhi.mapper;

import com.zgn.fazhi.pojo.po.UserTime;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.vo.UserTimeData;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Repository
public interface UserTimeMapper extends BaseMapper<UserTime> {

    @Select("select time_length from user_time where uid=#{uid}")
    Integer timeLength(@Param("uid") Integer id);

    @Select("select name from rank_info where time_length=#{timeL}")
    String getName(@Param("timeL") Integer timeL);

    /**
     * 查询所有有使用时长数据的用户数据
     * @return
     */
    @Select("SELECT uid, time_length, name " +
            "FROM user_time AS time INNER JOIN user_info AS info ON time.uid = info.id")
    List<UserTimeData> selectAllData();

}
