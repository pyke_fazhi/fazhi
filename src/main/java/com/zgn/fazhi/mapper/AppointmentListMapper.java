package com.zgn.fazhi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zgn.fazhi.pojo.po.AppointmentList;
import org.springframework.stereotype.Repository;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 21/08/2021 11:34 AM
 */

@Repository
public interface AppointmentListMapper extends BaseMapper<AppointmentList> {
}
