package com.zgn.fazhi.utils;

import java.util.Calendar;
import java.util.Date;

public class BelongCalendar {

    /**
     * 判断当前时间是否在指定时间内
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @return 是否在范围内
     */
    public static boolean calendar( Date beginTime,
                                    Date endTime) {
        Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        System.out.println("现在时间: " + date);

        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);
        System.out.println("开始时间: " +begin);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);
        System.out.println("结束时间: " + end);

        if (date.after(begin) && date.before(end)) {
            System.out.println("在时间范围内，可以答题");
            return true;
        } else {
            System.out.println("不在时间内，无法答题");
            return false;
        }
    }


    public static boolean inCalendar( Date beginTime, Date endTime, Date now) {
        Calendar date = Calendar.getInstance();
        date.setTime(now);
        System.out.println("现在时间: " + date);

        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);
        System.out.println("开始时间: " +begin);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);
        System.out.println("结束时间: " + end);

        if (date.after(begin) && date.before(end)) {
            System.out.println("在时间范围内，可以答题");
            return true;
        } else {
            System.out.println("不在时间内，无法答题");
            return false;
        }
    }

}
