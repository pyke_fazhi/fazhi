package com.zgn.fazhi.utils;

import com.alibaba.excel.EasyExcel;
import com.zgn.fazhi.pojo.vo.UserExcel;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.net.URLEncoder;
import java.util.List;

public class ExcelUtils {

    /**
     * Excel表格导出
     * @param response HttpServletResponse对象
     * @param excelData Excel表格的数据，封装为List<List<String>>
     * @param fileName 导出Excel的文件名
     * @throws IOException 抛IO异常
     */
    public static void exportExcel(HttpServletResponse response,
                                   List<UserExcel> excelData,
                                   String fileName) throws IOException {
        // 准备将Excel的输出流通过response输出到页面下载
        // 八进制输出流
        response.setContentType("application/octet-stream");
        // 告诉浏览器用什么软件可以打开此文件
        response.setHeader("content-Type","application/vnd.ms-excel");
        String fileName1 = URLEncoder.encode( fileName, "UTF-8");
        //设置导出Excel的名称
        response.setHeader("Content-disposition", "attachment;filename=" + fileName1 + ".xlsx");
        System.out.println("data :" + excelData);
        EasyExcel.write(response.getOutputStream(), UserExcel.class).sheet("模板").doWrite(excelData);
    }
}


