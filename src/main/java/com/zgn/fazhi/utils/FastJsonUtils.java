package com.zgn.fazhi.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;

public class FastJsonUtils {

    /**
     * 将Json字符串转换为Object类型的
     */
    public static Object toObject(String str) {
        return JSON.parse(str);
    }

    /**
     * 将Json字符串转换为实例
     */
    public static <T> T toBean(Object object, Class<T> t) {
        return JSONObject.parseObject(JSONObject.toJSONString(object), t);
    }

    /**
     * 将Json转换为指定类型的List
     */
    public static <T> List<T> toList(List list, Class<T> t) {
        return JSONArray.parseArray(JSONObject.toJSON(list).toString(), t);
    }

    /**
     * 将Object转换为指定类型的List
     */
    public static <T> List<T> toList(Object list, Class<T> t) {
        return JSON.parseArray(JSONObject.toJSONString(list), t);
    }

    /**
     * 将Json转换为Map
     */
    public static <T> Map<String, T> toMap(String str) {
        return (Map<String, T>) JSONObject.parseObject(str);
    }

}
