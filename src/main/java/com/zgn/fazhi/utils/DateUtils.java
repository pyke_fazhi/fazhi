package com.zgn.fazhi.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateUtils {

    /**
     * 获取从今天开始指定日数的日期
     *
     * @param past 指定日数
     * @return 返回后past日期的string类型的list集合
     * @throws ParseException 抛出date转化异常
     */
    public static List<String> getFutureDate(int past) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        ArrayList<String> dateList = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR));
        // 加入当天数据
        dateList.add(format.format(calendar.getTime()));
        past--;
        for (int i = 0; i < past; i++) {
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 1);
            Date today = calendar.getTime();
            dateList.add(format.format(today));
        }
        return dateList;
    }

    /**
     * 将string类型日期转化为date类型
     *
     * @param strDate 需要转换的str
     * @return 返回date类型的日期
     */
    public static Date toDate(String strDate) throws ParseException {
        // 注意月份是MM
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.parse(strDate);
    }

    public static Date toDateHms(String strDate) throws ParseException {
        // 注意月份是MM
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.parse(strDate);
    }

    /**
     * 日期date类型转str类型
     *
     * @param date 需要转换的日期
     * @return 返回str类型的date
     */
    public static String dateToString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(date);
    }

}
