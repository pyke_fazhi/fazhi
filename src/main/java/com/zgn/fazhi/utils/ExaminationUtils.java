package com.zgn.fazhi.utils;

import com.zgn.fazhi.controller.AppointmentController;
import com.zgn.fazhi.pojo.po.Examination;
import com.zgn.fazhi.pojo.po.ExaminationAnswer;
import com.zgn.fazhi.service.ExaminationAnswerService;
import com.zgn.fazhi.service.ExaminationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ExaminationUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExaminationUtils.class);

    @Lazy
    @Autowired
    private ExaminationService examinationService;

    @Autowired
    private ExaminationAnswerService examinationAnswerService;

    @Autowired
    private RedisUtil redisUtil;

    private String key = "examination";



    public String hasExamination(){
        System.out.println("注入redis工具类 :" + redisUtil);
        if (!redisUtil.hasKey(key)) {
            // 没有则从数据库中获取保存到缓存中, 每次更新考题库时刷新缓存
            initExamination();
        }
        return key;
    }

    /**
     * 刷新考题库缓存
     * @return key
     */
    public String refreshExamination(){
        LOGGER.info("刷新考题库缓存中");
        if (redisUtil.hasKey(key)) {
            redisUtil.del(key);
            // 没有则从数据库中获取保存到缓存中, 每次更新考题库时刷新缓存
            initExamination();
        }
        return key;
    }

    /**
     * 从数据库中获取所有考题，保存到redis缓存中
     */
    public void initExamination(){
        List<Examination> examinations = examinationService.selectAllPaper();
        HashMap<String, Examination> map = new HashMap<>();
        for (Examination examination : examinations) {
            map.put(examination.getId().toString(), examination);
        }
        redisUtil.hmSet(key, map);

        // 获取所有选项，放入redis缓存中
        List<ExaminationAnswer> optionList = examinationAnswerService.list();
        Map<Integer, List<ExaminationAnswer>> options = optionList.stream().collect(Collectors.groupingBy(ExaminationAnswer::getEid));
        String ket1 = "exaOption:";
        options.keySet().forEach(eid -> {
            redisUtil.lSet(ket1 + eid, options.get(eid));
        });

    }

}
