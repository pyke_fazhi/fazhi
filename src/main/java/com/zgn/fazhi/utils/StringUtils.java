package com.zgn.fazhi.utils;

import com.zgn.fazhi.service.WebConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;

@Component
public class StringUtils {

    @Autowired
    private WebConfigService webConfigService;

    /**
     * java生成随机账号uid
     *
     * @return
     */
    public String getRandomUsername() {
        StringBuilder val = new StringBuilder();
        for (int i = 0; i < webConfigService.getUserNameLength(); i++) {
            val.append(ThreadLocalRandom.current().nextInt(10));
        }
        return val.toString();
    }
}
