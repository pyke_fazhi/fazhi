package com.zgn.fazhi.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author ydw
 */
@Data
@Accessors(chain = true)
public class UserGradeInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    private Integer uid;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "用户别名")
    private String name;

    @ApiModelProperty(value = "各个级别数据")
    private List<Map<String, Object>> gradeInfo;

    @ApiModelProperty(value = "总练习时长")
    private Integer ptime;

    @ApiModelProperty(value = "练习总题数")
    private Integer ptotal;

    @ApiModelProperty(value = "查询时间")
    private String time;

}
