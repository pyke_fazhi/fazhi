package com.zgn.fazhi.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 上传文件的表单数据实体类
 */
@Data
public class FileUploadForm implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "上传id为空")
    private Integer id;

    /**
     * 文件分类id
     */
    @NotNull(message = "分类id为空")
    private Integer typeId;

    /**
     * 文件信息
     */
    private String info;

    /**
     * 文件标题
     */
    private String title;

    /**
     * 上传的文件
     */
    @ApiModelProperty(value = "文件",dataType = "MultipartFile")
    @NotNull(message = "文件为空")
    private MultipartFile uploadFile;


}
