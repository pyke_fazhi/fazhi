package com.zgn.fazhi.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class UserGradeDate implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "等级名称")
    private String gname;

    @ApiModelProperty(value = "练习时长")
    private Integer ptime;

    @ApiModelProperty(value = "正确率")
    private Float acc;

    @ApiModelProperty(value = "练习题数")
    private Integer total;

}
