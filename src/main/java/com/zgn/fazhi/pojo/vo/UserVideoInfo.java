package com.zgn.fazhi.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class UserVideoInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer uid;

    private String name;

    private String school;

    private Integer time;

}
