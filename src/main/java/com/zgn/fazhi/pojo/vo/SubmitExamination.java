package com.zgn.fazhi.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 用户提交考试数据
 * @author ydw
 */
@Data
@Accessors(chain = true)
public class SubmitExamination implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户答案")
    private List<Answer> answer;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开始答题时间")
    private Date start;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "提交考试时间")
    private Date end;

    @ApiModelProperty(value = "统考id")
    private Integer recordId;

    @ApiModelProperty(value = "当前用户id")
    private Integer uid;

}
