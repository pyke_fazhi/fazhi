package com.zgn.fazhi.pojo.vo;

import com.zgn.fazhi.pojo.po.ExaminationRecord;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 返回前端查询的当前用户的考试/练习列表，分为进行中和非进行中
 */
@Data
@Accessors(chain = true)
public class ExaRecordMap implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<ExaRecord> startList;

    private List<ExaRecord> noStartList;

}
