package com.zgn.fazhi.pojo.vo;

import com.zgn.fazhi.pojo.po.ExaminationPermission;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author ydw
 * @version 1.0
 * @description 考试练习权限开放
 * @date 20/08/2021 4:14 PM
 */

@Data
@Accessors(chain = true)
public class ExaPermissionVoList implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 开放等级为学校列表
     */
    private List<ExaPermissionVo> schoolList;

    /**
     * 开放等级为年级列表
     */
    private List<ExaPermissionVo> gradeList;

    /**
     * 开放等级为班级列表
     */
    private List<ExaPermissionVo> classList;

}
