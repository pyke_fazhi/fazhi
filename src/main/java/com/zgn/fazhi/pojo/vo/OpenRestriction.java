package com.zgn.fazhi.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @author ydw
 */
@Data
public class OpenRestriction implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "学校id不能为空")
    @ApiModelProperty(value = "学校id")
    private Integer schoolId;

    @ApiModelProperty(value = "年级id集合")
    private List<Integer> gradeId;

    @ApiModelProperty(value = "班级id集合")
    private List<Integer> classId;

}
