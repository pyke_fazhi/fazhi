package com.zgn.fazhi.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户对考试详情
 * @author ydw
 */
@Data
@Accessors(chain = true)
public class Grades implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    private Integer uid;

    @ApiModelProperty(value = "统考id")
    private Integer eid;

    @ApiModelProperty(value = "开始时间")
    private Date start;

    @ApiModelProperty(value = "结束时间")
    private Date end;

    @ApiModelProperty(value = "考试时长")
    private Integer length;

    @ApiModelProperty(value = "该用户本次考试所用时长")
    private Integer useLength;

    @ApiModelProperty(value = "成绩")
    private Integer score;

    @ApiModelProperty(value = "是否还可以考试")
    private Integer ifExamination;

    @ApiModelProperty(value = "考试名称")
    private String title;

}
