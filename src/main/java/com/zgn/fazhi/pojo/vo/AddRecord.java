package com.zgn.fazhi.pojo.vo;

import com.zgn.fazhi.pojo.po.EtypeScore;
import com.zgn.fazhi.pojo.po.ExaminationRecord;
import lombok.Data;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

/**
 * 添加统考实体类，接收前端数据
 * @author ydw
 */
@Data
public class AddRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 考试基本设置
     */
    @Valid
    private ExaminationRecord examinationRecord;

    /**
     * 考试题型设置
     */
    private List<EtypeScore> etypeScore;

}
