package com.zgn.fazhi.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="封装资源权限角色列表", description="")
public class PermissionList implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "资源类型id为空")
    @ApiModelProperty(value = "资源类型id，比如video/image")
    private Integer resourcesTid;

    @NotNull(message = "资源id为空")
    @ApiModelProperty(value = "该资源类型的资源id")
    private Integer resid;

    @NotNull(message = "资源分类id为空")
    @ApiModelProperty(value = "该资源类型的分类id")
    private Integer typeId;

    @NotNull(message = "公开选项为空")
    @ApiModelProperty(value = "是否公开")
    private Integer ifPublic;

    @NotNull(message = "角色权限集合为空")
    @ApiModelProperty(value = "学历id List")
    private List<Integer> educationIdList;

}
