package com.zgn.fazhi.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ydw
 * @version 1.0
 * @description 直播间基础信息
 * @date 17/08/2021 11:10 PM
 */

@Data
public class UserLiveInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("房间号")
    private Integer id;

    @ApiModelProperty("主播名")
    private String name;

    @ApiModelProperty("直播标题")
    private String title;

    @ApiModelProperty("直播简介")
    private String info;

    @ApiModelProperty("是否正在直播")
    private Integer ifLive;

    @ApiModelProperty("直播截图url")
    private String screenshot;

}
