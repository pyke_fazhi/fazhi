package com.zgn.fazhi.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 封装练习后返回前端的答案解析
 */
@Data
@Accessors(chain = true)
public class AnswerAnalysis implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "练习题id")
    private Integer eid;

    @ApiModelProperty(value = "题型id")
    private Integer tid;

    @ApiModelProperty(value = "标准答案")
    private String answer;

    @ApiModelProperty(value = "用户答案")
    private String userAnswer;

    @ApiModelProperty(value = "用户是否答对该题")
    private Boolean ifCorrect;

    @ApiModelProperty(value = "该题所有选项")
    private List<String> options;


}
