package com.zgn.fazhi.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class ExaRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "练习/考试id")
    private Integer recordId;

    @ApiModelProperty(value = "开始时间")
    private Date start;

    @ApiModelProperty(value = "结束时间")
    private Date end;

    @ApiModelProperty(value = "考试时长")
    private Integer length;

    @ApiModelProperty(value = "是否还可以参加")
    private Integer ifParticipate;

    @ApiModelProperty(value = "考试/练习名称")
    private String title;



}
