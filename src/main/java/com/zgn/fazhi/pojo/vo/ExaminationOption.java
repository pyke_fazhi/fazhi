package com.zgn.fazhi.pojo.vo;


import com.zgn.fazhi.pojo.po.Examination;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

@Data
@Accessors(chain = true)
public class ExaminationOption implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    private Examination examination;

    private List<String> options;

}
