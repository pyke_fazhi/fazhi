package com.zgn.fazhi.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
public class UserExaGrades implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<AnswerAnalysis> answerAnalyses;

    private Integer grades;

}
