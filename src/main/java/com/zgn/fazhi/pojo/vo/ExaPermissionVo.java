package com.zgn.fazhi.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 20/08/2021 5:23 PM
 */

@Data
@Accessors(chain = true)
public class ExaPermissionVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    @ApiModelProperty(value = "记录id")
    private Integer recordId;

    @ApiModelProperty(value = "学校id")
    private Integer schoolId;

    private String schoolName;

    @ApiModelProperty(value = "年级id")
    private Integer gradeId;

    private String gradeName;

    @ApiModelProperty(value = "班级id")
    private Integer classId;

    private String className;

}
