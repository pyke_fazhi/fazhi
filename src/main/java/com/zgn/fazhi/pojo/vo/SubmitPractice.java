package com.zgn.fazhi.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 接收用户提交练习题
 * @author ydw
 */
@Data
public class SubmitPractice implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 答案数组,格式为: [{题目id:用户答案},{题目id:用户答案}]
     */
    private List<Answer> answerList;
    /**
     * 开始时间
     */
    private Date start;
    /**
     * 结束时间
     */
    private Date end;



}
