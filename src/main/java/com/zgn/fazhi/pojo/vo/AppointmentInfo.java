package com.zgn.fazhi.pojo.vo;

import com.zgn.fazhi.pojo.po.Appointment;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * 传输预约数据json
 * @author ydw
 */
@Data
@Accessors(chain = true)
public class AppointmentInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Valid
    @NotNull(message = "预约数据不能为空")
    @ApiModelProperty(value = "预约数据")
    private Appointment info;

    @Size(min = 1, message = "参观地点至少选择一处")
    @ApiModelProperty(value = "参观地点id的list数组")
    private List<Integer> visitList;

}
