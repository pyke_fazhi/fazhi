package com.zgn.fazhi.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class UserTimeData implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private Integer uid;

    /**
     * 用户名
     */
    private String name;

    /**
     * 使用时长
     */
    private Integer timeLength;

}
