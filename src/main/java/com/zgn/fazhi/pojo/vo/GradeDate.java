package com.zgn.fazhi.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 级别数据
 * 数据包括各个等级正确率,各个级别总练习时长，各个级别练习人数
 * @author ydw
 */
@Data
@Accessors(chain = true)
public class GradeDate implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "级别id")
    private Integer gid;

    @ApiModelProperty(value = "正确率")
    private Float accRate;

    @ApiModelProperty(value = "总练习时长")
    private Integer practiceLength;

    @ApiModelProperty(value = "总练习题数")
    private Integer praTotal;

    @ApiModelProperty(value = "练习人数")
    private Integer praNum;

    @ApiModelProperty(value = "等级描述")
    private String gname;

}
