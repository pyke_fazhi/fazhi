package com.zgn.fazhi.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *  提交答案的数据格式
 * @author ydw
 */
@Data
@Accessors(chain = true)
public class Answer implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer pid;
    private String option;

}
