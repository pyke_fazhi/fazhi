package com.zgn.fazhi.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 21/08/2021 1:11 PM
 */

@Data
public class ApiSaveLiveModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * domain : live.aliyunlive.com
     * app : live
     * stream : hello
     * uri : live/hello/0_2017-03-08-23:09:46_2017-03-08-23:10:40.flv
     * duration : 69.403
     * start_time : 1488985786
     * stop_time : 1488985840
     */

    private String domain;
    private String app;

    /**
     * 流id
     */
    private String stream;

    /**
     * 视频存储路径
     */
    private String uri;

    /**
     * 录制时间
     */
    private double duration;

    /**
     * 开始时间戳
     */
    private int start_time;

    /**
     * 结束时间戳
     */
    private int stop_time;

}
