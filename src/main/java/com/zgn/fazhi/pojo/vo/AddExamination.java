package com.zgn.fazhi.pojo.vo;

import com.zgn.fazhi.pojo.po.ExaminationtTypeOption;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 接收前端插入统考考题
 * @author ydw
 */
@Data
public class AddExamination implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 统考id
     */
    private Integer rId;

    /**
     * 如果手动组题，需要考题id
     * 考题id集合
     */
    private List<Integer> examinationIdList;

    /**
     * 自动组题配置
     */
    private  List<ExaminationtTypeOption> autoConfigList;

}
