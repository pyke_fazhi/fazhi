package com.zgn.fazhi.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 封装单个用户的成绩实体类
 */
@Data
@Accessors(chain = true)
public class UserExaGrade implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 考试练习id
     */
    private Integer recordId;

    /**
     * 考试练习标题
     */
    private String title;

    /**
     * 总分
     */
    private Integer score;

    /**
     * 开始考试时间
     */
    private Date start;

    /**
     * 结束考试时间
     */
    private Date end;

    /**
     * 考试时长
     */
    private Integer timeLength;

}
