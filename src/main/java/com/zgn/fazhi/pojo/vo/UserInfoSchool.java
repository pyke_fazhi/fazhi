package com.zgn.fazhi.pojo.vo;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;


/**
 * 用户信息
 * @author wt
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoSchool implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "用户别名")
    private String name;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "角色中文名")
    private String nameZh;

    @ApiModelProperty("学校名称")
    private String SchoolName;

    @ApiModelProperty(value = "年级")
    private Integer grade;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "创建日期")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "学历")
    private String education;

    @ApiModelProperty(value = "生日")
    private Date birthday;
}
