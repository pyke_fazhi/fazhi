package com.zgn.fazhi.pojo.vo;

import com.zgn.fazhi.pojo.po.UserVideo;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
@Accessors(chain = true)
public class UserVideoDate implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 所有观看视频记录
     */
    private List<UserVideo> userVideo;

    /**
     * 播放总时长
     */
    private Integer totalTime;

}
