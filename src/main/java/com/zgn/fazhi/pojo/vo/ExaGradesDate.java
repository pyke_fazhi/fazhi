package com.zgn.fazhi.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 用于封装一次考试练习的总数居的实体类
 * 数据包括该次考试练习的平均分、正确率、参与人数、参与率
 */
@Data
@Accessors(chain = true)
public class ExaGradesDate implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 平均分
     */
    private Double avg;

    /**
     * 参加总人数
     */
    private Integer numOfPeople;

    /**
     * 参与率
     */
    private Integer participationRate;

    /**
     * 最高分
     */
    private Integer maxScore;

    /**
     * 最低分
     */
    private Integer minScore;

    /**
     * 及格率
     */
    private String passRate;

}
