package com.zgn.fazhi.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 接收前端考试/练习开放人群限制
 * @author ydw
 */
@Data
@Accessors(chain = true)
public class ExaPermission implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "记录id不能为空")
    @ApiModelProperty(value = "记录id")
    private Integer recordId;

    private List<OpenRestriction> permission;


}
