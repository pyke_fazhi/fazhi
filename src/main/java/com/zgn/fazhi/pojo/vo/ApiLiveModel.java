package com.zgn.fazhi.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ApiLiveModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 事件： publish 推流
     *       publish_done 断流
     */
    private List<String> action;

    /**
     * 推流的客户端IP
     */
    private List<String> ip;

    /**
     * 推流流名称
     */
    private List<String> id;

    /**
     * 推流域名。默认为自定义的推流域名，如果未绑定推流域名即为播流域名
     */
    private List<String> app;

    /**
     * 推流应用名称
     */
    private List<String> appname;

    /**
     * Unix时间戳。单位：秒
     */
    private List<String> time;

    /**
     *  用户推流的参数
     */
    private List<String> usrargs;

    /**
     *  CDN接受流的节点或者机器名
     */
    private List<String> node;

}
