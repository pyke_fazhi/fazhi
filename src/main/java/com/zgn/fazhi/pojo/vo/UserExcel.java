package com.zgn.fazhi.pojo.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
public class UserExcel extends BaseRowModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ExcelProperty(index = 0, value = "昵称")
    private String name;

    @ExcelProperty(index = 1, value = "角色")
    private String role;

    @ExcelProperty(index = 2, value = "学校")
    private String school;

    @ExcelProperty(index = 3, value = "年级")
    private String grade;

    @ExcelProperty(index = 4, value = "班级")
    private String className;

    @ExcelProperty(index = 5, value = "密码")
    private String password;

    @ExcelProperty(index = 6, value = "账号")
    private String username;

}
