package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserExamination对象", description="")
public class UserExamination implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户考试成绩主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer uid;

    @ApiModelProperty(value = "统考id")
    private Integer recordId;

    @ApiModelProperty(value = "用户在该次统考的成绩")
    private Integer achievement1;

    @ApiModelProperty(value = "开始答题时间")
    private Date start;

    @ApiModelProperty(value = "提交时间")
    private Date end;


}
