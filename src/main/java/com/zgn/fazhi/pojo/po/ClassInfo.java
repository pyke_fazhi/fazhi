package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 班级
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ClassInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotNull(message = "班级名称不能为空")
    @TableField(value = "class_name")
    @ApiModelProperty(value = "班级名称")
    private String name;

    @ApiModelProperty(value = "年级id")
    private Integer gradeId;

    @ApiModelProperty(value = "学校id")
    private Integer schoolId;

}
