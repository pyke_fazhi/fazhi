package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 公有题库每个单元的章节
 */
@Data
public class Chapter implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotNull(message = "单元id不能为空")
    @ApiModelProperty(value = "单元id")
    private Integer unitId;

    @NotNull(message = "章节id不能为空")
    @ApiModelProperty(value = "章节标题")
    private String title;

    @ApiModelProperty(value = "章节简介")
    private String info;

}
