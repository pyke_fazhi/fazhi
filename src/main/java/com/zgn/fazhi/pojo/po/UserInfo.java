package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserInfo对象", description="")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户名")
    private String username;

    @NotBlank(message = "密码不能为空或空格无效字符")
    @ApiModelProperty(value = "密码")
    private String password;

    @NotBlank(message = "用户昵称不能为空")
    @ApiModelProperty(value = "用户昵称")
    private String name;

    @ApiModelProperty(value = "角色id")
    private Integer roleId;

    @NotNull(message = "学校id不能为空")
    @ApiModelProperty(value = "学校id")
    private Integer schoolId;

    @NotNull(message = "年级id不能为空")
    @ApiModelProperty(value = "年级id")
    private Integer gradeId;

    @NotNull(message = "班级id不能为空")
    @ApiModelProperty(value = "班级id")
    private Integer classId;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "创建日期")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    private String salt;

    @NotNull(message = "学历id不能为空")
    @ApiModelProperty(value = "学历id")
    private Integer educationId;

    @ApiModelProperty(value = "生日")
    private Date birthday;

}
