package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * 封装返回用户考试练习数据
 */
@Data
@Accessors(chain = true)
public class RecordExa implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试题id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题型")
    private Integer typeId;

    @ApiModelProperty("考试题目")
    private String title;

    @ApiModelProperty("单元id")
    private Integer unitId;

    @ApiModelProperty("章节id")
    private Integer chapterId;

    @ApiModelProperty("该题目的选项")
    List<String> option;


}
