package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TestAccuracy对象", description="")
public class TestAccuracy implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "考试正确率数据")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "统考id")
    private Integer eid;

    @ApiModelProperty(value = "题型id、比如多选单选判断")
    private Integer typeId;

    @ApiModelProperty(value = "正确率")
    private Double accuracy;

    @ApiModelProperty(value = "正确题数")
    private Integer correct;

    @ApiModelProperty(value = "错误题数")
    private Integer error;


}
