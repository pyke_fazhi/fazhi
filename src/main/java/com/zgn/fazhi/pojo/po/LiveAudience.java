package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ydw
 * @version 1.0
 * @description 直播观众
 * @date 21/08/2021 8:22 PM
 */

@Data
@Accessors(chain = true)
public class LiveAudience implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "观看直播的用户id")
    private Integer uid;

    @ApiModelProperty(value = "观看时长时间戳")
    private Integer viewTime;

    @ApiModelProperty(value = "第一次进入时间")
    private Date start;

    @ApiModelProperty(value = "最后一次退出时间")
    private Date end;

    @ApiModelProperty(value = "直播记录id")
    private Integer recordId;

}
