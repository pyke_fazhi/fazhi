package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author ydw
 * @version 1.0
 * @description 预约名单
 * @date 21/08/2021 11:27 AM
 */

@Data
@Accessors(chain = true)
public class AppointmentList implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "预约id外键")
    private Integer aid;

    @ApiModelProperty(value = "成员名")
    private String member;


}
