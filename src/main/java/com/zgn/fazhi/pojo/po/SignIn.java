package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.Version;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="SignIn对象", description="")
public class SignIn implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "签到表id")
    private Integer id;

    @ApiModelProperty(value = "保存路径")
    private String url;

    @ApiModelProperty(value = "保存时间")
    private LocalDateTime time;


}
