package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Appointment对象", description="")
public class Appointment implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "预约主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "预约日期不能为空")
    @ApiModelProperty(value = "预约日期")
    private Date goDate;

    @ApiModelProperty(value = "预约时段id,比如1为上午，2为中午...")
    private Integer intervalId;

    @ApiModelProperty(value = "当前创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "是否满人")
    private Boolean manchu;

    @Max(value = 50, message = "参观人数不能超过50人")
    @ApiModelProperty(value = "参观人数")
    private Integer teamNumber;

    @ApiModelProperty(value = "领队者手机号码")
    private String phone;

    @ApiModelProperty(value = "用户id")
    private Integer uid;

    @ApiModelProperty(value = "学校id")
    private Integer schoolId;

    @TableField(exist = false)
    @ApiModelProperty(value = "参观地点")
    private List<String> place;

    @TableField(exist = false)
    @ApiModelProperty(value = "学校名称")
    private String schoolName;

}
