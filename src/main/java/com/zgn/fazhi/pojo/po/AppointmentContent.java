package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="AppointmentContent对象", description="")
public class AppointmentContent implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "一次预约选择的参观地点")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "预约地点id")
    private Integer vid;

    @ApiModelProperty(value = "预约记录id")
    private Integer aid;


}
