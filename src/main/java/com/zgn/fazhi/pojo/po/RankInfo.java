package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Rank对象", description="")
public class RankInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "成长等级主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotEmpty(message = "等级名称不能为空")
    @ApiModelProperty(value = "等级名称")
    private String name;

    @ApiModelProperty(value = "等级信息")
    private String info;

    @NotNull(message = "时间不能为空")
    @ApiModelProperty(value = "到达等级所需时间戳")
    private Integer timeLength;


}
