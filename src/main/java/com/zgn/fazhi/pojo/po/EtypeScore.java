package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="EtypeScore对象", description="")
public class EtypeScore implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统考分数组成")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "统考id")
    private Integer eid;

    @NotNull
    @ApiModelProperty(value = "题型id")
    private Integer tid;

    @NotNull
    @ApiModelProperty(value = "题目数量")
    private Integer number;

    @NotNull
    @ApiModelProperty(value = "每题分数")
    private Integer score;


}
