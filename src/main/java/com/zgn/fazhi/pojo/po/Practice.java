package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Practice对象", description="")
public class Practice implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "练习题id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题型,1为单选，2为多选，3为判断")
    private Integer typeId;

    @ApiModelProperty(value = "级别，1为小学，2为初中，3为高中")
    private Integer gradeId;

    @ApiModelProperty(value = "答案")
    private String answer;

    @ApiModelProperty(value = "答案解析,字数最多为127")
    private String analysis;

    @ApiModelProperty(value = "导入时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "随机组题时，是否加入筛选")
    private Integer deleted;

    @ApiModelProperty("题目")
    private String title;


}
