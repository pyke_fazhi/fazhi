package com.zgn.fazhi.pojo.po;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 练习题配置，主要配置用户练习每次获取的题型题数的数据
 * @author ydw
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="练习题配置", description="")
public class PracticeConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "练习题配置主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "练习题题型")
    private Integer tid;

    @ApiModelProperty(value = "该题型的数量")
    private Integer number;

}
