package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author ydw
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="预约配置", description="")
public class AppointmentConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "预约配置主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "每个时间段最大人数")
    private Integer maxNumber;


}
