package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="VideoType对象", description="")
public class VideoType implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "视频分类主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "类别名称")
    private String name;


}
