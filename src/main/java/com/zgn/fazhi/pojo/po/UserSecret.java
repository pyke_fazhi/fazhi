package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户密保中间表
 */
@Data
public class UserSecret implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户")
    private Integer uid;

    @ApiModelProperty(value = "密保问题id")
    private Integer secretId;

    @ApiModelProperty(value = "回答")
    private String answer;

}
