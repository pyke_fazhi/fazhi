package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="PgradeAccouracy对象", description="")
public class PgradeAccouracy implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分级练习题数据主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "级别id")
    private Integer gid;

    @ApiModelProperty(value = "正确题数")
    private Integer correct;

    @ApiModelProperty(value = "错误题数")
    private Integer error;

    @ApiModelProperty(value = "正确率")
    private Integer accuracy;


}
