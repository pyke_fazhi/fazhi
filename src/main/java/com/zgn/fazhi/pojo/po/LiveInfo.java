package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ydw
 * @version 1.0
 * @description 直播间信息
 * @date 17/08/2021 8:08 PM
 */

@Data
@Accessors(chain = true)
@ApiModel("直播信息实体类")
public class LiveInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "房间id不能为空")
    @ApiModelProperty("直播房间号id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("主播id")
    private Integer uid;

    @ApiModelProperty("直播间标题")
    private String title;

    @ApiModelProperty("直播简介")
    private String info;

    @ApiModelProperty("是否直播中")
    private Integer ifLive;

    @ApiModelProperty("直播截图url")
    private String screenshot;

    @ApiModelProperty("最后一次直播记录id")
    private Integer recordId;

}
