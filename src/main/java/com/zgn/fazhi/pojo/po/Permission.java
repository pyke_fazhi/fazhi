package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="权限管理", description="")
public class Permission implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "资源类型id，比如video/image")
    private Integer resourcesTid;

    @ApiModelProperty(value = "该资源类型的资源id")
    private Integer resid;

    @ApiModelProperty(value = "是否公开")
    private Integer ifPublic;

    @ApiModelProperty(value = "学历id")
    private Integer educationId;

    @ApiModelProperty(value = "该资源类型的分类id")
    private Integer typeId;

}
