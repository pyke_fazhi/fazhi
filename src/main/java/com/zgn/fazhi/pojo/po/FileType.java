package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 文档分类
 * @author ydw
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="文档分类", description="")
public class FileType implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文档主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "分类表述")
    private String info;

}
