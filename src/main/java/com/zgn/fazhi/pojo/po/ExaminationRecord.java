package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ExaminationRecord对象", description="")
public class ExaminationRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "统考id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "考试开始时间不能为空")
    @ApiModelProperty(value = "考试开始时间")
    private Date start;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "考试结束时间不能为空")
    @ApiModelProperty(value = "考试结束时间")
    private Date end;

    @ApiModelProperty(value = "考试人数")
    private Integer number;

    @ApiModelProperty(value = "平均合格率")
    private Double passRate;

    @NotNull(message = "考试时长不能为空")
    @ApiModelProperty(value = "考试时长时间戳")
    private Integer lengthTime;

    @Min(value = 1, message = "考试次数必须大于等于1")
    @ApiModelProperty(value = "可考试次数，默认为1次")
    private Integer times;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @NotNull(message = "考试标题不能为空")
    @ApiModelProperty(value = "考试名称")
    private String title;

    @ApiModelProperty(value = "发起者id")
    private Integer uid;

    @NotNull(message = "请指定为练习或考试")
    @ApiModelProperty(value = "是否为练习")
    private Integer ifpractice;

    @NotNull(message = "请选择是否公开")
    @ApiModelProperty(value = "是否公开")
    private Integer ifPublic;

}
