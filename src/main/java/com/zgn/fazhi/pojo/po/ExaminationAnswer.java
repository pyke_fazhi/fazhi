package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ExaminationAnswer对象", description="")
public class ExaminationAnswer implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @ApiModelProperty(value = "统考试题id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotNull
    @ApiModelProperty(value = "选项内容")
    private String answerOption;

    @NotNull
    @ApiModelProperty(value = "考题id")
    private Integer eid;


}
