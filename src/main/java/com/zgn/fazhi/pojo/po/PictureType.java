package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.Version;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="PictureType对象", description="")
public class PictureType implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "图片类型主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotNull
    @ApiModelProperty(value = "类型名称")
    private String name;


}
