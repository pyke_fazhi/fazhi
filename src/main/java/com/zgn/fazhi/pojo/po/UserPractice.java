package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="UserPractice对象", description="")
public class UserPractice implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户练习数据主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer uid;

    @ApiModelProperty(value = "练习题id")
    private Integer pid;

    @ApiModelProperty(value = "用户对该题的答案")
    private String answer;

    @ApiModelProperty(value = "是否回答正确，1表示正确，0表示错误")
    private Integer correct;

    @ApiModelProperty(value = "提交时间")
    private LocalDateTime time;


}
