package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 年级
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserGrade implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotNull(message = "年级名称不能为空")
    @TableField(value = "grade_name")
    @ApiModelProperty(value = "年级名称")
    private String name;

    @ApiModelProperty(value = "学历id")
    private Integer educationId;

}
