package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserVideo对象", description="")
public class UserVideo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户观看视频数据主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer uid;

    @NotNull(message = "视频id不能为空")
    @ApiModelProperty(value = "视频id")
    private Integer vid;

    @NotNull(message = "播放起点为空")
    @ApiModelProperty(value = "开始播放时间点")
    private Date start;

    @ApiModelProperty(value = "播放时长单位s")
    private Integer length;

    @NotNull(message = "播放结束点为空")
    @ApiModelProperty(value = "结束播放时间点")
    private Date end;


}
