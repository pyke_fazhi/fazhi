package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 21/08/2021 8:29 PM
 */

@Data
@Accessors(chain = true)
public class LiveRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "直播开始时间")
    private Date liveStart;

    @ApiModelProperty(value = "直播结束时间")
    private Date liveEnd;

    @ApiModelProperty(value = "点赞数")
    private Integer like;

    @ApiModelProperty(value = "直播间id")
    private Integer liveId;

}
