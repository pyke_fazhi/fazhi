package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Examination对象", description="")
public class Examination implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试题id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotNull(message = "题型不能为空")
    @ApiModelProperty(value = "题型")
    private Integer typeId;

    @NotNull(message = "答案不能为空")
    @ApiModelProperty(value = "答案")
    private String answer;

    @ApiModelProperty(value = "答案解析,最多127字")
    private String analysis;

    @ApiModelProperty(value = "导入时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty(value = "随机组题时，是否会进入筛选")
    private Integer deleted;

    @NotNull(message = "题目不能为空")
    @ApiModelProperty("考试题目")
    private String title;

    @ApiModelProperty("单元id")
    private Integer unitId;

    @ApiModelProperty("章节id")
    private Integer chapterId;

    @ApiModelProperty("教师id")
    private Integer teacherId;


}
