package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserExaminationScore对象", description="")
public class UserExaminationScore implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户统考成绩数据主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "统考id")
    private Integer rid;

    @ApiModelProperty(value = "题型id，比如单选多选")
    private Integer tid;

    @ApiModelProperty(value = "正确题数")
    private Integer correct;

    @ApiModelProperty(value = "错误题数")
    private Integer error;

    @ApiModelProperty(value = "正确率")
    private Double accuracy;

    @ApiModelProperty(value = "该题总得分")
    private Integer score;


}
