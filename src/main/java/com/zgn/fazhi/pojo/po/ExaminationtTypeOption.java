package com.zgn.fazhi.pojo.po;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 考试/练习记录 自动组题配置
 * @author ydw
 */
@Data
@Accessors(chain = true)
public class ExaminationtTypeOption implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "单元id")
    private Integer unitId;

    @ApiModelProperty(value = "章节id")
    private Integer chapter;

    @ApiModelProperty(value = "题型id")
    private Integer type;

    @ApiModelProperty(value = "题数")
    private Integer number;

}
