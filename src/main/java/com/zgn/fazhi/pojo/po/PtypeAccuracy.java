package com.zgn.fazhi.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="PtypeAccuracy对象", description="")
public class PtypeAccuracy implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "练习题类型数据主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题型id，比如单选多选")
    private Integer pid;

    @ApiModelProperty(value = "正确数量")
    private Integer correct;

    @ApiModelProperty(value = "错误数量")
    private Integer error;

    @ApiModelProperty(value = "正确率")
    private Double accuracy;


}
