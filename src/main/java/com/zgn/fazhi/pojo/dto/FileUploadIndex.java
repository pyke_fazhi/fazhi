package com.zgn.fazhi.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 19/08/2021 3:55 PM
 */

@Data
@Accessors(chain = true)
public class FileUploadIndex implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 上传id
     */
    private String uploadId;

    /**
     * 上传路径
     */
    private String uploadPath;

}
