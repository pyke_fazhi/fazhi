package com.zgn.fazhi.exception;

import lombok.Data;

/**
 * 自定义异常
 */
public class RestException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private Integer errorCode;
    private String errorMessage;
    private Object date;

    public RestException(Integer errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public RestException(Integer errorCode, String errorMessage, Object date) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.date = date;
    }

    public Object getDate() {
        return date;
    }

    public void setDate(Object date) {
        this.date = date;
    }


    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
