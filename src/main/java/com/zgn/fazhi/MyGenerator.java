package com.zgn.fazhi;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
//import com.baomidou.mybatisplus.generator.AutoGenerator;
//import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
//import com.baomidou.mybatisplus.generator.config.GlobalConfig;
//import com.baomidou.mybatisplus.generator.config.PackageConfig;
//import com.baomidou.mybatisplus.generator.config.StrategyConfig;
//import com.baomidou.mybatisplus.generator.config.po.TableFill;
//import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.ArrayList;
/**
 * 代码生成器
 */
//public class MyGenerator {
//    public static void main(String[] args) {
//        AutoGenerator mpg = new AutoGenerator();
//        // 全局配置
//        GlobalConfig gc = new GlobalConfig();
//        //改成自己的项目路径
//        String projectPath = System.getProperty("user.dir");
//        gc.setOutputDir(projectPath+"/src/main/java");
//        gc.setAuthor("wt");
//        gc.setFileOverride(false);// 是否覆盖同名文件，默认是false
//        gc.setServiceName("%sService");//去Service的I前缀
//        gc.setSwagger2(true);
//        mpg.setGlobalConfig(gc);
//
//        // 数据源配置
//        DataSourceConfig dsc = new DataSourceConfig();
//        dsc.setDbType(DbType.MYSQL);
//        //数据源参数改一下
//        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
//        dsc.setUsername("root");
//        dsc.setPassword("root");
//        dsc.setUrl("jdbc:mysql://localhost:3306/fazhi?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT");
//        mpg.setDataSource(dsc);
//
//
//        // 策略配置
//        StrategyConfig strategy = new StrategyConfig();
//        strategy.setNaming(NamingStrategy.underline_to_camel);// 表名生成策略
//        strategy.setEntityLombokModel(true); // 启用lombok增加实体类的get，set方法简化代码；如果不启用可以改为false
//        strategy.setInclude("appointment","appointment_content","carousel","etype_score","evaluate",
//                "examination","examination_answer","examination_record","file","general_examination",
//                "grade","interval","pgrade_accouracy","picture","picture_info","picture_type","practice",
//                "practice_answer","ptype_accuracy","rank","role","sign_in","subject_type","test_accuracy",
//                "user_examination","user_examination_score","user_file","user_info","user_practice","user_time",
//                "user_video","video","video_type","visit_content","web_info","wrong","welcome_page"); // 需要生成的表
//
//        TableFill create_time = new TableFill("create_time", FieldFill.INSERT);//设置时的生成策略
//        ArrayList<TableFill> list = new ArrayList<>();
//        list.add(create_time);
//        strategy.setTableFillList(list);
//
//        //乐观锁
//        strategy.setVersionFieldName("version");
//        strategy.setRestControllerStyle(true);
//        strategy.setControllerMappingHyphenStyle(true);//localhost:8080/hello_id_1
//        mpg.setStrategy(strategy);
//
//        // 包配置
//        PackageConfig pc = new PackageConfig();
//        pc.setParent("com.zgn.fazhi");
//        pc.setEntity("pojo");
//        pc.setMapper("mapper");
//        pc.setService("service");
//        pc.setController("controller");
//        mpg.setPackageInfo(pc);
//
//        // 执行生成
//        mpg.execute();
//    }
//
//}
