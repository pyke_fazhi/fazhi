package com.zgn.fazhi.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.zgn.fazhi.pojo.po.Chapter;
import com.zgn.fazhi.pojo.po.SubjectType;
import com.zgn.fazhi.pojo.po.Unit;
import com.zgn.fazhi.service.ChapterService;
import com.zgn.fazhi.service.SubjectTypeService;
import com.zgn.fazhi.service.UnitService;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 检测excel表格是否正确
 *
 * @author ydw
 */

@Slf4j
public class BeforeExaminationListener extends AnalysisEventListener<Map<Integer, String>> {

    private SubjectTypeService subjectTypeService;

    private RedisUtil redisUtil;

    private UnitService unitService;

    private ChapterService chapterService;

    private Integer uid;
    
    /**
     * 每隔10条存储数据库,然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 10;
    Map<Integer, String> headMap = new HashMap<>();
    List<Map<Integer, String>> list = new ArrayList<Map<Integer, String>>();

    /**
     * 构造方法,由于当前类无法被spring托管,所以使用注入方式注入service
     *
     * @param subjectTypeService 查询题型service
     * @param redisUtil          redis工具类
     */
    public BeforeExaminationListener(SubjectTypeService subjectTypeService, RedisUtil redisUtil, UnitService unitService, ChapterService chapterService, Integer uid) {
        this.subjectTypeService = subjectTypeService;
        this.redisUtil = redisUtil;
        this.unitService = unitService;
        this.chapterService = chapterService;
        this.uid = uid;
    }

    /**
     * 检查每一条数据的合法性
     */
    @SneakyThrows
    @Override
    public void invoke(Map<Integer, String> data, AnalysisContext context) {
        log.info("解析到一条数据:{}", data);
        list.add(data);
        // 每5条记录校验一次合法性
        if (list.size() >= BATCH_COUNT) {
            checkList(list);
            // 存储完成清理 list
            list.clear();
        }
    }

    @SneakyThrows
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        checkList(list);
        System.out.println("打印头" + headMap);
        log.info("所有的数据存储完毕!" + list.size());
    }

    /**
     * 检查数据合法性
     *
     * @param list 数据数组
     */
    private void checkList(List<Map<Integer, String>> list) {
        System.out.println("校验中 : " + list);
        // 从数据库中查询所有的题型和等级，校验是否合法
        List<SubjectType> subjectTypes = subjectTypeService.getAllSubjectType();
        // 遍历该10条数据
        String key = "exa:" + uid;
        out:
        for (Map<Integer, String> map : list) {
            // 判断是否是数据库已有题型
            Boolean isType = false;
            for (SubjectType subjectType : subjectTypes) {
                if (!(StringUtils.isEmpty(subjectType) && map.isEmpty())) {
                    if (subjectType.getId().toString().equals(map.get(1))) {
                        // 如果在题型范围内
                        isType = true;
                    }
                }
            }
            if (!isType) {
                // 如果不在题型范围内，将该数据放入redis缓存中,用list + string
                log.info("不在题型范围内: {}", map.toString());
                redisUtil.lSet(key, map);
                continue out;
            }

            // excel单元标题
            String title = map.get(2);
            // 标题是否合法
            Integer uid = -1;
            // 查询所有单元
            List<Unit> allUnit = unitService.findAllUnit();
            for (Unit unit : allUnit) {
                if (unit.getTitle().equals(title)) {
                    uid = unit.getId();
                }
            }
            if (uid == -1) {
                // 将当前考题放入redis中返回前端
                log.info(map.toString());
                redisUtil.lSet(key, map);
                continue out;
            }

            // excel章节标题
            String chapter = map.get(3);
            // 判单章节是否存在
            Boolean ifChapter = false;
            // 通过单元id查询所有章节
            List<Chapter> chapterList = chapterService.getAllByUid(uid);
            for (Chapter chapter1 : chapterList) {
                if (chapter1.getTitle().equals(chapter)) {
                    ifChapter = true;
                }
            }
            if (!ifChapter) {
                // 将当前考题放入redis中返回前端
                log.info(map.toString());
                redisUtil.lSet(key, map);
            }

        }


    }

    /**
     * 读取表头数据
     * 这里会一行行的返回头
     * 监听器只需要重写这个方法就可以读取到头信息
     *
     * @param headMap 表头map
     */
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        this.headMap = headMap;
    }

}
