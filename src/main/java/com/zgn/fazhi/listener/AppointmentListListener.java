package com.zgn.fazhi.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.zgn.fazhi.pojo.vo.UserExcel;
import com.zgn.fazhi.service.AppointmentListService;
import com.zgn.fazhi.service.ExaminationService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 21/08/2021 11:43 AM
 */

@Slf4j
public class AppointmentListListener extends AnalysisEventListener<Map<Integer, String>> {

    private AppointmentListService appointmentListService;

    // 预约id
    private Integer aid;

    /**
     * 每隔10条存储数据库,然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 10;
    Map<Integer,String> headMap = new HashMap<>();
    List<Map<Integer, String>> list = new ArrayList<Map<Integer, String>>();

    @SneakyThrows
    @Override
    public void invoke(Map<Integer, String> data, AnalysisContext context) {
        log.info("解析到一条数据:{}", data);
        list.add(data);
        // 每5条记录刷新一次
        if (list.size() >= BATCH_COUNT) {
            saveData(list);
            // 存储完成清理 list
            list.clear();
        }
    }

    public AppointmentListListener(AppointmentListService appointmentListService, Integer aid) {
        this.appointmentListService = appointmentListService;
        this.aid = aid;
    }

    @SneakyThrows
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        saveData(list);
        System.out.println("打印头"+headMap);
        log.info("所有的数据存储完毕!"+list.size());

    }

    /**
     * 加上存储数据库
     */
    private void saveData(List<Map<Integer, String>> list) {
        log.info("{}条数据，开始存储数据库！长度为", list.size());
        // 老师上传考题到题库中
        appointmentListService.addList(list, aid);
    }

    /**
     * 读取表头数据
     * 这里会一行行的返回头
     * 监听器只需要重写这个方法就可以读取到头信息
     *
     * @param headMap 表头map
     */
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        this.headMap = headMap;
    }

}
