package com.zgn.fazhi.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zgn.fazhi.service.ExaminationService;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

/**
 * 直接用map接收数据
 *
 * @author ydw
 */
public class TeacherDataListener extends AnalysisEventListener<Map<Integer, String>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TeacherDataListener.class);

    private ExaminationService examinationService;

    private Integer tid;

    /**
     * 每隔10条存储数据库，实际使用中可以3000条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 10;
    Map<Integer,String> headMap = new HashMap<>();
    List<Map<Integer, String>> list = new ArrayList<Map<Integer, String>>();

    @SneakyThrows
    @Override
    public void invoke(Map<Integer, String> data, AnalysisContext context) {
        LOGGER.info("解析到一条数据:{}", data);
        // 每5条记录刷新一次
        if (list.size() >= BATCH_COUNT) {
            saveData(list);
            // 存储完成清理 list
            list.clear();
        }
        list.add(data);
    }


    public TeacherDataListener(ExaminationService examinationService, Integer tid) {
        this.examinationService = examinationService;
        this.tid = tid;
    }

    @SneakyThrows
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        saveData(list);
        System.out.println("打印头"+headMap);
        LOGGER.info("所有的数据存储完毕!"+list.size());

    }

    /**
     * 加上存储数据库
     */
    private void saveData(List<Map<Integer, String>> list) {
        LOGGER.info("{}条数据，开始存储数据库！长度为", list.size());
        examinationService.addExaminationListTeacher(list, tid);
    }

    /**
     * 读取表头数据
     * 这里会一行行的返回头
     * 监听器只需要重写这个方法就可以读取到头信息
     *
     * @param headMap 表头map
     */
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        this.headMap = headMap;
    }

}
