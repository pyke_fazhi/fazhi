package com.zgn.fazhi.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.zgn.fazhi.pojo.po.*;
import com.zgn.fazhi.pojo.vo.UserExcel;
import com.zgn.fazhi.service.*;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 检测excel表格是否正确
 *
 * @author ydw
 */

public class BeforeUserInfoListener extends AnalysisEventListener<UserExcel> {

    private Integer uid;

    private static final Logger LOGGER = LoggerFactory.getLogger(BeforeUserInfoListener.class);
    /**
     * 每隔10条存储数据库，实际使用中可以3000条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 10;
    Map<Integer, String> headMap = new HashMap<>();
    List<UserExcel> list = new ArrayList();
    private SchoolService schoolService;
    private RoleService roleService;
    private RedisUtil redisUtil;
    UserGradeService userGradeService;
    ClassInfoService classInfoService;


    public BeforeUserInfoListener(SchoolService schoolService, RoleService roleService,
                                  RedisUtil redisUtil, Integer uid, UserGradeService userGradeService,
                                  ClassInfoService classInfoService) {
        this.schoolService = schoolService;
        this.roleService = roleService;
        this.redisUtil = redisUtil;
        this.uid = uid;
        this.userGradeService = userGradeService;
        this.classInfoService = classInfoService;
    }


    /**
     * 检查每一条数据的合法性
     */
    @SneakyThrows
    @Override
    public void invoke(UserExcel data, AnalysisContext context) {
        LOGGER.info("解析到一条数据:{}", data);
        // 每10条记录校验一次合法性
        list.add(data);
        if (list.size() >= BATCH_COUNT) {
            checkList(list);
            // 存储完成清理 list
            list.clear();
        }
    }

    @SneakyThrows
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        checkList(list);
        System.out.println("打印头" + headMap);
        LOGGER.info("所有的数据校验完毕!" + list.size());
    }

    /**
     * 检查数据合法性
     */
    private void checkList(List<UserExcel> userExcelList) {
        LOGGER.info("执行checkList校验用户数据中 : " + list);
        // 从数据库中查询所有的角色和学校，校验是否合法
        // 获取所有角色
        List<Role> roleList = roleService.getAllRole();
        List<String> roleNameList = roleList.stream().map(Role::getNameZh).collect(Collectors.toList());
        //  角色只能为数据库中的角色
        String key = "exa:" + uid;
        // 遍历该10条数据
        for (UserExcel userExcel : userExcelList) {
            LOGGER.info("开始校验");
            String role = userExcel.getRole();
            // 校验角色不为空且为合法的角色名
            LOGGER.info("userinfo" + userExcel);
            LOGGER.info("role :" + role);
            if (StringUtils.isEmpty(role) || !roleNameList.contains(role)){
                LOGGER.info(roleNameList.toString());
                // 添加的redis失败缓存中
                redisUtil.lSet(key, userExcel);
                LOGGER.info("数据错误 1");
                continue;
            }
            // 如果角色名不为学生，则跳过
            if ("学生".equals(role)){
                // 如果为学生，需要校验学校、年级、班级
                // 获取所有学校
                String school = userExcel.getSchool();
                String grade = userExcel.getGrade();
                if (StringUtils.isEmpty(school) || grade.isEmpty() || userExcel.getClassName().isEmpty()){
                    redisUtil.lSet(key, userExcel);
                    LOGGER.info("数据为空");
                    continue;
                }
                List<School> allSchool = schoolService.getAllSchool();
                List<String> schoolNameList = allSchool.stream().map(School::getSchoolName).collect(Collectors.toList());
                // 判断年级是否存在
                List<UserGrade> allUserGrade = userGradeService.getAllUserGrade();
                List<String> gradeNameList = allUserGrade.stream().map(UserGrade::getName).collect(Collectors.toList());
                if (!schoolNameList.contains(school) || !gradeNameList.contains(grade)){
                    redisUtil.lSet(key, userExcel);
                    LOGGER.info("数据错误 2");
                }
                // 通过学校和年级查找所有班级，查看是否有该班级
                List<ClassInfo> classInfoList = classInfoService.getAllClassBySnameAndGname(school, grade);
                List<String> classNameList = classInfoList.stream().map(ClassInfo::getName).collect(Collectors.toList());
                if (!classNameList.contains(userExcel.getClassName())){
                    redisUtil.lSet(key, userExcel);
                    LOGGER.info("数据错误 3");
                }
            }
        }

    }

    /**
     * 读取表头数据
     * 这里会一行行的返回头
     * 监听器只需要重写这个方法就可以读取到头信息
     *
     * @param headMap 表头map
     */
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        this.headMap = headMap;
    }

}
