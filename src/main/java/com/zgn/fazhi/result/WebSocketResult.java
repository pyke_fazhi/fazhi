package com.zgn.fazhi.result;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

/**
 * @author ydw
 * @version 1.0
 * @description websocket信息封装类
 * @date 22/08/2021 11:16 AM
 */

@Data
@Accessors(chain = true)
public class WebSocketResult {

    /**
     * 返回数据类型
     * 0: 接收其他用户的信息
     * 1: 用户进入退出直播间,直播间名单更新
     * 2: 心跳检测
     */
    private Integer messageType;

    /**
     * 用户群发信息 / 用户进入退出直播间信息
     */
    private String message;

    /**
     * 消息的发送者uid
     */
    private Integer uid;

    /**
     * 用户名单列表
     */
    private Set<String> list;

}
