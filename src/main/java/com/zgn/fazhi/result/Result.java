package com.zgn.fazhi.result;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Result {

    //状态码
    private int code;

    //返回信息
    private String message;

    //返回数据
    private Object data;

}
