package com.zgn.fazhi.ws;

import com.zgn.fazhi.service.UserInfoService;
import com.zgn.fazhi.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 直播间即时通信 websoket
 * 已停用，使用netty版本的websocket
 */
@Slf4j
@ServerEndpoint(value = "/ws/live/{roomId}", configurator = GetUserInfoConfigurator.class)
public class LiveEndopint {

    /**
     * 使用map来收集session，key为roomName，value为同一个房间的用户集合
     * concurrentMap的key不存在时报错，不是返回null
     *  房间id : 房间用户列表
     */
    private static final Map<String, Set<Session>> ROOMS = new ConcurrentHashMap();

    /**
     * 用来给客户端发送消息
     */
    private Session session;

    /**
     * 存放当前用户名
     */
    private String name;

    private static UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService){
        LiveEndopint.userInfoService = userInfoService;
    }

    /**
     * 建立连接触发事件
     *
     * @param roomId  房间id
     * @param session 用户session
     */
    @OnOpen
    public void connect(@PathParam("roomId") String roomId, Session session, EndpointConfig endpointConfig) throws Exception {
        try {
            this.name = userInfoService.findByUid(
                    JwtUtils.getUidByToken(
                            endpointConfig
                                    .getUserProperties()
                                    .get("token")
                                    .toString()))
                    .getName();
        } catch (NullPointerException ignored){}
        this.session = session;

        // 将session按照房间名来存储，将各个房间的用户隔离
        if (!ROOMS.containsKey(roomId)) {
            // 创建房间不存在时，创建房间
            Set<Session> room = new HashSet<>();
            // 添加用户
            room.add(session);
            ROOMS.put(roomId, room);
        } else {
            // 房间已存在，直接添加用户到相应的房间
            ROOMS.get(roomId).add(session);
        }
        String msg = "用户" + name + "进入房间";
        // 对该房间进行广播
        broadcast(roomId, msg);
        log.info(msg);
    }

    /**
     * 客户端断开连接
     *
     * @param roomId  房间id
     * @param session 用户session
     */
    @OnClose
    public void disConnect(@PathParam("roomId") String roomId, Session session) {
        ROOMS.get(roomId).remove(session);
        log.info("用户{}退出直播间", name);
    }


    /**
     * 用户推送信息到直播间
     *
     * @param roomId  房间id
     * @param msg     消息
     * @param session 用户session
     */
    @OnMessage
    public void receiveMsg(@PathParam("roomId") String roomId, String msg, Session session) {
        // 此处应该有html过滤
        msg = name + ":" + msg;
        log.info("向房间号:{}推送消息:{}", roomId, msg);
        // 接收到信息后进行广播
        try {
            broadcast(roomId, msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 按照房间id进行广播
     *
     * @param roomId 房间id
     * @param msg    消息
     * @throws Exception
     */
    public static void broadcast(String roomId, String msg) throws Exception {
        for (Session session : ROOMS.get(roomId)) {
            session.getBasicRemote().sendText(msg);
        }
    }

}
