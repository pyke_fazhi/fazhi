package com.zgn.fazhi.ws;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.result.WebSocketResult;
import com.zgn.fazhi.service.WebSocketService;
import com.zgn.fazhi.utils.JwtUtils;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.yeauty.annotation.*;
import org.yeauty.pojo.Session;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author ydw
 * @version 1.0
 * @description 基于Netty的websocket服务端
 * @date 18/08/2021 2:56 PM
 */

@Component
@Slf4j
@ServerEndpoint(path = "${ws.path}", port = "${ws.port}")
public class NettyWebsocketEndpoint {

    /**
     * 使用map来收集session，key为roomName，value为同一个房间的用户集合
     * concurrentMap的key不存在时报错，不是返回null
     * 房间id : ( 用户名 : 用户session )
     */
    private static final Map<String, Map<String, Session>> ROOMS = new ConcurrentHashMap();

    /**
     * 当前用户session
     */
    private Session session;

    private static WebSocketService webSocketService;

    @Autowired
    public void setService(WebSocketService webSocketService) {
        NettyWebsocketEndpoint.webSocketService = webSocketService;
    }

    /**
     * 当前用户对象
     */
    private UserInfo userInfo;

    /**
     * 所在房间号
     */
    private String roomId;

    /**
     * 进入时间
     */
    private Date start;

    /**
     * 心跳检测
     */
    private static final String HEART_BEAT = "PING";

    /**
     * 客户端和服务端握手前
     *
     * @param headers 请求头
     */
    @BeforeHandshake
    public void handshake(HttpHeaders headers) {
        try {
            userInfo = webSocketService.getUser(JwtUtils.getUidByToken(headers.get(JwtUtils.AUTH_HEADER)));
        } catch (NullPointerException e) {
            userInfo.setName("游客" + ThreadLocalRandom.current().nextInt(9999));
        }
    }

    /**
     * 握手成功后，连接websocket
     *
     * @param session 用户session
     */
    @OnOpen
    public void onOpen(Session session, @PathVariable String roomId) {
        // 记录进入时间
        start = new Date();
        this.session = session;
        this.roomId = roomId;
        // 将用户session按照房间号放入ROOMS中
        putRoom(userInfo.getName());
        // 广播进入直播间信息
        systemMessage(true);
    }

    /**
     * 将用户session按照房间号放入ROOMS中
     *
     * @param name 用户名 作为key
     */
    private void putRoom(String name) {
        // 判断有无该直播间的map
        if (!ROOMS.containsKey(roomId)) {
            // 使用LinkedHashMap保证元素不重复且有序
            Map<String, Session> user = new LinkedHashMap<>();
            // 添加用户
            user.put(name, session);
            ROOMS.put(roomId, user);
        } else {
            // 房间已存在，直接添加用户到相应的房间
            ROOMS.get(roomId).put(name, session);
        }
        log.info("用户 {} 进入房间号: {}", userInfo.getName(), roomId);
    }

    /**
     * 客户端关闭ws事件
     *
     * @param session 用户session
     */
    @OnClose
    public void onClose(Session session) {
        Map<String, Session> stringSessionMap = ROOMS.get(roomId);
        // 将用户移除map
        stringSessionMap.remove(userInfo.getName());
        // 广播退出直播间信息
        systemMessage(false);
        // 判断map是否为空，为空则删除key
        if (stringSessionMap.isEmpty()) {
            ROOMS.remove(roomId);
        }
        // 保存用户在线时间
        Boolean ifSave = webSocketService.saveOnLineTime(roomId, userInfo.getId(), start);
        if (!ifSave){
            log.info("用户在线时间过短，上传在线时间失败!");
        }
        log.info("用户{}退出直播间: {}", userInfo.getName(), roomId);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        throwable.printStackTrace();
    }

    @OnMessage
    public void onMessage(Session session, String msg) {
        // 判断是否为心跳
        if (HEART_BEAT.equals(msg)) {
            session.sendText(JSON.toJSONString(new WebSocketResult().setMessageType(2)));
        } else {
            WebSocketResult webSocketResult = new WebSocketResult();
            webSocketResult
                    .setMessage(StrUtil.format("{}: {}", userInfo.getName(), msg))
                    .setList(getAllByRoomId(roomId))
                    .setMessageType(0)
                    .setUid(userInfo.getId());
            log.info("向房间号:{}推送消息:{}", roomId, webSocketResult);
            // 接收到信息后进行广播
            broadcast(webSocketResult, roomId);
        }
    }

    /**
     * 当接收到二进制消息时，对该方法进行回调
     *
     * @param session 用户session
     * @param bytes   二进制消息
     */
    @OnBinary
    public void onBinary(Session session, byte[] bytes) {
        for (byte b : bytes) {
            System.out.println(b);
        }
        session.sendBinary(bytes);
    }

    /**
     * 当接收到Netty的事件时，对该方法进行回调
     *
     * @param session 用户session
     * @param evt     事件
     */
    @OnEvent
    public void onEvent(Session session, Object evt) {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent idleStateEvent = (IdleStateEvent) evt;
            switch (idleStateEvent.state()) {
                case READER_IDLE:
                    log.info("read idle");
                    break;
                case WRITER_IDLE:
                    log.info("write idle");
                    break;
                case ALL_IDLE:
                    log.info("all idle");
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 推送系统信息到该房间号
     *
     * @param isEnter 是否为进入房间
     */
    private void systemMessage(boolean isEnter) {
        String message;
        String currentName = userInfo.getName();
        if (isEnter) {
            message = StrUtil.format("用户{}进入直播间", currentName);
        } else {
            message = StrUtil.format("用户{}退出直播间", currentName);
        }
        // 推送系统信息(数据包括用户进入/退出直播间, 以及进入/退出后的在线名单)
        WebSocketResult webSocketResult = new WebSocketResult();
        webSocketResult.setMessageType(1)
                .setMessage(message)
                .setList(getAllByRoomId(roomId));
        // 对在直播间的用户广播系统信息
        broadcast(webSocketResult, roomId);
    }

    /**
     * 按照房间id进行广播
     *
     * @param webSocketResult 数据信息
     * @param roomId          房间号
     */
    private void broadcast(WebSocketResult webSocketResult, String roomId) {
        // 通过房间号获取map, 再遍历map的key获取session发送消息
        Map<String, Session> room = ROOMS.get(roomId);
        if (!room.isEmpty()) {
            room.keySet().forEach(name -> room.get(name).sendText(JSON.toJSONString(webSocketResult)));
        }
    }

    /**
     * 通过房间号id获取在房间的用户名
     *
     * @param roomId 房间id
     * @return 用户名集合
     */
    private Set<String> getAllByRoomId(String roomId) {
        return ROOMS.get(roomId).keySet();
    }


}
