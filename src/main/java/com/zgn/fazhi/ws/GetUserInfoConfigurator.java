package com.zgn.fazhi.ws;

import com.zgn.fazhi.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;

import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;
import java.util.List;
import java.util.Map;

@Slf4j
public class GetUserInfoConfigurator extends ServerEndpointConfig.Configurator {

    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        // 获取请求头 token
        try {
            String token = request.getHeaders().get(JwtUtils.AUTH_HEADER).get(0);
            Map<String, Object> userProperties = sec.getUserProperties();
            userProperties.put("token", token);
        } catch (NullPointerException ignored){}
    }
}
