package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.vo.ExaPermission;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.ExaminationPermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 考试/练习权限控制
 */
@Api(tags = "考试/练习开放限制接口")
@RestController
public class ExaminationPermissionController {

    @Autowired
    private ExaminationPermissionService permissionService;

    @ApiOperation("通过id对考试或练习添加开放人群")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/addExaPermission")
    public Result addExaPermission(@Valid @RequestBody ExaPermission exaPermission) {
        permissionService.addExaPermission(exaPermission);
        return ResultFactory.buildSuccessResult("操作成功");
    }

    @ApiOperation("通过考试练习id查询开放权限")
    @GetMapping("/admin/getExaPermission/{rid}")
    public Result getExaPermission(@PathVariable Integer rid) {
        return ResultFactory.buildSuccessResult(permissionService.getExaPermissionByRid(rid));
    }

    @ApiOperation("批量删除权限")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/delExaPermission")
    public Result delExaPermission(@RequestBody List<Integer> idList) {
        return ResultFactory.buildSuccessResult(permissionService.delExaPermissionById(idList));
    }

}
