package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.WebConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@Api(tags = "程序设置")
@RestController
public class WebConfigController {

    @Autowired
    private WebConfigService webConfigService;

    @ApiOperation("更新用户注册时生成的用户名长度, 默认为6")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/updateUsernameLength/{length}")
    public Result updateUsernameLength(@NotNull(message = "长度为空") @PathVariable Integer length){
        webConfigService.updateUsernameLength(length);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("获取当前注册时的用户名长度")
    @GetMapping("/admin/getUsernameLength")
    public Result getUsernameLength(){
        return ResultFactory.buildSuccessResult(webConfigService.getUserNameLength());
    }

}
