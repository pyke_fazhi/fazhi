package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.FileType;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.FileTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 文档分类
 * @author wt
 */
@Api(tags = "文档分类接口")
@RestController
public class FileTypeController {

    @Autowired
    FileTypeService fileTypeService;

    @ApiOperation("获取全部文档分类")
    @GetMapping("/api/getAllFileType")
    public Result getAll(){
        return ResultFactory.buildSuccessResult(fileTypeService.getAllFileType());
    }


    @ApiOperation("根据Id获取文档分类")
    @GetMapping("/api/getFileTypeById/{id}")
    public Result getById(@PathVariable Integer id){
        return ResultFactory.buildSuccessResult(fileTypeService.getAllFileTypeById(id));
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("增加文档分类")
    @PostMapping("/api/addFileType")
    public Result addFileType(@RequestBody FileType fileType){
        fileTypeService.addFileType(fileType);
        return ResultFactory.buildSuccessResult("增加文档分类成功");
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("删除文档分类")
    @GetMapping("/api/deleteFileType/{id}")
    public Result deleteFileType(@PathVariable Integer id){
        fileTypeService.deleteFileTypeById(id);
        return ResultFactory.buildSuccessResult("删除文档分类成功");
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("修改文档分类")
    @PostMapping("/api/updateFileType")
    public Result updateFileType(@RequestBody FileType fileType){
        fileTypeService.updateFileType(fileType);
        return ResultFactory.buildSuccessResult("修改文档分类成功");
    }
}
