package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Unit;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.UnitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 单元
 * @author ydw
 */
@Api(tags = "单元管理")
@RestController
public class UnitController {

    @Autowired
    private UnitService unitService;

    @ApiOperation("增加单元")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/admin/addUnit")
    public Result addUnit(@Valid @RequestBody Unit unit) {
        unitService.addUnit(unit);
        return ResultFactory.buildSuccessResult("添加成功");
    }

    @ApiOperation("删除单元")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/addUnit/{id}")
    public Result delUnit(@PathVariable Integer id) {
        unitService.delUnitById(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("修改单元")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/admin/updateUnit")
    public Result updateUnit(@RequestBody Unit unit) {
        unitService.updateById(unit);
        return ResultFactory.buildSuccessResult("更新成功");
    }

    @ApiOperation("查询所有单元")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/getUnit")
    public Result getUnit() {
        return ResultFactory.buildSuccessResult(unitService.findAllUnit());
    }

    @ApiOperation("通过id查询单元")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/getUnitById/{id}")
    public Result getUnitById(@PathVariable Integer id) {
        return ResultFactory.buildSuccessResult(unitService.findById(id));
    }



}
