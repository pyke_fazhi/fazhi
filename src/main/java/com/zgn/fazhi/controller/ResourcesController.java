package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Resources;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.ResourcesService;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "资源分类管理")
@RestController
public class ResourcesController {

    @Autowired
    private ResourcesService resourcesService;

    @GetMapping("/admin/getResourcesList")
    public Result getResourcesList(){
        return ResultFactory.buildSuccessResult(resourcesService.list());
    }

    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/admin/delResources/{id}")
    public Result delResources(@PathVariable Integer id){
        resourcesService.removeById(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/addResources")
    public Result addResources(@RequestBody @Valid Resources resources){
        resourcesService.save(resources);
        return ResultFactory.buildSuccessResult("添加成功");
    }

    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/updateResources")
    public Result updateResources(@RequestBody Resources resources){
        resourcesService.updateById(resources);
        return ResultFactory.buildSuccessResult("更新成功");
    }

}
