package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.SecretGuard;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.SecretGuardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author ydw
 * @version 1.0
 * @description 密保
 * @date 20/08/2021 11:43 AM
 */

@Api(tags = "密保管理")
@RestController
public class SecretGuardController {

    @Autowired
    private SecretGuardService secretGuardService;

    @ApiOperation("查询所有密保问题")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/api/getSecretGuard")
    public Result getSecretGuard() {
        return ResultFactory.buildSuccessResult(secretGuardService.list());
    }

    @ApiOperation("增加密保问题")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/api/saveSecretGuard")
    public Result addSecretGuard(@RequestBody @Valid SecretGuard secretGuard) {
        secretGuardService.save(secretGuard);
        return ResultFactory.buildSuccessResult("添加成功");
    }

    @ApiOperation("根据id删除密保问题")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/api/delSecretGuard")
    public Result delSecretGuard(@RequestParam("id") Integer id){
        secretGuardService.removeById(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("根据id更新密保问题")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/api/updateSecretGuard")
    public Result updateSecretGuard(@RequestBody @Valid SecretGuard secretGuard){
        secretGuardService.updateById(secretGuard);
        return ResultFactory.buildSuccessResult("更新成功");
    }

}
