package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.RankInfo;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.RankInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 用户等级表增删改查
 */
@Api(tags = "使用时长等级接口")
@RestController
public class RankController {

    @Autowired
    private RankInfoService rankInfoService;

    @ApiOperation("查询所有等级")
    @GetMapping("/api/getRank")
    public Result getRank() {
        return ResultFactory.buildSuccessResult(rankInfoService.getRank());
    }

    @ApiOperation("通过id删除等级")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/delRank/{id}")
    public Result delRankById(@PathVariable Integer id) {
        rankInfoService.deleteRank(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("通过id修改等级")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/updateRank")
    public Result updateRank(@Valid @RequestBody RankInfo rankInfo) {
        rankInfoService.updateRank(rankInfo);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("添加等级")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/addRank")
    public Result addRankById(@Valid @RequestBody RankInfo rankInfo) {
        rankInfoService.addRank(rankInfo);
        return ResultFactory.buildSuccessResult("添加成功");
    }

}
