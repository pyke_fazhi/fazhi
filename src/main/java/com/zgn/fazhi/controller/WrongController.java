package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Wrong;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.WrongService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 错题集管理模块
 *
 * @author ydw
 */
@Api(tags = "错题集管理")
@RestController
public class WrongController {

    @Autowired
    private WrongService wrongService;

    @ApiOperation("查询当前用户的错题集")
    @GetMapping("api/getWrong")
    public Result getWrong(@RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        return ResultFactory.buildSuccessResult(wrongService.findByUid(JwtUtils.getUidByToken(token)));
    }

    @ApiOperation("当前用户添加错题")
    @GetMapping("api/addWrong/{pid}")
    public Result addWrong(@RequestHeader(JwtUtils.AUTH_HEADER) String token, @PathVariable Integer pid) {
        // 获取当前用户id
        wrongService.addWrong(JwtUtils.getUidByToken(token), pid);
        return ResultFactory.buildSuccessResult("添加成功");

    }

    @ApiOperation("当前用户删除错题")
    @GetMapping("api/delWrong/{pid}")
    public Result delWrong(@RequestHeader(JwtUtils.AUTH_HEADER) String token, @PathVariable Integer pid) {
        // 获取当前用户id
        wrongService.delWrong(JwtUtils.getUidByToken(token), pid);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("查询某个用户的错题集")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("api/selectWrongByUid/{uid}")
    public Result selectWrongByUid(@PathVariable Integer uid){
        return ResultFactory.buildSuccessResult(wrongService.findByUid(uid));
    }

}
