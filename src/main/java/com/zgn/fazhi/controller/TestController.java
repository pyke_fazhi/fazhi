package com.zgn.fazhi.controller;


import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.result.Result;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.hibernate.validator.constraints.Length;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

@Api(tags = "测试接口")
@RestController
@Slf4j
public class TestController {

    @GetMapping("/admin/delete")
    public Result deleteArticle(@RequestBody UserInfo userInfo) {
        log.info("删除成功");
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @PostMapping("/read")
    public Result readArticle(@RequestBody @NotNull Integer id,
                              @RequestBody @Length(max = 10, min = 1) String name) {
        return ResultFactory.buildSuccessResult("查看成功");
    }


}
