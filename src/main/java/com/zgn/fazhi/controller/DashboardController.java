package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Role;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.RoleService;
import com.zgn.fazhi.service.UserInfoService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * 后台模块
 *
 * @author ydw
 */
@Api(tags = "后台操作接口")
@RestController
public class DashboardController {

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private RoleService roleService;

    @ApiOperation("后台登陆")
    @GetMapping("/api/adminLogin")
    public Result adminLogin(@RequestParam("username") @NotNull String username, @RequestParam("password") @NotNull String password, ServletResponse response) {

        // 查询用户名是否为管理员
        UserInfo userInfo = userInfoService.findByUsername(username);
        Integer roleId = userInfo.getRoleId();
        // 查找admin的角色id
        Role admin = roleService.findByName("admin");
        Role teacher = roleService.findByName("teacher");
        if (!admin.getId().equals(roleId) || !teacher.getId().equals(roleId)) {
            return ResultFactory.buildFailResult("账号或密码错误,请重新登陆");
        }
        // 如果是管理员，正常登陆
        // 获取当前用户主体
        Subject subject = SecurityUtils.getSubject();
        String msg = null;
        boolean loginSuccess = false;
        // 将用户名和密码封装成 UsernamePasswordToken 对象
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            subject.login(token);
            msg = "登录成功。";
            loginSuccess = true;
        } catch (UnknownAccountException uae) { // 账号不存在
            msg = "用户名与密码不匹配，请检查后重新输入！";
        } catch (IncorrectCredentialsException ice) { // 账号与密码不匹配
            msg = "用户名与密码不匹配，请检查后重新输入！";
        } catch (LockedAccountException lae) { // 账号已被锁定
            msg = "该账户已被锁定，如需解锁请联系管理员！";
        } catch (AuthenticationException ae) { // 其他身份验证异常
            msg = "登录异常，请联系管理员！";
        }

        if (loginSuccess) {
            // 若登录成功，签发 JWT token
            String jwtToken = JwtUtils.sign(userInfoService.findByUsername(username).getId().toString(), JwtUtils.SECRET);
            // 将签发的 JWT token 设置到 HttpServletResponse 的 Header 中
            ((HttpServletResponse) response).setHeader(JwtUtils.AUTH_HEADER, jwtToken);
            return ResultFactory.buildSuccessResult(msg);
        } else {
            return ResultFactory.buildFailResult(msg);
        }

    }


}
