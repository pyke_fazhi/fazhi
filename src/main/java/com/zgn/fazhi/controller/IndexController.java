package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.WebInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 首页模块
 *
 * @author ydw
 */
@Api(tags = "首页访问接口")
@RestController
public class IndexController {

    @Autowired
    private WebInfoService webInfoService;

    @ApiOperation("访问首页，访问+1，使用redis缓存完成")
    @GetMapping("/api/addView")
    public Result addView() {

       return ResultFactory.buildSuccessResult(webInfoService.incr());

    }

}
