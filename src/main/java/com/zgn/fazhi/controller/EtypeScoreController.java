package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.EtypeScore;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.EtypeScoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "考试练习记录题目题型分数配置")
@RestController
public class EtypeScoreController {

    @Autowired
    private EtypeScoreService etypeScoreService;

    @ApiOperation("修改配置")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/updateEtypeScore")
    public Result updateEtypeScore(@Valid @RequestBody EtypeScore etypeScore) {
        etypeScoreService.updateById(etypeScore);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("通过考试练习id查找题目配置")
    @GetMapping("/api/getAllEtypeScoreByRid/{rid}")
    public Result getAllEtypeScoreByRid(@PathVariable Integer rid){
        return ResultFactory.buildSuccessResult(etypeScoreService.findAllById(rid));
    }

    @ApiOperation("增加题目题型分数配置")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/addEtypeScore")
    public Result addEtypeScore(@RequestBody @Valid List<EtypeScore> etypeScores){
        etypeScoreService.saveBatch(etypeScores);
        return ResultFactory.buildSuccessResult("添加成功");
    }

    @ApiOperation("通过id删除")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/admin/delEtypeScoreById/{id}")
    public Result delEtypeScoreById(@PathVariable Integer id){
        etypeScoreService.removeById(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("通过考试记录id删除所有配置")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/admin/delEtypeScoreByRId/{id}")
    public Result delEtypeScoreByRId(@PathVariable Integer rid){
        etypeScoreService.delByEid(rid);
        return ResultFactory.buildSuccessResult("删除成功");
    }

}
