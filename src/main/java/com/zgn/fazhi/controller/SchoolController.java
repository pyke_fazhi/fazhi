package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.School;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.SchoolService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * 用户模块
 *
 * @author Y121
 */
@Api(tags = "学校管理")
@RestController
public class SchoolController {

    @Autowired
    private SchoolService schoolService;

    @ApiOperation("查询所有学校")
    @GetMapping("/api/getSchool")
    public Result getSchool() {
        return ResultFactory.buildSuccessResult(schoolService.getAllSchool());
    }

    @ApiOperation("通过id删除学校")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/getSchool/{id}")
    public Result delSchoolById(@PathVariable @NotNull Integer id) {
        schoolService.delById(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("通过id修改学校")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/admin/updateSchool")
    public Result updateSchool(@RequestBody @Valid School school) {
        schoolService.updateSchoolById(school);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("添加学校")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/admin/addSchool")
    public Result addSchool(@RequestBody @Valid School school) {
        schoolService.addSchool(school);
        return ResultFactory.buildSuccessResult("添加成功");
    }

}
