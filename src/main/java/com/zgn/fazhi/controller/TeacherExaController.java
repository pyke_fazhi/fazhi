package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Examination;
import com.zgn.fazhi.pojo.vo.ExaminationOption;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.ExaminationService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 教师题库
 */
@Api(tags = "教师题库接口")
@RestController
public class TeacherExaController {

    @Autowired
    private ExaminationService examinationService;

    @ApiOperation("查询当前教师的题库")
    @GetMapping("/api/getTeacherExa")
    public Result getTeacherExa(@RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        // 返回缺少选项数据
        return ResultFactory.buildSuccessResult(
                examinationService.selectAllByTid(JwtUtils.getUidByToken(token)));
    }

    @ApiOperation("教师删除考题")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/api/delTeacherExa/{eid}")
    public Result delTeacherExa(@RequestHeader(JwtUtils.AUTH_HEADER) String token, @PathVariable Integer eid) {
        examinationService.delTeacherExaById(eid, JwtUtils.getUidByToken(token));
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("教师向私有题库添加单个考题")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/api/addTeacherExa")
    public Result addTeacherExa(@RequestHeader(JwtUtils.AUTH_HEADER) String token, @RequestBody @Valid ExaminationOption examination) {
        examinationService.addExaminationTeacher(JwtUtils.getUidByToken(token), examination);
        return ResultFactory.buildSuccessResult("添加成功");
    }

    @ApiOperation("教师修改考题")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/api/updateTeacherExa")
    public Result updateTeacherExa(@RequestHeader(JwtUtils.AUTH_HEADER) String token, @Valid @RequestBody Examination examination) {
        examinationService.updateById(examination.setTeacherId(JwtUtils.getUidByToken(token)));
        return ResultFactory.buildSuccessResult("修改成功");
    }


}
