package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.ClassInfo;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.ClassInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 班级增删改查
 *
 * @author wt
 */
@Api(tags = "用户班级接口")
@RestController
public class ClassInfoController {

    @Autowired
    private ClassInfoService classInfoService;

    @ApiOperation("查询所有班级")
    @GetMapping("/api/getClassInfo")
    public Result getClassInfo() {
        return ResultFactory.buildSuccessResult(classInfoService.getAllClassInfo());
    }

    @ApiOperation("通过id删除班级")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/admin/getClassInfo/{id}")
    public Result delClassInfoById(@PathVariable Integer id) {
        classInfoService.delClassInfo(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("通过id修改班级")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/updateClassInfo")
    public Result updateClassInfo(@Valid @RequestBody ClassInfo classInfo) {
        classInfoService.updateClassInfoById(classInfo);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("添加班级")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/addClassInfo")
    public Result addClassInfoById(@Valid @RequestBody ClassInfo classInfo) {
        classInfoService.addClassInfo(classInfo);
        return ResultFactory.buildSuccessResult("添加成功");
    }

    @ApiOperation("通过学校年级id查询所有班级")
    @PostMapping("/api/getClassBy/{sid}/{gid}")
    public Result getClassBySidAndGid(@PathVariable Integer sid, @PathVariable Integer gid) {
        return ResultFactory.buildSuccessResult(classInfoService.getAllClassBySidGid(sid, gid));
    }

}
