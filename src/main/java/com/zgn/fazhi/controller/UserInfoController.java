package com.zgn.fazhi.controller;


import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.UserInfoService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Api(tags = "用户接口")
@RestController
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private JwtUtils jwtUtils;

    @ApiOperation("根据用户身份查找")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/api/userInfo/{rid}")
    public Result selectUser(@PathVariable Integer rid) {
        return ResultFactory.buildSuccessResult(userInfoService.selectRoleId(rid));
    }


    @ApiOperation("用户注册")
    @PostMapping("/api/userRegister")
    public Result userRegister(@Valid @RequestBody UserInfo userInfo) {
        return ResultFactory.buildSuccessResult(userInfoService.addUser(userInfo));
    }


    @ApiOperation("根据Id删除用户信息")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/api/deleteUser")
    public Result deleteId(@RequestParam(value = "id") String id) {
        String[] gpIds = id.split(",");
        List<Integer> list = new ArrayList<>();
        for (String str : gpIds) {
            list.add(Integer.parseInt(str));
        }
        userInfoService.batchDelete(list);
        return ResultFactory.buildSuccessResult("删除成功");
    }


    @ApiOperation("根据Id修改用户信息")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/api/updateUser")
    public Result updateUser(@RequestBody UserInfo userInfo) {
        userInfoService.updateUser(userInfo);
        return ResultFactory.buildSuccessResult("更新成功");
    }


    @ApiOperation("查询所有用户信息")
    @GetMapping("/api/findAllUser")
    public Result findAllUser() {
        return ResultFactory.buildSuccessResult(userInfoService.selectAll());
    }


    @ApiOperation("根据用户名显示")
    @GetMapping("/api/getUserByUsername")
    public Result getUserByUsername(@RequestBody String username) {
        return ResultFactory.buildSuccessResult(userInfoService.selectByUsername(username));
    }

    @ApiOperation("获取当前用户信息")
    @GetMapping("/api/getCurrentUserInfo")
    public Result getCurrentUserInfo(@RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        return ResultFactory.buildSuccessResult(userInfoService
                .selectByUsername(jwtUtils.getCurrentUserInfo(token)
                        .getUsername()));
    }

    @ApiOperation("用户修改密码")
    @PostMapping("/api/updatePassword")
    public Result getCurrentUserInfo(@RequestParam("password") @NotNull(message = "密码不能为空") String password,
                                     @RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        userInfoService.updatePassword(JwtUtils.getUidByToken(token), password);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("通过username获取密保问题")
    @GetMapping("/api/getQuestion/{username}")
    public Result getQuestion(@PathVariable String username) {
        return ResultFactory.buildSuccessResult(userInfoService.getQuestion(username));
    }

    @ApiOperation("根据用户名和密保答案找回密码")
    @PostMapping("/api/retrieve")
    public Result retrieve(@RequestParam("answer") String answer, @RequestParam("username") String username) {
        return ResultFactory.buildSuccessResult(userInfoService.retrieve(answer, username));
    }

    @ApiOperation("用户通过回执key修改密码")
    @PostMapping("/api/updatePasswordByKey")
    public Result updatePasswordByKey(@RequestParam("key") String key, @RequestParam("password") String password) {
        userInfoService.updatePasswordByKey(key, password);
        return ResultFactory.buildSuccessResult("修改成功");
    }

}