package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.UserInfoService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

@Api(tags = "登陆接口")
@RestController
@Slf4j
public class LoginController {

    @Autowired
    private UserInfoService userInfoService;

    @PostMapping(value = "/api/login")
    public Result userLogin(@RequestParam("username") String username, @RequestParam("password") String password,  ServletResponse response) {
        log.info("用户名: {}", username);
        log.info("密码: {}", password);
        // 获取当前用户主体
        Subject subject = SecurityUtils.getSubject();
        String msg = null;
        boolean loginSuccess = false;
        // 将用户名和密码封装成 UsernamePasswordToken 对象
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            subject.login(token);
            msg = "登录成功。";
            loginSuccess = true;
        } catch (UnknownAccountException uae) { // 账号不存在
            msg = "用户名与密码不匹配，请检查后重新输入！";
        } catch (IncorrectCredentialsException ice) { // 账号与密码不匹配
            msg = "用户名与密码不匹配，请检查后重新输入！";
        } catch (LockedAccountException lae) { // 账号已被锁定
            msg = "该账户已被锁定，如需解锁请联系管理员！";
        } catch (AuthenticationException ae) { // 其他身份验证异常
            msg = "登录异常，请联系管理员！";
        }
        if (loginSuccess) {
            // 若登录成功，签发 JWT token
            String jwtToken = JwtUtils.sign(userInfoService.findByUsername(username).getId().toString(), JwtUtils.SECRET);
            log.info("签名后获取该uid: {}", JwtUtils.getClaimFiled(jwtToken, "uid"));
            // 将签发的 JWT token 设置到 HttpServletResponse 的 Header 中
            ((HttpServletResponse) response).setHeader(JwtUtils.AUTH_HEADER, jwtToken);
            return ResultFactory.buildSuccessResult(msg);
        } else {
            return ResultFactory.buildFailResult(msg);
        }
    }
    @GetMapping("/api/logout")
    public Result logout() {

        return ResultFactory.buildSuccessResult("登出成功");

    }
}