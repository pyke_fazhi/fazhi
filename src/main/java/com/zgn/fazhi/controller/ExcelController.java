package com.zgn.fazhi.controller;

import com.alibaba.excel.EasyExcel;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.listener.*;
import com.zgn.fazhi.pojo.vo.UserExcel;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.*;
import com.zgn.fazhi.utils.ExcelUtils;
import com.zgn.fazhi.utils.JwtUtils;
import com.zgn.fazhi.utils.RedisUtil;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 导入导出Excel
 *
 * @author ydw
 */
@Api(tags = "excel操作接口")
@RestController
@Slf4j
public class ExcelController {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SubjectTypeService subjectTypeService;

    @Autowired
    private ExaminationService examinationService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private RoleService roleService;

    @Autowired
    private SchoolService schoolService;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private UserGradeService userGradeService;

    @PostMapping(value = "/api/uploadPractice", consumes = "multipart/*", headers = "content-type=multipart/form-data")
    @RequiresRoles(value = {"admin"})
    @ApiOperation("管理员上传考题excel")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "form", dataType = "_file", name = "上传文件", required = true)})
    public Result practiceFile(@ApiParam(value = "上传练习题", required = true) @RequestPart("file") MultipartFile file,
                               @RequestHeader(JwtUtils.AUTH_HEADER) String token) throws IOException {
        checkFile(file);
        Integer uid = JwtUtils.getUidByToken(token);
        // 校验
        EasyExcel.read(file.getInputStream(), new BeforeExaminationListener(subjectTypeService,
                redisUtil, unitService, chapterService, uid)).headRowNumber(1).sheet().doRead();
        // 查看是否有错误数据,有则直接返回前端
        ifError(uid);
        // 如果数据无误，写入数据库
        EasyExcel.read(file.getInputStream(), new ExaminationDataListener(examinationService))
                .headRowNumber(1).sheet().doRead();
        return ResultFactory.buildSuccessResult("数据无误，导入成功");
    }

    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @ApiOperation("教师上传题库excel")
    @PostMapping(value = "/api/uploadExamination", consumes = "multipart/*", headers = "content-type=multipart/form-data")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "form", dataType = "_file", name = "上传文件", required = true)})
    public Result examinationFile(@ApiParam(value = "上传考题", required = true) @RequestPart("file") MultipartFile file,
                                  @RequestHeader(JwtUtils.AUTH_HEADER) String token) throws IOException {
        // 导入后需要刷新每个级别、题型练习题缓存
        // 判断文件是否为空
        checkFile(file);
        Integer uid = JwtUtils.getUidByToken(token);
        // 校验考试数据
        EasyExcel.read(file.getInputStream(), new BeforeListener(subjectTypeService, redisUtil, uid))
                .headRowNumber(1).sheet().doRead();
        // 查看是否有错误数据,有则直接返回前端
        ifError(uid);
        // 如果数据无误，写入数据库
        EasyExcel.read(file.getInputStream(), new TeacherDataListener(examinationService, uid))
                .headRowNumber(1).sheet().doRead();
        return ResultFactory.buildSuccessResult("数据无误，导入成功");

    }

    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @ApiOperation("上传用户excel")
    @PostMapping(value = "/api/uploadUser", consumes = "multipart/*", headers = "content-type=multipart/form-data")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "form", dataType = "_file", name = "上传用户", required = true)})
    public void userFile(@ApiParam(value = "上传用户", required = true) @RequestPart("file") MultipartFile file,
                         @RequestHeader(JwtUtils.AUTH_HEADER) String token, HttpServletResponse response) throws IOException {
        // 判断文件是否为空
        checkFile(file);
        Integer uid = JwtUtils.getUidByToken(token);
        // 校验用户数据
        EasyExcel.read(file.getInputStream(), UserExcel.class, new BeforeUserInfoListener(schoolService, roleService,
                redisUtil, uid, userGradeService, classInfoService)).headRowNumber(1).sheet().doRead();
        // 查看是否有错误数据,有则直接返回前端
        ifError(uid);

        // 如果数据无误，写入数据库
        EasyExcel.read(file.getInputStream(), UserExcel.class, new UserInfoListener(userInfoService, uid)).headRowNumber(1).sheet().doRead();

        // 返回前端生成的用户名
        String key = "userExcel:" + uid;
        try {
            List<UserExcel> userExcelList = redisUtil.lGet(key, 0, -1);
            String fileName = "用户表";
            ExcelUtils.exportExcel(response, userExcelList, fileName);
        } finally {
            // 清理缓存
            redisUtil.del(key);
        }

    }

    /**
     * 校验文件是否为空，且redis是否挂掉
     *
     * @param file 上传的文件
     */
    public void checkFile(MultipartFile file) {
        // 判断文件是否为空
        if (file.isEmpty()) {
            System.out.println("文件为空");
            throw new RestException(400, "文件为空");
        }
        try {
            System.out.println("执行redis");
            redisTemplate.opsForValue().get("skr");
        } catch (RedisConnectionFailureException e) {
            log.error("插入练习题出现连接异常: " + e.toString());
            throw new RestException(400, "连接redis出现异常，请联系运维!");
        }
    }


    /**
     * 是否有不合法的数据
     */
    public void ifError(Integer uid) {
        String key = "exa:" + uid;
        if (redisUtil.lGetListSize(key) != 0) {
            try {
                throw new RestException(400, "数据有误", redisUtil.lGet(key, 0, -1));
            } finally {
                // 清除缓存
                redisUtil.del(key);
            }
        }
    }


}
