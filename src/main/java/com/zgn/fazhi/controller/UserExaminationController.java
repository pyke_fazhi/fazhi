package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Unit;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.UserExaminationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 用户考试/练习成绩查
 * @author ydw
 */
@Api(tags = "总体用户考试/练习成绩接口")
@RestController
public class UserExaminationController {

    @Autowired
    private UserExaminationService userExaminationService;

    @ApiOperation("通过考试/练习id获取该次练习考试的数据")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/admin/getExaData/{rid}")
    public Result getExaData(@PathVariable Integer rid) {
       return ResultFactory.buildSuccessResult(userExaminationService.getExaDataByRid(rid));
    }


}
