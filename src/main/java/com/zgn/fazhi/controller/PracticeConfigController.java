package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.PracticeConfig;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.PracticeConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 练习题的配置
 * 需求更改，已废除
 */

public class PracticeConfigController {

    @Autowired
    private PracticeConfigService practiceConfigService;

    @ApiOperation("增加配置")
    @PostMapping("/api/addPraConfig")
    public Result addPraConfig(@RequestBody PracticeConfig practiceConfig){
        practiceConfigService.addPraConfig(practiceConfig);
        return ResultFactory.buildSuccessResult("添加成功");
    }

    @ApiOperation("删除配置")
    @PostMapping("/api/delPraConfig/{id}")
    public Result delPraConfig(@PathVariable Integer id){
        practiceConfigService.delPraConfig(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("修改配置")
    @PostMapping("/api/updatePraConfig")
    public Result updatePraConfig(@RequestBody PracticeConfig practiceConfig){
        practiceConfigService.addPraConfig(practiceConfig);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("查询配置")
    @GetMapping("/api/getPraConfig")
    public Result getPraConfig(){
        return ResultFactory.buildSuccessResult(practiceConfigService.getAllConfig());
    }

}
