package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.UserGrade;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.UserGradeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 年级增删改查
 *
 * @author wt
 */
@Api(tags = "用户年级接口")
@RestController
public class UserGradeController {

    @Autowired
    private UserGradeService userGradeService;

    @ApiOperation("查询所有年级")
    @GetMapping("/api/getUserGrade")
    public Result getUserGrade() {
        return ResultFactory.buildSuccessResult(userGradeService.getAllUserGrade());
    }

    @ApiOperation("通过id删除年级")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/getUserGrade/{id}")
    public Result delUserGradeById(@PathVariable Integer id) {
        userGradeService.delUserGrade(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("通过id修改年级")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/updateUserGrade")
    public Result updateUserGrade(@Valid @RequestBody UserGrade userGrade) {
        userGradeService.updateUserGradeById(userGrade);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("添加年级")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/addUserGrade")
    public Result addUserGradeById(@Valid @RequestBody UserGrade userGrade) {
        userGradeService.addUserGrade(userGrade);
        return ResultFactory.buildSuccessResult("添加成功");
    }

}
