package com.zgn.fazhi.controller;


import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.PictureType;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.PictureTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 图片分类
 * @author wt
 */
@Api(tags = "图片分类接口")
@RestController
public class PictureTypeController {

    @Autowired
    private PictureTypeService pictureTypeService;

    @ApiOperation("获取全部图片分类")
    @GetMapping("/api/getAllPictureType")
    public Result getAll(){
        return ResultFactory.buildSuccessResult(pictureTypeService.getAllPictureType());
    }


    @ApiOperation("根据Id获取图片分类")
    @GetMapping("/api/getPictureTypeById/{id}")
    public Result getById(@PathVariable Integer id){
        return ResultFactory.buildSuccessResult(pictureTypeService.getAllPictureTypeById(id));
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("增加图片分类")
    @PostMapping("/api/addPictureType")
    public Result addPictureType(@Valid @RequestBody PictureType pictureType){
        pictureTypeService.addPictureType(pictureType);
        return ResultFactory.buildSuccessResult("增加图片分类成功");
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("删除图片分类")
    @GetMapping("/api/deletePictureType/{id}")
    public Result deletePictureType(@PathVariable Integer id){
        pictureTypeService.deletePictureTypeById(id);
        return ResultFactory.buildSuccessResult("删除图片分类成功");
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("修改图片分类")
    @PostMapping("/api/updatePictureType")
    public Result updatePictureType(@Valid @RequestBody PictureType pictureType){
        pictureTypeService.updatePictureType(pictureType);
        return ResultFactory.buildSuccessResult("修改图片分类成功");
    }
}
