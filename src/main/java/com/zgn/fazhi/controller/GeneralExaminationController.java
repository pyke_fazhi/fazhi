package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.GeneralExamination;
import com.zgn.fazhi.pojo.vo.AddExamination;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.GeneralExaminationService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 考试/练习 题目模块
 *
 * @author ydw
 */
@Api(tags = "考试/练习记录的抽题接口")
@RestController
public class GeneralExaminationController {

    @Autowired
    private GeneralExaminationService generalExaminationService;

    @ApiOperation("后台通过统考id插入该次统考的考题")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/addAutoExamination")
    public Result addAutoExamination(@RequestBody AddExamination examination, @RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        generalExaminationService.addAutoExamination(examination, JwtUtils.getUidByToken(token));
        return ResultFactory.buildSuccessResult("添加成功");
    }

}
