package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.ExaminationAnswer;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.ExaminationAnswerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "题目答案选项管理")
@RestController
public class ExaminationAnswerController {

    @Autowired
    private ExaminationAnswerService examinationAnswerService;

    @ApiOperation("通过题目id删除该题目的所有选项")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/admin/delOptionByEid/{eid}")
    public Result delOptionByEid(@PathVariable Integer eid){
        examinationAnswerService.delAnswerListByEid(eid);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("通过题目id获取该题目的所有选项")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/api/getOptionByEid/{eid}")
    public Result getOptionByEid(@PathVariable Integer eid){
        return ResultFactory.buildSuccessResult(examinationAnswerService.selectByEid(eid));
    }

    @ApiOperation("通过id删除选项")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/delOptionById")
    public Result delOptionById(@RequestBody List<Integer> idList){
        examinationAnswerService.removeByIds(idList);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("通过id更新选项")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/updateOptionById")
    public Result updateOptionById(@RequestBody @Valid ExaminationAnswer examinationAnswer){
        examinationAnswerService.updateById(examinationAnswer);
        return ResultFactory.buildSuccessResult("更新成功");
    }


}
