package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.UserExaminationScoreService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "用户个人成绩接口")
@RestController
public class UserExaminationScoreController {

    @Autowired
    private UserExaminationScoreService userExaminationScoreService;

    @ApiOperation("查询当前用户对该次考试练习的成绩")
    @GetMapping("/api/getCurrentExaDate/{rid}")
    public Result getCurrentExaDate(@RequestHeader(JwtUtils.AUTH_HEADER) String token, @PathVariable Integer rid) {
        return ResultFactory.buildSuccessResult(
                userExaminationScoreService.getCurrentExaDateByUid(JwtUtils.getUidByToken(token), rid));

    }

    @ApiOperation("当前用户查询历次考试练习的成绩")
    @GetMapping("/api/getCurrentExaDateList/{rid}")
    public Result getCurrentExaDateList(@RequestHeader(JwtUtils.AUTH_HEADER) String token){
        return ResultFactory.buildSuccessResult(
                userExaminationScoreService.getCurrentExaDateListByUid(JwtUtils.getUidByToken(token)));
    }

}
