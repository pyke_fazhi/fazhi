package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.vo.UserTime;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.FileUploadService;
import com.zgn.fazhi.service.ResourcesService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "用户访问文件资源接口")
@RestController
public class FileResourcesController {

    @Autowired
    private ResourcesService resourcesService;

    @Autowired
    private FileUploadService fileUploadService;

    @Autowired
    private JwtUtils jwtUtils;

    /**
     * 用户通过资源分类id获取当前用户角色开放的资源
     *
     * @param rid 资源分类id
     */
    @ApiOperation("通过资源分类id和该资源分类id获取当前用户可见的资源")
    @GetMapping("/api/getResources/{rid}/{typeId}")
    public Result getResources(@PathVariable Integer rid, @PathVariable Integer typeId,
                               @RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        return ResultFactory.buildSuccessResult(
                resourcesService.getResources(rid,
                        jwtUtils.getCurrentUserInfo(token).getGradeId(),
                        typeId));
    }

    @ApiOperation("保存用户观看时间")
    @PostMapping("/api/saveUseTime")
    public Result saveUseTime(@RequestBody @Valid UserTime userTime, @RequestHeader(JwtUtils.AUTH_HEADER) String token){
        resourcesService.saveUseTime(userTime, JwtUtils.getUidByToken(token));
        return ResultFactory.buildSuccessResult("上传成功");
    }

    @ApiOperation("取消上传")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/admin/cancelUpload/{uploadId}")
    public Result cancelUpload(@PathVariable Integer uploadId){
        fileUploadService.cancelUpload(uploadId);
        return ResultFactory.buildSuccessResult("取消成功");
    }

    @ApiOperation("查看上传进度")
    @GetMapping("/admin/getProgress/{uploadId}")
    public Result getProgress(@PathVariable Integer uploadId){
        return ResultFactory.buildSuccessResult(fileUploadService.getProgress(uploadId));
    }



}
