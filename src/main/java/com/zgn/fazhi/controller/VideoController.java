package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Video;
import com.zgn.fazhi.pojo.vo.FileUploadForm;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.PermissionService;
import com.zgn.fazhi.service.ResourcesService;
import com.zgn.fazhi.service.VideoService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 视频增删改查接口
 */
@Api(tags = "视频接口")
@RestController
public class VideoController {

    @Autowired
    private VideoService videoService;

    @Autowired
    private ResourcesService resourcesService;

    @Autowired
    private JwtUtils jwtUtils;

    @ApiOperation("上传视频，需要前端生成随机的id，用于查询上传进度")
    @RequiresRoles(value = {"admin"})
    @PostMapping(value = "/admin/videoUpload",consumes = "multipart/*",headers = "content-type=multipart/form-data")
    public Result videoUpload(@ModelAttribute @Valid FileUploadForm fileUploadForm) {
        videoService.videoUpload(fileUploadForm);
        return ResultFactory.buildSuccessResult("上传成功");
    }

    @ApiOperation("根据id删除视频")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/videoDelete/{id}")
    public Result videoDelete(@PathVariable Integer id) {
        videoService.delById(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("通过视频分类id获取该分类所有视频")
    @GetMapping("/admin/videoList/{id}")
    public Result videoList(@PathVariable Integer id) {
        return ResultFactory.buildSuccessResult(videoService.listByTid(id));
    }

    @ApiOperation("根据id更新视频信息")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/admin/videoUpdate")
    public Result videoUpdate(@RequestBody @Valid Video video){
        videoService.updateById(video);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("当前用户获取当前可见的视频")
    @GetMapping("/api/getCurrentVideo/{typeId}")
    public Result getCurrentVideo(@PathVariable Integer typeId, @RequestHeader(JwtUtils.AUTH_HEADER) String token){
        return ResultFactory.buildSuccessResult(
                resourcesService.getResources(1, jwtUtils.getCurrentUserInfo(token).getGradeId(), typeId));
    }
}
