package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Evaluate;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.EvaluateService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 评价模块
 * @author ydw
 */
@Api(tags = "基地评价")
@RestController
public class EvaluateController {

    @Autowired
    private EvaluateService evaluateService;

    @ApiOperation("查询所有的评价")
    @GetMapping("/admin/getAllEvaluate")
    public Result getAllEvaluate(){
        return ResultFactory.buildSuccessResult(evaluateService.getAll());
    }

    @ApiOperation("插入评价")
    @PostMapping("/api/addEvaluate")
    public Result addEvaluate(@RequestHeader(JwtUtils.AUTH_HEADER) String token, @RequestBody Evaluate evaluate){
        // 通过用户id查询是否已经评价
        // 如果评价返回400，如果未评价，插入数据库
        evaluateService.addEvaluate(evaluate, JwtUtils.getUidByToken(token));
        return ResultFactory.buildSuccessResult("插入成功");
    }

    @ApiOperation("查询当前用户是否评价")
    @GetMapping("/api/getEvaluate")
    public Result getEvaluate(@RequestHeader(JwtUtils.AUTH_HEADER) String token){
        evaluateService.getEvaluate(JwtUtils.getUidByToken(token));
        return ResultFactory.buildSuccessResult("用户未评价");
    }

    @ApiOperation("查询好评率")
    @GetMapping("/api/getRate")
    public Result getRate(){
        // 查询所有评价，算出总数
        List<Evaluate> list = evaluateService.getAll();
        float i = list.size();
        // 查询所有好评 satisfied == 1的，算出总数
        List<Evaluate> resultList = list.stream().filter(evaluate -> Objects.equals(evaluate.getSatisfied(),1)).collect(Collectors.toList());
        float j = resultList.size();
        // 好评数/总数 得出好评率
        java.text.NumberFormat percentFormat = java.text.NumberFormat.getPercentInstance();
        float rate = j/i;
        return ResultFactory.buildSuccessResult(percentFormat.format(rate));

    }




}
