package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.vo.PermissionList;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.PermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "资源权限管理")
@RestController
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    @ApiOperation("给某个资源添加权限")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/addPermission")
    public Result addPermission(@RequestBody @Valid PermissionList permissionList){
        permissionService.addPermission(permissionList);
        return ResultFactory.buildSuccessResult("添加成功");
    }

    @ApiOperation("给某个资源更新权限")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/updatePermission")
    public Result updatePermission(@RequestBody @Valid PermissionList permissionList){
        permissionService.updatePermission(permissionList);
        return ResultFactory.buildSuccessResult("更新成功");
    }

    @ApiOperation("获取某个资源的权限范围，资源分类id 和 该资源id")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/getPermissionList/{resTid}/{resId}")
    public Result getPermissionList(@PathVariable Integer resTid, @PathVariable Integer resId){
        return ResultFactory.buildSuccessResult(permissionService.getByTidAndRid(resTid, resId));
    }

}
