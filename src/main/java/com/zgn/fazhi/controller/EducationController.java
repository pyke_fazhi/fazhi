package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Education;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.EducationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 学历增删改查
 *
 * @author wt
 */
@Api(tags = "学历接口(小初高")
@RestController
public class EducationController {

    @Autowired
    private EducationService educationService;

    @ApiOperation("查询所有学历")
    @GetMapping("/api/getEducation")
    public Result getEducation() {
        return ResultFactory.buildSuccessResult(educationService.getAllEducation());
    }

    @ApiOperation("通过id删除学历")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/getEducation/{id}")
    public Result delEducationById(@PathVariable Integer id) {
        educationService.delEducation(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("通过id修改学历")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/updateEducation")
    public Result updateEducation(@Valid @RequestBody Education education) {
        educationService.updateEducationById(education);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("添加学历")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/addEducation")
    public Result addEducationById(@Valid @RequestBody Education education) {
        educationService.addEducation(education);
        return ResultFactory.buildSuccessResult("添加成功");
    }
}
