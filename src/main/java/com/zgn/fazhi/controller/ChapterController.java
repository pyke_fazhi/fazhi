package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Chapter;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.ChapterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author ydw
 */
@Api(tags = "章节管理")
@RestController
public class ChapterController {

    @Autowired
    private ChapterService chapterService;


    @ApiOperation("增加章节")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/admin/addChapter")
    public Result addChapter(@Valid @RequestBody Chapter chapter) {
        chapterService.save(chapter);
        return ResultFactory.buildSuccessResult("添加成功");
    }

    @ApiOperation("删除章节")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/delChapter/{id}")
    public Result addChapter(@PathVariable Integer id) {
        chapterService.removeById(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("修改章节")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/admin/updateChapter")
    public Result updateChapter(@Valid @RequestBody Chapter chapter) {
        chapterService.updateById(chapter);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("通过单元id查询所有章节")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/getChapter/{uid}")
    public Result getChapterByUid(@PathVariable Integer uid) {
        return ResultFactory.buildSuccessResult(chapterService.getAllByUid(uid));
    }


}
