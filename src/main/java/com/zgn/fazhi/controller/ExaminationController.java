package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.*;
import com.zgn.fazhi.pojo.vo.AddExamination;
import com.zgn.fazhi.pojo.vo.AddRecord;
import com.zgn.fazhi.pojo.vo.SubmitExamination;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.ExaminationService;
import com.zgn.fazhi.service.GeneralExaminationService;
import com.zgn.fazhi.service.UserExaminationService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 考试模块
 *
 * @author ydw
 */

@Api(tags = "考题/练习题接口")
@RestController
public class ExaminationController {
    @Autowired
    private ExaminationService examinationService;

    @Autowired
    private UserExaminationService userExaminationService;

    @Autowired
    private GeneralExaminationService generalExaminationService;

    @Autowired
    private JwtUtils jwtUtils;

    @ApiOperation("根据类型查找考题/练习题")
    @GetMapping("/api/paperType/{id}")
    public Result selectType(@PathVariable Integer id) {
        return ResultFactory.buildSuccessResult(examinationService.selectByType(id));
    }

    @ApiOperation("查找所有的考题/练习题")
    @GetMapping("/api/getAllExa")
    public Result getAllExa() {
        return ResultFactory.buildSuccessResult(examinationService.selectAllPaper());
    }


    @ApiOperation("根据Id查找考题/练习题")
    @GetMapping("/api/paperId/{id}")
    public Result selectId(@PathVariable Integer id) {
        return ResultFactory.buildSuccessResult(examinationService.selectPaperById(id));
    }

    @ApiOperation("根据Id批量删除考题/练习题")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/api/delBatchExa")
    public Result delBatchExa(@RequestBody List<Integer> idList) {
        examinationService.removeByIds(idList);
        // 从缓存中删去
        examinationService.delExaCache(idList);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("增加考题/练习题")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/api/addExa")
    public Result add(@RequestBody Examination examination) {
        return examinationService.addPaper(examination);
    }

    @ApiOperation("删除考题/练习题")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/api/delExamination/{eid}")
    public Result delExamination(@PathVariable Integer eid) {
        // 通过考题id删除考题
        examinationService.delExaminationById(eid);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("根据Id修改考题/练习题")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/api/updateExa")
    public Result updateId(@RequestBody Examination examination) {
        return examinationService.updatePaperById(examination);
    }

    @ApiOperation("当前用户通过统考/练习id获取该次考试的所有题目")
    @GetMapping("/admin/getExaminationByRid/{rid}")
    public Result getExaminationByRid(@RequestHeader(JwtUtils.AUTH_HEADER) String token, @PathVariable Integer rid) {
        return ResultFactory.buildSuccessResult(generalExaminationService.findAllExaminationByRid(rid, JwtUtils.getUidByToken(token)));

    }

    @ApiOperation("当前用户通过考试/练习id获取该次考试/练习成绩")
    @GetMapping("/admin/getScore/{rid}")
    public Result getScore(@RequestHeader(JwtUtils.AUTH_HEADER) String token, @PathVariable Integer rid) {
        // 通过当前用户uid和统考id获取该次考试成绩
        UserInfo currentUserInfo = jwtUtils.getCurrentUserInfo(token);
        return ResultFactory.buildSuccessResult(userExaminationService.getScoreByUid(currentUserInfo.getId(), rid));
    }


    @ApiOperation("通过章节id获取所有该单元所有题目")
    @GetMapping("/admin/getByChapter/{chapterId}")
    public Result getByChapter(@PathVariable Integer chapterId){
        return ResultFactory.buildSuccessResult(examinationService.getListByChapter(chapterId));
    }

    @ApiOperation("通过单元id获取所有该章节所有题目")
    @GetMapping("/admin/getByUnit/{unitId}")
    public Result getByUnit(@PathVariable Integer unitId){
        return ResultFactory.buildSuccessResult(examinationService.getListByUnit(unitId));
    }

}
