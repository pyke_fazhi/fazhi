package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Picture;
import com.zgn.fazhi.pojo.vo.FileUploadForm;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.PermissionService;
import com.zgn.fazhi.service.PictureService;
import com.zgn.fazhi.service.ResourcesService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 图片上传下载
 */
@Api(tags = "图片上传接口")
@RestController
public class ImageController {

    @Autowired
    private PictureService pictureService;

    @Autowired
    private ResourcesService resourcesService;

    @Autowired
    private JwtUtils jwtUtils;

    @ApiOperation("上传表单数据")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping(value = "/admin/imgUpload",consumes = "multipart/*",headers = "content-type=multipart/form-data")
    public Result pictureUpload(@ModelAttribute @Valid FileUploadForm fileUploadForm) {
        pictureService.pictureUpload(fileUploadForm);
        return ResultFactory.buildSuccessResult("上传成功");
    }

    @ApiOperation("通过id删除图片")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/admin/imgDelete/{id}")
    public Result delete(@PathVariable Integer id) {
        pictureService.delById(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("查看该分类所有文件")
    @GetMapping("/admin/imgList/{id}")
    public Result imgList(@PathVariable Integer id) {
        return ResultFactory.buildSuccessResult(pictureService.listByTid(id));
    }

    @ApiOperation("修改文件信息")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/imgUpdate")
    public Result imgUpdate(@RequestBody @Valid Picture picture){
        pictureService.updateById(picture);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("当前用户获取当前可见的图片")
    @GetMapping("/api/getCurrentImg/{typeId}")
    public Result getCurrentImg(@PathVariable Integer typeId, @RequestHeader(JwtUtils.AUTH_HEADER) String token){
        return ResultFactory.buildSuccessResult(resourcesService.getResources(2, jwtUtils.getCurrentUserInfo(token).getGradeId(), typeId));
    }


}
