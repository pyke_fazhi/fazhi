package com.zgn.fazhi.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.AppointmentList;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.AppointmentListService;
import io.swagger.annotations.*;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 21/08/2021 11:38 AM
 */

@Api(tags = "预约名单管理")
@RestController
public class AppointmentListController {

    @Autowired
    private AppointmentListService appointmentListService;

    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @ApiOperation("上传预约名单")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "form", dataType = "_file", name = "上传预约名单", required = true)})
    @PostMapping(value = "/api/insertAppointmentList", consumes = "multipart/*", headers = "content-type=multipart/form-data")
    public Result insertAppointmentList(@RequestParam("aid") @NotNull Integer aid,
                                        @ApiParam(value = "上传预约名单", required = true) @RequestPart("file") MultipartFile file) {
        appointmentListService.uploadExcel(aid, file);
        return ResultFactory.buildSuccessResult("上传成功");
    }


    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @ApiOperation("通过预约id删除预约名单")
    @GetMapping("/api/delAppointmentList/{aid}")
    public Result delAppointmentList(@PathVariable Integer aid) {
        appointmentListService.remove(new QueryWrapper<AppointmentList>().eq("aid", aid));
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("通过预约id查看预约名单")
    @GetMapping("/api/getAppointmentList/{aid}")
    public Result getAppointmentList(@PathVariable Integer aid) {
        return ResultFactory.buildSuccessResult(appointmentListService.list(new QueryWrapper<AppointmentList>().eq("aid", aid)));
    }
}
