package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.UserFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "后台用户观看文档时长数据接口")
@RestController
public class UserFileController {

    @Autowired
    private UserFileService userFileService;

    @ApiOperation("查询所有用户的浏览文件时长")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/getFileTimeData")
    public Result getFileTimeData(){
        return ResultFactory.buildSuccessResult(userFileService.getFileTimeData());
    }

    @ApiOperation("获取用户浏览时长")
    @GetMapping("/api/getFileTimeDataByUid/{uid}")
    public Result getFileTimeDataByUid(@PathVariable Integer uid){
        return ResultFactory.buildSuccessResult(userFileService.getById(uid).getLength());
    }

}
