package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.*;
import com.zgn.fazhi.pojo.vo.AppointmentInfo;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.AppointmentService;
import com.zgn.fazhi.service.VisitContentService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.*;

/**
 * 预约模块
 *
 * @author ydw
 */
@Api(tags = "预约")
@RestController
@Slf4j
public class AppointmentController {

    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private VisitContentService visitContentService;

    @Autowired
    private JwtUtils jwtUtils;

    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @ApiOperation("获取某一天的预约情况")
    @PostMapping("/api/getAppointmentInfo")
    public Result getAppointmentInfo(@RequestBody String date) {
        // 通过日期查询当天的预约情况
        return ResultFactory.buildSuccessResult(appointmentService.getAppointmentByDate(date));
    }

    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @ApiOperation("查询所有可参观的地点")
    @GetMapping("/api/getAppContent")
    public Result getAppContent() {
        // 获取全部可参观地点
        return ResultFactory.buildSuccessResult(visitContentService.getAllVisitContent());
    }

    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @ApiOperation("通过id删除预约数据")
    @GetMapping("/api/delAppointment/{id}")
    public Result delAppointment(@PathVariable Integer id, @RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        // 需要删除两张表的预约数据
        appointmentService.delAppById(id);
        log.info("用户{}删除了一条预约记录", jwtUtils.getCurrentUserInfo(token));
        return ResultFactory.buildSuccessResult("删除成功 预约id : " + id);
    }

    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @ApiOperation("查询当前用户的预约情况")
    @GetMapping("/api/getCurrentAppointment")
    public Result getCurrentAppointment(@RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        // 查询用户的预约情况
        return ResultFactory.buildSuccessResult(appointmentService.findAllByUid(JwtUtils.getUidByToken(token)));
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("查询所有预约数据")
    @GetMapping("/api/getAllAppointment")
    public Result getAllAppointment() {
        return ResultFactory.buildSuccessResult(appointmentService.getAllApp());
    }

    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @ApiOperation("插入预约记录")
    @PostMapping("/api/insertAppointment")
    public Result insertAppointment(@Valid @RequestBody AppointmentInfo info, @RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        // 获取当前用户,并设置当前用户id
        // 插入前 需要判断当日日期 时间段预约是否满人
        Appointment appointment = info.getInfo();
        appointment.setUid(JwtUtils.getUidByToken(token));
        appointmentService.isFull(appointment);

        // 日期、时间段id、参观人数、手机号
        info.getInfo().setCreateTime(new Date());
        appointmentService.insertAppointment(info);
        return ResultFactory.buildSuccessResult("插入预约数据成功");
    }



}
