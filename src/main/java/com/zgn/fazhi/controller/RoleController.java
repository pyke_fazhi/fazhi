package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Role;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.RoleService;
import com.zgn.fazhi.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 角色管理模块
 *
 * @author ydw
 */
@Api(tags = "角色管理")
@RestController
public class RoleController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserInfoService userInfoService;

    @ApiOperation("查询所有角色")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/getAllRole")
    public Result getAllRole() {
        return ResultFactory.buildSuccessResult(roleService.getAllRole());
    }

    @ApiOperation("增加角色")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/addRole")
    public Result addRole(@Valid @RequestBody Role role) {
        roleService.addRole(role);
        return ResultFactory.buildSuccessResult("添加成功");
    }

    @ApiOperation("通过id删除角色,影响范围广,谨慎使用!")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/delRole/{id}")
    public Result addRole(@PathVariable Integer id) {
        // 无法删除admin角色
        if (id.equals(roleService.findByName("admin").getId())) {
            return ResultFactory.buildFailResult("无法删除admin角色");
        }
        // 删除角色，将会一并删除用户所用的角色, role_id会替换为0
        // 查询拥有该角色的所有用户
        List<UserInfo> allByRoleId = userInfoService.findAllByRoleId(id);
        // 开启事务将这群用户roleid改为0，且删除roleid
        roleService.delRole(allByRoleId, id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("更新角色")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/admin/updateRole")
    public Result updateRoleById(@RequestBody @Valid Role role){
        roleService.updateRoleById(role);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("改变用户角色,需要参数：用户id和需要切换的角色id")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/admin/updateUserRole/{userId}/{roleId}")
    public Result updateUserRole(@PathVariable Integer userId, @PathVariable Integer roleId) {

        userInfoService.updateUserRoleId(userId, roleId);
        return ResultFactory.buildSuccessResult("更新成功");
    }


}
