package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.VideoType;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.VideoTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 视频分类
 * @author wt
 */
@Api(tags = "视频分类管理")
@RestController
public class VideoTypeController {

    @Autowired
    private VideoTypeService videoTypeService;

    @ApiOperation("获取全部视频分类")
    @GetMapping("/api/getAllVideoType")
    public Result getAll(){
        return ResultFactory.buildSuccessResult(videoTypeService.getAllVideoType());
    }


    @ApiOperation("根据Id获取视频分类")
    @GetMapping("/api/getVideoTypeById/{id}")
    public Result getById(@PathVariable Integer id){
        return ResultFactory.buildSuccessResult(videoTypeService.getAllVideoTypeById(id));
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("增加视频分类")
    @PostMapping("/api/addVideoType")
    public Result addVideoType(@RequestBody VideoType videoType){
        videoTypeService.addVideoType(videoType);
        return ResultFactory.buildSuccessResult("增加视频分类成功");
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("删除视频分类")
    @GetMapping("/api/deleteVideoType/{id}")
    public Result deleteVideoType(@PathVariable Integer id){
        videoTypeService.deleteVideoTypeById(id);
        return ResultFactory.buildSuccessResult("删除视频分类成功");
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("修改视频分类")
    @PostMapping("/api/updateVideoType")
    public Result updateVideoType(@RequestBody VideoType videoType){
        videoTypeService.updateVideoType(videoType);
        return ResultFactory.buildSuccessResult("修改视频分类成功");
    }
}
