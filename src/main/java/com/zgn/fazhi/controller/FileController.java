package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.File;
import com.zgn.fazhi.pojo.vo.FileUploadForm;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.FileService;
import com.zgn.fazhi.service.PermissionService;
import com.zgn.fazhi.service.ResourcesService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 文件模块
 * @author Y121
 */
@Api(tags = "文档接口")
@RestController
public class FileController {

    @Autowired
    private FileService fileService;

    @Autowired
    private ResourcesService resourcesService;

    @Autowired
    private JwtUtils jwtUtils;

    @ApiOperation("上传文件")
    @PostMapping(value = "/admin/fileUpload",consumes = "multipart/*",headers = "content-type=multipart/form-data")
    public Result fileUpload(@ModelAttribute @Valid FileUploadForm fileUploadForm) {
        fileService.fileUpload(fileUploadForm);
        return ResultFactory.buildSuccessResult("上传成功");
    }


    @ApiOperation("通过id删除文件")
    @GetMapping("/admin/fileDelete/{id}")
    public Result delete(@PathVariable Integer id) {
        fileService.delById(id);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("查看该分类所有文件")
    @GetMapping("/admin/fileList/{id}")
    public Result fileList(@PathVariable Integer id) {
        return ResultFactory.buildSuccessResult(fileService.listByTid(id));
    }

    @ApiOperation("修改文件信息")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/admin/fileUpdate")
    public Result fileUpdate(@RequestBody @Valid File file){
        fileService.updateById(file);
        return ResultFactory.buildSuccessResult("修改成功");
    }

    @ApiOperation("当前用户获取当前可见的视频")
    @GetMapping("/api/getCurrentFile/{typeId}")
    public Result getCurrentFile(@PathVariable Integer typeId, @RequestHeader(JwtUtils.AUTH_HEADER) String token){
        return ResultFactory.buildSuccessResult(
                resourcesService.getResources(3, jwtUtils.getCurrentUserInfo(token).getGradeId(), typeId));
    }


}
