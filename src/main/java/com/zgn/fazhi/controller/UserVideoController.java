package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.UserVideo;
import com.zgn.fazhi.pojo.po.Video;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.UserVideoService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "视频后台数据分析接口")
@RestController
public class UserVideoController {

    @Autowired
    private UserVideoService userVideoService;

    @ApiOperation("获取所有用户的观看视频总时长")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/admin/getUserVideoData")
    public Result getUserVideoInfo(){
        return ResultFactory.buildSuccessResult(userVideoService.getUserVideoInfo());
    }

    @ApiOperation("通过用户id获取该用户观看视频记录以及总时长")
    @GetMapping("/admin/getUserVideoInfoByUid/{uid}")
    public Result getUserVideoInfoByUid(@PathVariable Integer uid){
        return ResultFactory.buildSuccessResult(userVideoService.getUserVideoInfoByUid(uid));
    }

    @ApiOperation("上传用户观看视频时长")
    @PostMapping("/api/saveVideoTime")
    public Result saveVideoTime(@RequestHeader(JwtUtils.AUTH_HEADER) String token, @RequestBody @Valid UserVideo userVideo){
        userVideoService.saveVideoTime(userVideo.setUid(JwtUtils.getUidByToken(token)));
        return ResultFactory.buildSuccessResult("保存成功");
    }

}
