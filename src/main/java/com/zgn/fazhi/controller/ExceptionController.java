package com.zgn.fazhi.controller;

import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.result.Result;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.ShiroException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;


@RestControllerAdvice
/**
 * 处理全局异常
 */
@Slf4j
public class ExceptionController {
    /**
     * 处理shiro异常
     */
    @ExceptionHandler(ShiroException.class)
    public Object handleShiroException(ShiroException e) {

        return ResultFactory.buildFailResult(e.getMessage());

    }

    /**
     * 统一处理controller参数校验异常
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Result validExceptionHandle(MethodArgumentNotValidException exception){
        exception.printStackTrace();
        BindingResult result = exception.getBindingResult();
        StringBuilder errorMsg = new StringBuilder() ;
        if (result.hasErrors()) {
            List<FieldError> fieldErrors = result.getFieldErrors();
            fieldErrors.forEach(error -> {
                System.out.println("field: " + error.getField() + ", msg:" + error.getDefaultMessage());
                errorMsg.append(error.getDefaultMessage()).append("!");
            });
        }
        exception.printStackTrace();
        return ResultFactory.buildFailResult(errorMsg.toString());
    }

    /**
     * 统一处理自定义异常
     * @param e 自定义异常
     */
    @ExceptionHandler(RestException.class)
    public Result handleRestException(RestException e) {
        log.error(e.getErrorMessage());
        return ResultFactory.buildResult(e.getErrorCode(), e.getErrorMessage(), e.getDate());

    }

//    // 捕捉其他所有异常
//    @ExceptionHandler(Exception.class)
//    public Object globalException(HttpServletRequest request, Throwable ex) {
//
//        return ResultFactory.buildFailResult(ex.getMessage());
//    }
}
