package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.LiveInfo;
import com.zgn.fazhi.pojo.vo.ApiLiveModel;
import com.zgn.fazhi.pojo.vo.ApiSaveLiveModel;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.AliYunLiveService;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Api(tags = "阿里云直播接口")
@RestController
@Slf4j
public class AliYunLiveController {

    @Autowired
    private AliYunLiveService aliYunLiveService;

    @ApiOperation("获取直播间信息")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/api/getLiveInfo")
    public Result getLiveInfo(@RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        return ResultFactory.buildSuccessResult(aliYunLiveService.getLiveInfo(JwtUtils.getUidByToken(token)));
    }

    @ApiOperation("更新直播间信息")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/api/updateLiveInfo")
    public Result updateLiveInfo(@RequestHeader(JwtUtils.AUTH_HEADER) String token, @Valid @RequestBody LiveInfo liveInfo) {
        aliYunLiveService.updateLiveInfo(liveInfo, JwtUtils.getUidByToken(token));
        return ResultFactory.buildSuccessResult("更新成功");
    }

    @ApiOperation("开启直播，获取推流url")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/api/startLive")
    public Result startLive(@RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        return ResultFactory.buildSuccessResult(aliYunLiveService.startLive(JwtUtils.getUidByToken(token)));
    }

    @ApiOperation("获取正在开播的直播")
    @GetMapping("/api/getAllLive")
    public Result getAllLive() {
        return ResultFactory.buildSuccessResult(aliYunLiveService.getAllLiveIfOn());
    }

    @ApiOperation("通过房间号获取播流url")
    @GetMapping("/api/getLiveByRoomId/{id}")
    public Result getLiveByRoomId(@PathVariable Integer id) {
        return ResultFactory.buildSuccessResult(aliYunLiveService.getPullUrl(id));
    }

    /**
     * 推流回调
     *
     * @param request 回调状态
     */
    @GetMapping("/api/aliyunLive")
    public void aliyunLive(HttpServletRequest request) {
        aliYunLiveService.aliyunLiveCallback(
                FastJsonUtils.toBean(request.getParameterMap(), ApiLiveModel.class));
    }

    /**
     * 直播录制回调
     *
     * @param model 回调信息封装
     */
    @PostMapping("/api/liveRecording")
    public void liveRecording(@RequestBody ApiSaveLiveModel model) {
        try {
            aliYunLiveService.saveliveRecord(model);
            log.info("保存作品成功");
        } catch (Exception e) {
            log.info("保存作品失败");
            e.printStackTrace();
        }
    }

    @ApiOperation("给直播间点赞")
    @GetMapping("/api/likeIncr")
    public String likeIncr(@RequestParam Integer liveId) {
        aliYunLiveService.likeIncr(liveId);
        return "ok";
    }


}
