package com.zgn.fazhi.controller;


import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.vo.AddRecord;
import com.zgn.fazhi.pojo.vo.SubmitExamination;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.ExaminationRecordService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 练习/考试记录
 */
@Api(tags = "考试/练习记录接口")
@RestController
public class ExaminationRecordController {

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private ExaminationRecordService examinationRecordService;


    @ApiOperation("当前用户查询所有考试")
    @GetMapping("/api/getExamination")
    public Result getExamination(@RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        return ResultFactory.buildSuccessResult(examinationRecordService.getExaminationByUid(jwtUtils.getCurrentUserInfo(token), 0));
    }

    @ApiOperation("删除考试练习记录")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/admin/delRecord")
    public Result delRecord(@PathVariable Integer rid){
        examinationRecordService.delRecord(rid);
        return ResultFactory.buildSuccessResult("删除成功");
    }

    @ApiOperation("当前用户查询所有练习")
    @GetMapping("/api/getExercise")
    public Result getExercise(@RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        return ResultFactory.buildSuccessResult(examinationRecordService.getExaminationByUid(jwtUtils.getCurrentUserInfo(token), 1));
    }

    @ApiOperation("当前用户提交考试/练习")
    @PostMapping("/api/submitExamination")
    public Result submitExamination(@RequestHeader(JwtUtils.AUTH_HEADER) String token, @RequestBody SubmitExamination submitExamination) {
        return ResultFactory.buildSuccessResult(examinationRecordService.submitExamination(submitExamination.setUid(JwtUtils.getUidByToken(token))));
    }

    @ApiOperation("查询所有考试/练习")
    @PostMapping("/admin/getExaList/{ifPractice}")
    public Result getExaList(@PathVariable Integer ifPractice, @RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        return ResultFactory.buildSuccessResult(examinationRecordService.getAllExa(ifPractice, jwtUtils.getCurrentUserInfo(token)));
    }


    @ApiOperation("后台开始一次考试练习")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @PostMapping("/admin/addExaminationRecord")
    public Result addExaminationRecord(@Valid @RequestBody AddRecord record, @RequestHeader(JwtUtils.AUTH_HEADER) String token) {
        record.getExaminationRecord().setUid(JwtUtils.getUidByToken(token));
        // 从前端接收该次统考的基本设置和题型设置
        examinationRecordService.addExaminationRecord(record);
        return ResultFactory.buildSuccessResult("插入成功");
    }

}
