package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.UserTimeService;
import com.zgn.fazhi.utils.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "用户使用时长等级接口")
@RestController
public class UserTimeController {

    @Autowired
    private UserTimeService userTimeService;

    @ApiOperation("获取当前用户等级")
    @GetMapping("/getRankByThisUser")
    public Result getRankByThisUser(@RequestHeader(JwtUtils.AUTH_HEADER) String token){
        return ResultFactory.buildSuccessResult(userTimeService.getRankByTimeLength(JwtUtils.getUidByToken(token)));
    }

    @ApiOperation("提交当前用户在线时长数据")
    @PostMapping("/api/saveUserTime")
    public Result saveUserTime(@RequestBody Integer time, @RequestHeader(JwtUtils.AUTH_HEADER) String token){
        userTimeService.saveUserTime(JwtUtils.getUidByToken(token), time);
        return ResultFactory.buildSuccessResult("保存成功");
    }

    @ApiOperation("获取所有用户的在线时长数据")
    @RequiresRoles(value = {"admin", "teacher"}, logical = Logical.OR)
    @GetMapping("/admin/getUserTimeData")
    public Result getUserTimeData(){
        return ResultFactory.buildSuccessResult(userTimeService.getUserTimeData());
    }

}
