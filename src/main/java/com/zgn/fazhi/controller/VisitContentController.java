package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.VisitContent;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.VisitContentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 参观地点
 * @author wt
 */
@Api(tags = "预约参观地点管理")
@RestController
public class VisitContentController {

    @Autowired
    private VisitContentService visitContentService;

    @ApiOperation("获取全部参观地点")
    @GetMapping("/api/getAllVisitContent")
    public Result getAll(){
        return ResultFactory.buildSuccessResult(visitContentService.getAllVisitContent());
    }

    @ApiOperation("根据Id获取参观地点")
    @GetMapping("/api/getVisitContentById/{id}")
    public Result getById(@PathVariable Integer id){
        return ResultFactory.buildSuccessResult(visitContentService.getAllVisitContentById(id));
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("增加参观地点")
    @PostMapping("/api/addVisitContent")
    public Result addVisit(@RequestBody VisitContent visitContent){
        visitContentService.addVisitContent(visitContent);
        return ResultFactory.buildSuccessResult("增加地点成功");
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("删除参观地点")
    @GetMapping("/api/deleteVisitContent/{id}")
    public Result deleteVisit(@PathVariable Integer id){
        visitContentService.deleteVisitContentById(id);
        return ResultFactory.buildSuccessResult("删除地点成功");
    }

    @RequiresRoles(value = {"admin"})
    @ApiOperation("修改参观地点")
    @PostMapping("/api/updateVisitContent")
    public Result updateVisit(@RequestBody VisitContent visitContent){
        visitContentService.updateVisitContent(visitContent);
        return ResultFactory.buildSuccessResult("修改地点成功");
    }
}
