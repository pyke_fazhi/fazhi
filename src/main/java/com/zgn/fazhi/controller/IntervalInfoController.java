package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.IntervalInfo;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.IntervalInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 修改时间段
 * @author wt
 */
@Api(tags = "预约时间段接口")
@RestController
public class IntervalInfoController {
    @Autowired
    private IntervalInfoService intervalInfoService;

    @ApiOperation("获取全部时间段")
    @GetMapping("/api/getAllIntervalInfo")
    public Result getAll(){
        return ResultFactory.buildSuccessResult(intervalInfoService.getAllInterval());
    }

    @ApiOperation("根据Id获取时间段")
    @GetMapping("/api/getIntervalInfoById/{id}")
    public Result getById(@PathVariable Integer id){
        return ResultFactory.buildSuccessResult(intervalInfoService.getIntervalInfoById(id));
    }

    @ApiOperation("增加参观时间段")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/api/addIntervalInfo")
    public Result addVisit(@RequestBody IntervalInfo intervalInfo){
        intervalInfoService.addIntervalInfo(intervalInfo);
        return ResultFactory.buildSuccessResult("增加时间段成功");
    }

    @ApiOperation("删除参观时间段")
    @RequiresRoles(value = {"admin"})
    @GetMapping("/api/deleteIntervalInfo/{id}")
    public Result deleteVisit(@PathVariable Integer id){
        intervalInfoService.deleteIntervalInfoById(id);
        return ResultFactory.buildSuccessResult("删除时间段成功");
    }

    @ApiOperation("修改参观时间段")
    @RequiresRoles(value = {"admin"})
    @PostMapping("/api/updateIntervalInfo")
    public Result updateVisit(@RequestBody IntervalInfo intervalInfo){
        intervalInfoService.updateIntervalInfo(intervalInfo);
        return ResultFactory.buildSuccessResult("修改时间段成功");
    }
}
