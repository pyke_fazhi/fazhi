package com.zgn.fazhi.controller;

import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.PgradeAccouracyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 查看各个等级的正确率、总练习时长
 *  需求更改，已废除
 */
public class GradeAccouracyController {

    @Autowired
    private PgradeAccouracyService pgradeAccouracyService;

    @ApiOperation("查询全部练习级别数据")
    @GetMapping("/api/getGradeDate")
    public Result getGradeDate() {
        // 数据包括各个等级正确率,各个级别总练习时长，各个级别练习人数
        return ResultFactory.buildSuccessResult(pgradeAccouracyService.getGradeDate());
    }

    @ApiOperation("查询练习题的正确率，总练习人数")
    @GetMapping("/api/getPracticeAccDate")
    public Result getPracticeAccDate() {
        return ResultFactory.buildSuccessResult(pgradeAccouracyService.getPracticeAccDate());
    }

    @ApiOperation("查询所有用户各个等级的练习时长、各个等级正确率、总练习时长、总正确率")
    @GetMapping("/api/getUserGradeDate")
    public Result getUserGradeDate() {
        return ResultFactory.buildSuccessResult(pgradeAccouracyService.getUserGradeDate());
    }

}
