package com.zgn.fazhi.factory;

import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.result.ResultCode;

public class ResultFactory {

    /**
     * 封装成功200响应码
     * @param data 成功响应后需要返回的数据
     * @return 返回 200响应码的成功结果集，带上数据的结果集
     */
    public static Result buildSuccessResult(Object data){
        return buildResult(ResultCode.SUCCESS, "成功", data);
    }

    /**
     * 封装失败400响应码
     * @param message 响应错误时返回的信息
     * @return  返回 400响应码的失败结果集，不带上数据，错误只返回错误信息
     */
    public static Result buildFailResult(String message){
        return buildResult(ResultCode.FAIL, message,null);
    }

    /**
     * 自定义封装返回结果集
     * @param resultCode 枚举类型的响应码
     * @param message 返回信息
     * @param data  返回数据
     * @return 返回 以上数据封装到Result的结果集
     */
    public static Result buildResult(ResultCode resultCode, String message, Object data){
        return buildResult(resultCode.code, message, data);
    }

    /**
     * 自定义封装返回结果集
     * @param resultCode int类型的响应码
     * @param message  返回信息
     * @param data 返回数据
     * @return  返回以上数据封装到Result的结果集
     */
    public static Result buildResult(int resultCode, String message, Object data){
        return new Result(resultCode, message, data);
    }


}
