package com.zgn.fazhi.jwt;

import com.zgn.fazhi.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationToken;

public class JwtToken implements AuthenticationToken {

    private static final long serialVersionUID = 1L;

    private String token;

    private Integer uid;

    public JwtToken(String token) {
        this.token = token;
        this.uid = Integer.parseInt(JwtUtils.getClaimFiled(token, "uid"));
    }

    @Override
    public Integer getPrincipal() {
        return this.uid;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    @Override
    public String toString() {
        return "JwtToken{" +
                "token='" + token + '\'' +
                ", uid='" + uid + '\'' +
                '}';
    }
}