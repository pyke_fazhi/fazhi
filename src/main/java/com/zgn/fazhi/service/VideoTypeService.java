package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.PictureType;
import com.zgn.fazhi.pojo.po.VideoType;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface VideoTypeService extends IService<VideoType> {

    /**
     * 获取全部视频类型
     *
     */
    List<VideoType> getAllVideoType();

    /**
     * 根据id获取视频类型
     *
     */
    VideoType getAllVideoTypeById(Integer id);

    /**
     * 增加视频类型
     *
     */
    Integer addVideoType(VideoType videoType);

    /**
     * 根据id删除视频类型
     *
     */
    Integer deleteVideoTypeById(Integer id);

    /**
     * 修改视频类型
     *
     */
    Integer updateVideoType(VideoType videoType);



}
