package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.UserInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Repository
public interface RoleService extends IService<Role> {
    /**
     * 查找所有角色
     * @return 返回角色集合
     */
    List<Role> getAllRole();

    /**
     * 通过角色中文名查找角色
     * @return 返回角色
     */
    Role getRoleByNameZh(String name);

    /**
     * 添加新的角色
     * @param role 新角色
     * @return 是否添加成功
     */
    Boolean addRole(Role role);

    /**
     * 删除角色
     * @param userInfoList 需要改变角色的用户
     * @param roleId 需要删除的角色id
     * @return 是否删除成功
     */
    Boolean delRole(List<UserInfo> userInfoList, Integer roleId);

    /**
     * 通过英文名查找角色
     * @return
     */
    Role findByName(String name);

    /**
     * 通过用户id查询该角色名
     * @param uid 用户id
     * @return 角色名
     */
    String findRnameByUid(Integer uid);


    Boolean updateRoleById(Role role);

}
