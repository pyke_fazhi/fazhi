package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.PracticeConfig;

import java.util.List;

/**
 * @author Y121
 */
public interface PracticeConfigService extends IService<PracticeConfig> {

    /**
     * 获取练习题配置
     * @return 练习题配置数组
     */
    List<PracticeConfig> getAllConfig();

    /**
     * 根据id删除配置
     * @param id
     * @return
     */
    int delPraConfig(Integer id);

    int addPraConfig(PracticeConfig practiceConfig);

    int updatePraConfig(PracticeConfig practiceConfig);
}
