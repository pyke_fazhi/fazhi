package com.zgn.fazhi.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.AppointmentConfig;
import com.zgn.fazhi.pojo.po.AppointmentContent;

/**
 * @author ydw
 */
public interface AppointmentConfigService extends IService<AppointmentConfig> {

    /**
     * 获取预约配置 ： 一次时间段预约最大人数
     * @return 最大人数
     */
    Integer getMaxNumber();

}
