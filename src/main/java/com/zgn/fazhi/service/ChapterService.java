package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.Chapter;

import java.util.List;

public interface ChapterService extends IService<Chapter>{

    /**
     * 通过单元id查询所有章节
     * @return
     */
    List<Chapter> getAllByUid(Integer uid);

    /**
     * 通过章节title查询章节
     * @param title 标题
     * @return 章节数据
     */
    Chapter getByTitle(String title, Integer unitId);

    /**
     * 查询所有章节
     * @return
     */
    List<Chapter> selectAll();

}
