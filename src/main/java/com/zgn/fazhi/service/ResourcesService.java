package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.Resources;
import com.zgn.fazhi.pojo.vo.UserTime;

import java.util.List;

public interface ResourcesService extends IService<Resources> {

    /**
     * 查询当前用户角色开放的资源
     * @param rid 资源分类id, 例如video、file
     * @param gid 用户年级
     * @param typeId 详细分类id
     * @return 资源集合
     */
    List getResources(Integer rid, Integer gid, Integer typeId);

    /**
     * 保存用户资源查看时长
     * @param userTime 时长数据
     * @param uid 用户id
     * @return
     */
    Boolean saveUseTime(UserTime userTime, Integer uid);

}
