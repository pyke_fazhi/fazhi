package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.ExaminationAnswer;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface ExaminationAnswerService extends IService<ExaminationAnswer> {

    /**
     * 向管理员题库批量插入答案
     * @param map 考题答案数据
     * @param id 考题id
     */
    Boolean addAnswerListAdmin(Map<Integer, String> map, int id);

    /**
     * 向教师题库批量插入答案
     * @param map 考题答案数据
     * @param id 考题id
     */
    Boolean addAnswerListTeacher(Map<Integer, String> map, int id);

    Boolean delAnswerListByEid(Integer eid);

    Boolean delAnswerById();

    /**
     * 通过题目id获取该题目的所有选项
     * @param eid 题目id
     * @return
     */
    List<ExaminationAnswer> selectByEid(Integer eid);

}
