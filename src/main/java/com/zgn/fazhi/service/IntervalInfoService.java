package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.IntervalInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.VisitContent;

import java.util.List;

/**
 *  服务类
 *
 * @author wt
 * @since 2021-06-25
 */
public interface IntervalInfoService extends IService<IntervalInfo> {

    /**
     * 获取所有预约时间段
     * @return 返回所有预约时间段的数组
     */
    List<IntervalInfo> getAllInterval();

    /**
     * 通过id获取数据
     *
     */
    IntervalInfo getIntervalInfoById(Integer id);

    /**
     * 增加预约时间段
     *
     */
    Boolean addIntervalInfo(IntervalInfo intervalInfo);

    /**
     * 通过id删除预约时间段
     *
     */
    Boolean deleteIntervalInfoById(Integer id);

    /**
     * 修改预约时间段
     *
     */
    Boolean updateIntervalInfo(IntervalInfo intervalInfo);

    /**
     * 刷新缓存
     * @return
     */
    Boolean refreshRedis();
}
