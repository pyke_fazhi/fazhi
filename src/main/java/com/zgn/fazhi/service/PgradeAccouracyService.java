package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.PgradeAccouracy;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.vo.GradeDate;
import com.zgn.fazhi.pojo.vo.UserGradeInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface PgradeAccouracyService extends IService<PgradeAccouracy> {

    /**
     * 数据包括各个等级正确率,各个级别总练习时长，各个级别练习人数
     */
    ArrayList<GradeDate> getGradeDate();

    /**
     * 查询练习题的正确率，总练习人数
     */
    HashMap<String, Object> getPracticeAccDate();

    /**
     * 查询所有用户各个等级的练习时长、各个等级正确率、总练习时长、总正确率
     */
    List<UserGradeInfo> getUserGradeDate();

}
