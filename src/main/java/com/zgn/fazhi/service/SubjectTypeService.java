package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.SubjectType;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface SubjectTypeService extends IService<SubjectType> {

    /**
     * 查询所有题型
     * @return 返回所有题型的数组
     */
    List<SubjectType> getAllSubjectType();


}
