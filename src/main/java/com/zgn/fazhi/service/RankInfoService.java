package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.RankInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface RankInfoService extends IService<RankInfo> {

    List<RankInfo> getRank();

    Boolean updateRank(RankInfo rankInfo);

    Boolean deleteRank(Integer id);

    Boolean addRank(RankInfo rankInfo);

}
