package com.zgn.fazhi.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;


public interface FileUploadService {

    /**
     * 上传阿里OSS
     * @param uploadFile 上传文件
     * @param fileType 文件类型，比如video/image/picture
     * @param detailedTypeId 该资源类型的详细分类id
     * @param pattern 限制上传格式的正则表达式
     * @param id 当前上传文件的唯一标识
     * @return 上传OSS生成后的路径
     */
    String upload(MultipartFile uploadFile, String fileType, Integer detailedTypeId, String pattern, Integer id);

    /**
     * 删除文件
     * @param objectName 路径名
     * @return
     */
    Boolean delete(String objectName);

    Boolean upload(InputStream input, String filepath);

    /**
     * 取消上传
     * @param uploadId 上传id
     * @reture
     */
    Boolean cancelUpload(Integer uploadId);

    /**
     * 查看上传进度
     * @param uploadId 上传id
     * @return
     */
    Integer getProgress(Integer uploadId);

}
