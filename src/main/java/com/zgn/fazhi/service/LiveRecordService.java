package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.LiveRecord;

/**
 * @author ydw
 * @version 1.0
 * @Description TODO
 * @date 21/08/2021 8:33 PM
 */

public interface LiveRecordService extends IService<LiveRecord> {

    /**
     * 创建一个直播记录
     * @param liveRecord 直播记录数据
     * @return 插入后的记录id
     */
    Integer saveRecord(LiveRecord liveRecord);

    /**
     * 获取直播间的最后一次直播记录
     * @param roomId 直播间id
     * @return 直播记录
     */
    LiveRecord getLastRecord(Integer roomId);

}
