package com.zgn.fazhi.service;


import com.zgn.fazhi.pojo.po.LiveInfo;
import com.zgn.fazhi.pojo.vo.ApiLiveModel;
import com.zgn.fazhi.pojo.vo.ApiSaveLiveModel;
import com.zgn.fazhi.pojo.vo.UserLiveInfo;

import java.util.List;
import java.util.Map;

public interface AliYunLiveService {

    /**
     * 阿里云直播回调参数
     * @param model 回调参数
     */
    void aliyunLiveCallback(ApiLiveModel model);

    /**
     * 生成推流播流地址
     * @param sourceId 房间号
     */
    void saveLive(String sourceId);

    /**
     * 获取直播间信息
     * @param uid 用户id
     * @return 该用户的直播间信息
     */
    LiveInfo getLiveInfo(Integer uid);

    /**
     * 开启直播，获取推流url
     * @param uid 用户id
     * @return 推流url
     */
    String startLive(Integer uid);

    /**
     * 更新直播间信息
     * @param liveInfo 、直播间信息
     * @return
     */
    Boolean updateLiveInfo(LiveInfo liveInfo, Integer currentUserId);

    /**
     * 获取所有正在直播的直播间
     * @return
     */
    List<UserLiveInfo> getAllLiveIfOn();

    /**
     * 获取所有直播
     * @return
     */
    List<UserLiveInfo> getAllLive();

    /**
     * 根据房间号获取播流url
     * @param lId 房间号
     * @return rtmp类型的播流url
     */
    String getPullUrl(Integer lId);

    /**
     * 保存录制的视频
     * @param model 录制信息
     * @return
     */
    Boolean saveliveRecord(ApiSaveLiveModel model);

    /**
     * 给直播间点赞
     * @param liveId 直播间id
     */
    void likeIncr(Integer liveId);

}
