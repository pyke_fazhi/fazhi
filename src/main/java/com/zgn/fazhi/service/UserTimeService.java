package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.RankInfo;
import com.zgn.fazhi.pojo.po.UserTime;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.vo.UserTimeData;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface UserTimeService extends IService<UserTime> {

    String getRankByTimeLength (Integer id);

    /**
     * 保存个人使用时长
     * @param uid 用户id
     * @param time 新增时长
     * @return
     */
    Boolean saveUserTime(Integer uid, Integer time);

    /**
     * 获取所有用户的在线时长数据
     * @return
     */
    List<UserTimeData> getUserTimeData();

}
