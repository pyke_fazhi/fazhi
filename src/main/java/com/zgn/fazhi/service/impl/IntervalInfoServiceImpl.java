package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.IntervalInfo;
import com.zgn.fazhi.mapper.IntervalInfoMapper;
import com.zgn.fazhi.pojo.po.VisitContent;
import com.zgn.fazhi.service.IntervalInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class IntervalInfoServiceImpl extends ServiceImpl<IntervalInfoMapper, IntervalInfo> implements IntervalInfoService {

    @Autowired
    private IntervalInfoMapper intervalInfoMapper;

    private static final String KEY = "intervalInfo";

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 获取所有预约时间段
     * @return 返回所有预约时间段的数组
     */
    @Override
    public List<IntervalInfo> getAllInterval() {
        // 从缓存中查询
        try {
            if (redisUtil.hasKey(KEY)) {
                return FastJsonUtils.toList(redisUtil.get(KEY), IntervalInfo.class);
            } else {
                return selectPutCache();
            }
        } catch (RedisConnectionFailureException e){
            return intervalInfoMapper.selectList(null);
        }
    }

    @Override
    public IntervalInfo getIntervalInfoById(Integer id) {
        return intervalInfoMapper.selectById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addIntervalInfo(IntervalInfo intervalInfo) {
        intervalInfoMapper.insert(intervalInfo);
        redisUtil.del(KEY);
        return true;

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteIntervalInfoById(Integer id) {
        intervalInfoMapper.deleteById(id);
        // 删除后刷新缓存
        redisUtil.del(KEY);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateIntervalInfo(IntervalInfo intervalInfo) {
        intervalInfoMapper.updateById(intervalInfo);
        redisUtil.del(KEY);
        return true;
    }

    private List<IntervalInfo> selectPutCache(){
        List<IntervalInfo> intervalInfos = intervalInfoMapper.selectList(null);
        redisUtil.setIfAbsent(KEY, intervalInfos, RedisUtil.EXPIRETIME);
        return intervalInfos;
    }

    @Override
    public Boolean refreshRedis() {
        redisUtil.del(KEY);
        selectPutCache();
        log.info("刷新缓存");
        return true;
    }
}
