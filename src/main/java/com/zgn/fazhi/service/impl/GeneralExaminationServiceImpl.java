package com.zgn.fazhi.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.pojo.po.*;
import com.zgn.fazhi.mapper.GeneralExaminationMapper;
import com.zgn.fazhi.pojo.vo.AddExamination;
import com.zgn.fazhi.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.ExaminationUtils;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class GeneralExaminationServiceImpl extends ServiceImpl<GeneralExaminationMapper, GeneralExamination> implements GeneralExaminationService {

    @Autowired
    private ExaminationService examinationService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private UnitService unitService;

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private EtypeScoreService etypeScoreService;

    @Autowired
    private SubjectTypeService subjectTypeService;

    @Autowired
    private GeneralExaminationMapper generalExaminationMapper;

    @Autowired
    private ExaminationUtils examinationUtils;

    @Autowired
    private ExaminationAnswerService examinationAnswerService;

    @Autowired
    private UserExaminationService userExaminationService;

    @Lazy
    @Autowired
    private ExaminationRecordService examinationRecordService;

    public static final String KEY = "examinationListByRid:";

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addAutoExamination(AddExamination examination, Integer uid) {

        // 自动组题配置
        List<ExaminationtTypeOption> autoConfigList = examination.getAutoConfigList();
        // 手动组题 题id集合
        List<Integer> examinationIdList = examination.getExaminationIdList();
        // 考试/练习id
        Integer eId = examination.getRId();

        // 手动插入考题题型分组数据
        Map<Integer, Integer> typeMap = new HashMap<>();
        if (!StringUtils.isEmpty(examinationIdList)) {
            // 校验手动组题是否合法, 如果合法则插入, 返回缺失的题型题数
            typeMap = manualNormal(examinationIdList, eId);
            // 没有缺少题型表示手动插入完成
            log.info("位置1 map: {}", typeMap);
            if (typeMap.isEmpty()) {
                log.warn(typeMap.toString());
                log.warn("手动插入成功");
                return true;
            }
        } else {
            // 如果为空，则判断为全自动
            // 获取本次考试/练习的预设map typeId : 题数
            List<EtypeScore> etypeScores = etypeScoreService.findAllById(eId);
            Map<Integer, Integer> finalTypeMap1 = typeMap;
            etypeScores.forEach(etypeScore -> {
                finalTypeMap1.put(etypeScore.getTid(), etypeScore.getNumber());
            });
            typeMap = finalTypeMap1;
        }

        log.warn("判断是否自动组题");
        // 判断是否为自动组题
        if (autoConfigList != null && !autoConfigList.isEmpty()) {
            // 自动组题
            log.info("自动组题");
            // 校验配置是否合法
            autoNormal(autoConfigList, eId);

            // 分出题型id不为null的配置，优先插入
            List<ExaminationtTypeOption> autoNotNullList = autoConfigList.stream()
                    .filter(autoConfig -> !StringUtils.isEmpty(autoConfig.getType()))
                    .collect(Collectors.toList());
            Map<Integer, Integer> finalTypeMap = typeMap;
            if (!autoNotNullList.isEmpty()) {
                autoNotNullList.forEach(autoConfig -> {
                    log.info("位置2 map: {}", finalTypeMap);
                    Integer typeId = autoConfig.getType();
                    assert finalTypeMap != null;
                    if (autoConfig.getNumber() > finalTypeMap.get(typeId)) {
                        // 如果当前题型题数超过缺少的题型题数, 终止程序
                        throw new RestException(400, "添加的题型id: " + typeId + "总和超过预设题数");
                    }
                    List<Integer> exaIdList = null;
                    // 如果单元为空，表示随机抽取教师题库的题
                    if (StringUtils.isEmpty(autoConfig.getUnitId())) {
                        exaIdList = examinationService.getRandomListByUid(uid, autoConfig.getType(), autoConfig.getNumber());
                    } else if (!StringUtils.isEmpty(autoConfig.getChapter())) {
                        // 如果章节id不为空
                        // 获取题型id、单元、章节、题数向数据库中随机抽取题目
                        exaIdList = examinationService.getRandomList(autoConfig);
                    } else {
                        // 如果章节为空，表示随机抽取范围为单元级别
                        // 从单元抽取题目
                        exaIdList = examinationService.getRandomListCidNull(typeId, autoConfig.getUnitId(), autoConfig.getNumber());
                    }

                    GeneralExamination generalExamination = new GeneralExamination();
                    generalExamination.setRecordId(eId);
                    // 遍历题目id，依次插入
                    exaIdList.forEach(exaId -> {
                        generalExamination.setEid(exaId);
                        generalExaminationMapper.insert(generalExamination);
                    });
                    // 对已插入题型数量 在 记录中减去，用于后续无题型随机抽取
                    Integer number = finalTypeMap.get(typeId);
                    number -= exaIdList.size();
                    if (number == 0) {
                        // 如果该题型已饱和, 删除该key
                        finalTypeMap.remove(typeId);
                    } else {
                        finalTypeMap.put(typeId, number);
                    }
                });
                typeMap = finalTypeMap;
            }

            // 当题型id为空的情况，需要根据缺少的题型数量来随机抽取符合条件题目
            List<ExaminationtTypeOption> autoNullList = autoConfigList.stream()
                    .filter(autoConfig -> StringUtils.isEmpty(autoConfig.getType()))
                    .collect(Collectors.toList());
            Map<Integer, Integer> finalTypeMap2 = typeMap;
            typeMap.keySet().forEach(typeId -> {
                // typeId最外层为缺少的题型题数
                for (ExaminationtTypeOption autoConfig : autoNullList) {
                    log.info("位置3 map: {}", finalTypeMap2);
                    if (autoConfig.getNumber() == 0) {
                        continue;
                    }
                    List<Integer> eidList = new ArrayList();
                    // 里层为需要的单元/章节题数
                    // 每遍历一次外层，减少的是 里层的单元/章节题数
                    // 当前外循环需要typeId题数为nowTypeNum
                    Integer outTypeNum = finalTypeMap2.get(typeId);
                    // 当前内循环需要单元/章节题数为
                    Integer inNumber = autoConfig.getNumber();
                    // 如果外层小于内层，表示该题型题数饱和，内层需要题数减去外层题数, 并保存内存减去的题数,终止内循环
                    if (outTypeNum <= inNumber) {
                        if (StringUtils.isEmpty(autoConfig.getUnitId())) {
                            // 如果单元为空，表示随机抽取教师题库的题
                            eidList = examinationService.getRandomListByUid(uid, autoConfig.getType(), outTypeNum);
                        } else if (StringUtils.isEmpty(autoConfig.getChapter())) {
                            // 通过题型id、单元/章节随机插入
                            // 如果章节为空
                            eidList = examinationService.getRandomListTidNull(typeId, autoConfig.getUnitId(), outTypeNum);
                        } else {
                            // 如果章节不为空
                            eidList = examinationService.getRandomListTidNull(typeId, autoConfig.getChapter(), autoConfig.getUnitId(), outTypeNum);
                        }
                        saveDB(eId, eidList);
                        autoConfig.setNumber(inNumber - outTypeNum);
                        break;
                    } else {
                        // 如果外层大于内层，表示该题型题数不饱和，外层需要减去内层，并保存，并执行下一个内层循环
                        if (StringUtils.isEmpty(autoConfig.getUnitId())) {
                            // 如果单元为空，表示随机抽取教师题库的题
                            eidList = examinationService.getRandomListByUid(uid, autoConfig.getType(), inNumber);
                        } else if (StringUtils.isEmpty(autoConfig.getChapter())) {
                            // 如果章节为空
                            eidList = examinationService.getRandomListTidNull(typeId, autoConfig.getUnitId(), inNumber);
                        } else {
                            // 如果章节不为空
                            eidList = examinationService.getRandomListTidNull(typeId, autoConfig.getUnitId(), autoConfig.getChapter(), inNumber);
                        }
                        saveDB(eId, eidList);
                        finalTypeMap2.put(typeId, outTypeNum - inNumber);
                        autoConfig.setNumber(0);
                    }
                }
            });
        }
        // 清除缓存
        redisUtil.del(KEY + eId);
        return true;
    }

    @Override
    public Boolean manual(List<Integer> eIdList, Integer eid) {
        // 手动插入考题
        for (Integer id : eIdList) {
            GeneralExamination generalExamination = new GeneralExamination();
            // 考题id
            generalExamination.setEid(id);
            // 统考id
            generalExamination.setRecordId(eid);
            generalExaminationMapper.insert(generalExamination);
        }
        return true;
    }

    @Override
    public List<GeneralExamination> findAllByEid(Integer eid) {
        return generalExaminationMapper.selectList(null);
    }

    @Override
    public List<RecordExa> findAllExaminationByRid(Integer rid, Integer uid) {
        // 需要判断当前用户对该次统考是否已经考试,或考试次数用尽
        if (userExaminationService.ifExamination(uid, rid) <= 0) {
            throw new RestException(400, "当前用户考试次数用完");
        }
        String key = KEY + rid;
        // 查询缓存
        try {
            if (redisUtil.hasKey(key)){
                log.info("进入缓存查询题目");
                return FastJsonUtils.toList(redisUtil.lGetAll(key), RecordExa.class);
            }
        } catch (RedisConnectionFailureException e){
            log.error("redis连接错误");
            // redis 改掉则直接查数据库
            return selectExaminationByRid(rid);
        }
        // 没有缓存则查询数据库， 上锁
        Lock lock = new ReentrantLock();
        lock.lock();
        try {
            if (redisUtil.hasKey(key)){
                log.info("进入缓存查询题目");
                return FastJsonUtils.toList(redisUtil.lGetAll(key), RecordExa.class);
            }
            List<RecordExa> recordExas = selectExaminationByRid(rid);
            // 加入缓存中
            redisUtil.lSet(key, recordExas, RedisUtil.EXPIRETIME);
            return recordExas;
        } finally {
            lock.unlock();
        }
    }

    /**
     * 清理缓存
     */
    public void cleanRedis(List<Examination> examinations) {
        for (Examination examination : examinations) {
            String key = "examination:" + examination.getTypeId();
            if (redisUtil.hasKey(key)) {
                redisUtil.del(key);
            }
        }
    }

    /**
     * 校验自动组题配置的数据
     *
     * @param autoConfigList 自动组题的配置集合
     * @param eid            考试/练习id
     */
    public void autoNormal(List<ExaminationtTypeOption> autoConfigList, Integer eid) {
        // 校验单元、章节是否在范围内, 单元可以为空，表示教师的私有题库
        List<Integer> allUnit = autoConfigList
                .stream()
                .filter(autoConfig -> !StringUtils.isEmpty(autoConfig.getUnitId()))
                .map(ExaminationtTypeOption::getUnitId).collect(Collectors.toList())
                .stream()
                .distinct()
                .collect(Collectors.toList());
        // 取出单元id属性
        List<Integer> unitIdList = unitService.findAllUnit().stream().map(Unit::getId).collect(Collectors.toList());
        if (!unitIdList.containsAll(allUnit)) {
            log.warn("数据库单元 : " + unitIdList);
            log.warn(allUnit.toString());
            throw new RestException(400, "单元不在数据库范围");
        }

        // 章节可以为空，表示随机抽取单元题
        // 筛选章节为空校验
        List<Integer> allChapter = autoConfigList.stream().filter(autoConfig -> !StringUtils.isEmpty(autoConfig.getChapter()))
                .map(ExaminationtTypeOption::getChapter).collect(Collectors.toList())
                .stream().distinct().collect(Collectors.toList());
        // 单独取出章节id另存为list
        List<Integer> chapterIdList = chapterService.selectAll().stream().map(Chapter::getId).collect(Collectors.toList());
        if (!chapterIdList.containsAll(allChapter)) {
            throw new RestException(400, "章节不在数据库范围");
        }

        // 判断自动组题题数是否超过预设
        int sum = autoConfigList.stream().mapToInt(ExaminationtTypeOption::getNumber).sum();
        Integer exaNumDB = getExaNumDB(eid);
        log.info("当前总题数 : " + sum);
        log.info("数据库总题数 : " + exaNumDB);
        if (sum > exaNumDB) {
            throw new RestException(400, "当前总题数超过预设");
        }

        // 筛选出题型不为null的数据，避免分组出现空指针
        List<ExaminationtTypeOption> optionList = autoConfigList.stream().filter(option -> !StringUtils.isEmpty(option.getType()))
                .collect(Collectors.toList());
        // 通过该次考试记录查询该题数预设
        Map<Integer, Integer> typeMap =
                optionList
                        .stream()
                        .collect(Collectors.groupingBy(ExaminationtTypeOption::getType,
                                Collectors.summingInt(ExaminationtTypeOption::getNumber)));
        //遍历map中的键
        for (Integer typeId : typeMap.keySet()) {
            // key是题型id
            // 预设题数
            Integer number = etypeScoreService.findByEidAndTid(eid, typeId).getNumber();
            // 当前题型数量
            Integer nowNumber = typeMap.get(typeId);
            if (number < nowNumber) {
                throw new RestException(400, "题型ID : " + typeId + " 超过预设的题数: " + number + "当前所选题数为 : " + nowNumber);
            }
        }

        // 校验题型是否在范围内
        List<Integer> allType = optionList.stream().map(ExaminationtTypeOption::getType).collect(Collectors.toList())
                .stream().distinct().collect(Collectors.toList());
        // 题型可以为空，表示插入
        // 获取数据库所有题型
        List<Integer> subjectIdList = subjectTypeService.getAllSubjectType().stream().map(SubjectType::getId).collect(Collectors.toList());
        if (!subjectIdList.containsAll(allType)) {
            throw new RestException(400, "自动组题的题型数据参数错误");
        }
    }

    /**
     * 对手动组题的数据校验, 如果合法则插入
     *
     * @param examinationIdList 题目id
     * @param eid               考试/练习id
     * @return 插入题目后题型缺失 typeId : 题数
     */
    public Map<Integer, Integer> manualNormal(List<Integer> examinationIdList, Integer eid) {
        log.info("需要手动插入的题目: {}", examinationIdList);
        List<Examination> examinations = examinationService.listByIds(examinationIdList);
        log.info("从数据库查出手动组题的数据: {}", JSONArray.toJSON(examinations));
        // 是否超过预设题数
        Integer exaNumDB = getExaNumDB(eid);
        if (examinations.size() > exaNumDB) {
            throw new RestException(400, "手动组题数超过预设,当前题数: " + examinations.size() + ",预设题数 : " + exaNumDB);
        }
        // 对题型分组计数
        Map<Integer, Long> typeMap =
                examinations
                        .stream()
                        .collect(Collectors.groupingBy(Examination::getTypeId, Collectors.counting()));
        log.info("手动组题分组 typeId:题数 {}", typeMap);
        // 查询该次记录的题目题型分数配置，加入map中， 后面如果插入会覆盖
        List<EtypeScore> EtypeScores = etypeScoreService.findAllById(eid);
        log.info("完整预设题型题数: {}", JSONArray.toJSON(EtypeScores));
        // 分组map typeId : 题数
        Map defaultMap = EtypeScores
                .stream()
                .collect(Collectors.groupingBy(EtypeScore::getTid,
                        Collectors.summingInt(EtypeScore::getNumber)));
        log.info("完整预设题型题数: {}", defaultMap);
//        Map<Integer, Integer> defaultMap = new HashMap<>();
        // 与预设对比
        typeMap.keySet().forEach(typeId -> {
            log.info("遍历预设题型typeId: {}", typeId);
            log.info("当前记录id: {}", eid);
            Integer exaTypeNumDB = null;
            try {
                exaTypeNumDB = etypeScoreService.findByEidAndTid(eid, typeId).getNumber();
            } catch (NullPointerException e) {
                throw new RestException(400, "出现数据未知题型 id: " + typeId + ", 请检查是否符合预设题型");
            }
            // 当前题型题数
            Integer nowExaTypeNum = typeMap.get(typeId).intValue();
            if (nowExaTypeNum > exaTypeNumDB) {
                throw new RestException(400, "手动组题题型: " + typeId + "题数为" + nowExaTypeNum + "超过预设题型题数:" + exaTypeNumDB);
            }
            // 计算该题型缺少数量
            int i = exaTypeNumDB - nowExaTypeNum;
            if (i != 0) {
                defaultMap.put(typeId, i);
            } else {
                // 为0表示该题型插入完成，删除该key
                defaultMap.remove(typeId);
            }
        });
        // 将考题插入到当前 考试/练习记录中
        examinationIdList.forEach(examinationId -> {
            GeneralExamination generalExamination = new GeneralExamination();
            generalExamination
                    .setRecordId(eid)
                    .setEid(examinationId);
            generalExaminationMapper.insert(generalExamination);
        });
        log.info("缺少题型题数: {}", defaultMap);
        return defaultMap;
    }

    /**
     * 从数据库查找该次考试总题数
     *
     * @param eid 考试/练习id
     */
    public Integer getExaNumDB(Integer eid) {
        List<EtypeScore> eTypeScores = etypeScoreService.findAllById(eid);
        // 获取当前考试/练习预设总题数sumDB
        return eTypeScores.stream().mapToInt(EtypeScore::getNumber).sum();
    }

    public void saveDB(Integer eId, List<Integer> eidList) {
        GeneralExamination generalExamination = new GeneralExamination();
        generalExamination.setRecordId(eId);
        for (Integer id : eidList) {
            generalExamination.setEid(id);
            generalExaminationMapper.insert(generalExamination);
        }
    }

    private List<RecordExa> selectExaminationByRid(Integer rid){
        log.info("进入数据库查询");
        List<RecordExa> recordExas = new ArrayList<>();
        // 查询该次考试的所有考题id
        // 通过idList查询数据库所有题目
        examinationService.listByIds(
                generalExaminationMapper.selectList(
                        new QueryWrapper<GeneralExamination>().eq("record_id", rid))
                        .stream()
                        .map(GeneralExamination::getEid)
                        .collect(Collectors.toList())).forEach(examination -> {
            recordExas.add(
                    new RecordExa()
                            .setTitle(examination.getTitle())
                            .setUnitId(examination.getUnitId())
                            .setChapterId(examination.getChapterId())
                            .setTypeId(examination.getTypeId())
                            .setId(examination.getId())
                            .setOption(examinationAnswerService.selectByEid(examination.getId())
                                    .stream()
                                    .map(ExaminationAnswer::getAnswerOption)
                                    .collect(Collectors.toList())));
        });
        return recordExas;
    }

    @Override
    public Boolean refreshCache() {
        // 查询所有考试练习记录
        examinationRecordService.list().forEach(record -> {
            redisUtil.del(KEY + record.getId());
        });
        return true;
    }

    @Override
    public Boolean delByRid(Integer rid) {
        // 删除缓存
        redisUtil.del(KEY + rid);
        generalExaminationMapper.delete(
                new QueryWrapper<GeneralExamination>()
                        .eq("record_id", rid));
        return true;
    }
}


