package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.mapper.PictureTypeMapper;
import com.zgn.fazhi.pojo.po.PictureType;
import com.zgn.fazhi.pojo.po.VideoType;
import com.zgn.fazhi.mapper.VideoTypeMapper;
import com.zgn.fazhi.service.VideoTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class VideoTypeServiceImpl extends ServiceImpl<VideoTypeMapper, VideoType> implements VideoTypeService {

    @Autowired
    private VideoTypeMapper videoTypeMapper;

    @Autowired
    private RedisUtil redisUtil;

    private static final String KEY = "VideoTypeKey";

    @Override
    public List<VideoType> getAllVideoType() {
        try {
            if (redisUtil.hasKey(KEY)){
                return FastJsonUtils.toList(redisUtil.get(KEY), VideoType.class);
            }
        } catch (RedisConnectionFailureException e){
            return videoTypeMapper.selectList(null);
        }
        List<VideoType> videoTypes = videoTypeMapper.selectList(null);
        redisUtil.setIfAbsent(KEY, videoTypes, RedisUtil.EXPIRETIME);
        return videoTypes;
    }

    @Override
    public VideoType getAllVideoTypeById(Integer id) {
        return videoTypeMapper.selectById(id);
    }

    @Override
    public Integer addVideoType(VideoType videoType) {
        redisUtil.del(KEY);
        return videoTypeMapper.insert(videoType);
    }

    @Override
    public Integer deleteVideoTypeById(Integer id) {
        redisUtil.del(KEY);
        return videoTypeMapper.deleteById(id);
    }

    @Override
    public Integer updateVideoType(VideoType videoType) {
        redisUtil.del(KEY);
        return videoTypeMapper.updateById(videoType);
    }

}
