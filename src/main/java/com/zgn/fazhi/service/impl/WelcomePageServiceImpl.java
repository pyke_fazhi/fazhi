package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.WelcomePage;
import com.zgn.fazhi.mapper.WelcomePageMapper;
import com.zgn.fazhi.service.WelcomePageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-26
 */
@Service
public class WelcomePageServiceImpl extends ServiceImpl<WelcomePageMapper, WelcomePage> implements WelcomePageService {

}
