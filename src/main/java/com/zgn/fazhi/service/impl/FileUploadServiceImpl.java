package com.zgn.fazhi.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.model.*;
import com.zgn.fazhi.config.AliyunConfig;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.listener.PutObjectProgressListener;
import com.zgn.fazhi.pojo.dto.FileUploadIndex;
import com.zgn.fazhi.service.FileUploadService;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

@Service
@Slf4j
public class FileUploadServiceImpl implements FileUploadService {

    @Autowired
    private OSS ossClient;

    @Autowired
    private AliyunConfig aliyunConfig;

    @Autowired
    private RedisUtil redisUtil;

    private static final String UPLOAD_KEY = "uploadPercent:";

    /**
     * 每个分片的大小，用于计算文件有多少个分片。单位为字节 ，当前设置为1mb 1 * 1024 * 1024L
     */
    private static final long PART_SIZE = 1024 * 1024L;

    private static final String CANCEL_KEY = "CANCEL_UPLOAD_KEY:";

    @Override
    public String upload(MultipartFile uploadFile, String fileType, Integer detailedTypeId, String pattern, Integer id) {

        String fileName = check(uploadFile, pattern);

        // 生成文件路径
        String filePath = getFilePath(fileName, fileType, detailedTypeId);
        log.info("上传文件的OSS路径为: " + filePath);

        long size = uploadFile.getSize();
        // 分片上传
        try {
            fragmentation(uploadFile, id.toString(), filePath);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(400, "上传失败");
        }
        return filePath;
    }

    @Override
    public Boolean delete(String objectName) {
        // 根据BucketName,objectName删除文件
        ossClient.deleteObject(aliyunConfig.getBucketName(), objectName);
        return true;
    }

    @Override
    public Boolean upload(InputStream input, String filePath) {
        // 上传阿里云OSS
        try {
            ossClient.putObject(new PutObjectRequest(aliyunConfig.getBucketName(), filePath, input));
        } catch (Exception e) {
            e.printStackTrace();
            //上传失败
            throw new RestException(400, "上传失败");
        }
        return null;
    }

    /**
     * 生成路径以及文件名 例如：//file/image/1/15564277465972939.jpg
     */
    private String getFilePath(String sourceFileName, String folderName, Integer detailedTypeId) {
        return "file/" + folderName + "/" +
                detailedTypeId + "/" +
                System.currentTimeMillis() +

                // 获取区间的随机数
                ThreadLocalRandom.current().nextInt(100, 10000) + "." +

                // 该方法取sting1内分隔符sting2后的字符串
                StringUtils.substringAfterLast(sourceFileName, ".");
    }

    /**
     * 校验文件名
     *
     * @param uploadFile 上传的文件
     * @param pattern    正则表达式
     * @return 文件名
     */
    private String check(MultipartFile uploadFile, String pattern) {
        String fileName = uploadFile.getOriginalFilename();
        log.info("上传的文件名为: " + fileName);
        // 通过正则表达式校验文件格式
        if (StringUtils.isEmpty(fileName) || !Pattern.compile(pattern).matcher(fileName).matches()) {
            throw new RestException(400, "文件名不合法");
        }
        return fileName;
    }

    private void fragmentation(MultipartFile uploadFile, String uploadId1, String filePath) throws IOException {
        String bucketName = aliyunConfig.getBucketName();
        // 创建InitiateMultipartUploadRequest对象
        InitiateMultipartUploadRequest request = new InitiateMultipartUploadRequest(bucketName, filePath);
        // 初始化分片。
        InitiateMultipartUploadResult upResult = ossClient.initiateMultipartUpload(request);
        // 蛇者uploadId，它是分片上传事件的唯一标识。您可以根据该uploadId发起相关的操作，例如取消分片上传、查询分片上传等。
        String uploadId = upResult.getUploadId();
        // 保存生成id至redis中
        redisUtil.set(CANCEL_KEY + uploadId1,
                new FileUploadIndex()
                        .setUploadId(uploadId)
                        .setUploadPath(filePath), 60 * 30);
        // partETags是PartETag的集合。PartETag由分片的ETag和分片号组成
        List<PartETag> partETags = new ArrayList<>();

        // 计算文件大小
        long fileLength = uploadFile.getSize();
        int partCount = (int) (fileLength / PART_SIZE);
        log.info("上传片数: {}", partCount);
        if (fileLength % PART_SIZE != 0) {
            partCount++;
        }
        String key = UPLOAD_KEY + uploadId1;
        // 遍历分片上传。
        for (int i = 0; i < partCount; i++) {
            log.info("========= 上传第{}个分片", i + 1);
            // 设置当前进度
            int i1 = i * 100 / partCount;
            log.info("当前进度: {}", i1 + "%");
            redisUtil.set(key, i1, 60 * 5);

            long startPos = i * PART_SIZE;
            long curPartSize = (i + 1 == partCount) ? (fileLength - startPos) : PART_SIZE;
            InputStream inputStream = new ByteArrayInputStream(uploadFile.getBytes());
            // 跳过已经上传的分片。
            inputStream.skip(startPos);
            UploadPartRequest uploadPartRequest = new UploadPartRequest();
            uploadPartRequest.setBucketName(bucketName);
            uploadPartRequest.setKey(filePath);
            uploadPartRequest.setUploadId(uploadId);
            uploadPartRequest.setInputStream(inputStream);
            // 设置分片大小。除了最后一个分片没有大小限制，其他的分片最小为100 KB。
            uploadPartRequest.setPartSize(curPartSize);
            // 设置分片号。每一个上传的分片都有一个分片号，取值范围是1~10000，如果超出此范围，OSS将返回InvalidArgument错误码。
            uploadPartRequest.setPartNumber(i + 1);
            // 每个分片不需要按顺序上传，甚至可以在不同客户端上传，OSS会按照分片号排序组成完整的文件。
            UploadPartResult uploadPartResult = ossClient.uploadPart(
                    uploadPartRequest.withProgressListener(new PutObjectProgressListener(Integer.parseInt(uploadId1), redisUtil, fileLength)));

            // 每次上传分片之后，OSS的返回结果包含PartETag。PartETag将被保存在partETags中。
            partETags.add(uploadPartResult.getPartETag());
        }
        log.info("上传进度: 100%");
        redisUtil.set(key, 100, 60 * 5);

        // 创建CompleteMultipartUploadRequest对象。
        // 在执行完成分片上传操作时，需要提供所有有效的partETags。OSS收到提交的partETags后，
        // 会逐一验证每个分片的有效性。当所有的数据分片验证通过后，OSS将把这些分片组合成一个完整的文件。
        // 完成上传。
        ossClient.completeMultipartUpload(new CompleteMultipartUploadRequest(bucketName, filePath, uploadId, partETags));
    }

    @Override
    public Boolean cancelUpload(Integer uploadId) {
        String key = CANCEL_KEY + uploadId;
        FileUploadIndex fileUploadIndex = FastJsonUtils.toBean(redisUtil.get(key), FileUploadIndex.class);
        // 取消分片上传。
        ossClient.abortMultipartUpload(
                new AbortMultipartUploadRequest(aliyunConfig.getBucketName(),
                        fileUploadIndex.getUploadPath(),
                        fileUploadIndex.getUploadId()));
        redisUtil.del(key);
        return true;
    }

    @Override
    public Integer getProgress(Integer uploadId) {
        try {
            return Integer.parseInt(redisUtil.get(UPLOAD_KEY + uploadId).toString());
        } catch (NullPointerException e){
            throw new RestException(400, "该上传id无效");
        }
    }
}
