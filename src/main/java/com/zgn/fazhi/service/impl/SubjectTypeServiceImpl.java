package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.PictureType;
import com.zgn.fazhi.pojo.po.SubjectType;
import com.zgn.fazhi.mapper.SubjectTypeMapper;
import com.zgn.fazhi.service.SubjectTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
public class SubjectTypeServiceImpl extends ServiceImpl<SubjectTypeMapper, SubjectType> implements SubjectTypeService {

    @Autowired
    private SubjectTypeMapper subjectTypeMapper;

    /**
     * 查询所有题型
     * @return 返回所有题型的数组
     */
    @Override
    public List<SubjectType> getAllSubjectType() {

        List<SubjectType> subjectTypes = subjectTypeMapper.selectList(null);
        System.out.println(subjectTypes);
        return subjectTypes;
    }

}
