package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.mapper.UserGradeMapper;
import com.zgn.fazhi.pojo.po.UserGrade;
import com.zgn.fazhi.service.UserGradeService;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
@Slf4j
public class UserGradeServiceImpl extends ServiceImpl<UserGradeMapper, UserGrade> implements UserGradeService {

    public static final String KEY = "userGradeKey";

    @Autowired
    private UserGradeMapper userGradeMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public List<UserGrade> getAllUserGrade() {
        if (redisUtil.hasKey(KEY)){
            return redisUtil.lGetAll(KEY);
        }
        // 没有缓存则查询数据库
        return saveCache();
    }

    @Override
    public Boolean addUserGrade(UserGrade userGrade) {
        userGradeMapper.insert(userGrade);
        redisUtil.lHasSet(KEY, userGrade);
        return true;
    }

    @Override
    public Boolean updateUserGradeById(UserGrade userGrade) {
        userGradeMapper.updateById(userGrade);
        if (redisUtil.hasKey(KEY)){
            redisUtil.lRemove(KEY, 1, userGradeMapper.selectById(userGrade.getId()));
            redisUtil.lSet(KEY, userGrade);
            redisUtil.del(KEY + userGrade.getId());
        }
        return true;
    }

    @Override
    public Boolean delUserGrade(Integer id) {
        redisUtil.lRemove(KEY, 1, userGradeMapper.selectById(id));
        userGradeMapper.deleteById(id);
        redisUtil.del(KEY + id);
        return true;
    }

    @Override
    public UserGrade selectByName(String gName) {
        return userGradeMapper.selectOne(new QueryWrapper<UserGrade>().eq("name", gName));
    }

    @Override
    public List<UserGrade> saveCache() {
        Lock lock = new ReentrantLock();
        lock.lock();
        try {
            if (redisUtil.hasKey(KEY)){
                return redisUtil.lGetAll(KEY);
            }
            List<UserGrade> userGrades = userGradeMapper.selectList(null);
            redisUtil.lSet(KEY, userGrades, RedisUtil.EXPIRETIME);
            return userGrades;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Integer selectEidByGid(Integer gid) {
        String key = KEY + gid;
        try {
            if (redisUtil.hasKey(key)){
                return Integer.parseInt(redisUtil.get(key).toString());
            }
        } catch (RedisConnectionFailureException e){
            e.printStackTrace();
            return userGradeMapper.selectById(gid).getEducationId();
        }
        Integer educationId = userGradeMapper.selectById(gid).getEducationId();
        redisUtil.setIfAbsent(key, educationId, RedisUtil.EXPIRETIME);
        return educationId;
    }
}
