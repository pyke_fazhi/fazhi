package com.zgn.fazhi.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.zgn.fazhi.pojo.po.VisitContent;
import com.zgn.fazhi.mapper.VisitContentMapper;
import com.zgn.fazhi.service.VisitContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class VisitContentServiceImpl extends ServiceImpl<VisitContentMapper, VisitContent> implements VisitContentService {

    @Autowired
    private VisitContentMapper visitContentMapper;

    private static final String KEY = "visitContent";

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public List<VisitContent> getAllVisitContent() {
        // 从缓存中查询
        if (redisUtil.hasKey(KEY)) {
            return redisUtil.lGet(KEY, 0, -1);
        }
        return selectPutCache();
    }

    @Override
    public VisitContent getAllVisitContentById(Integer id) {
        return visitContentMapper.selectById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addVisitContent(VisitContent visitContent) {
        visitContentMapper.insert(visitContent);
        redisUtil.lHasSet(KEY, visitContent);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteVisitContentById(Integer id) {
        if (redisUtil.hasKey(KEY)) {
            redisUtil.lRemove(KEY, 1, visitContentMapper.selectById(id));
        }
        visitContentMapper.deleteById(id);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateVisitContent(VisitContent visitContent) {
        visitContentMapper.updateById(visitContent);
        if (redisUtil.hasKey(KEY)) {
            redisUtil.lRemove(KEY, 1, visitContentMapper.selectById(visitContent.getId()));
            redisUtil.lSet(KEY, visitContent);
        }
        return true;
    }

    private List<VisitContent> selectPutCache() {
        Lock lock = new ReentrantLock();
        // 从数据库中获取返回并写在缓存中
        lock.lock();
        try {
            if (redisUtil.hasKey(KEY)){
                return redisUtil.lGetAll(KEY);
            }
            List<VisitContent> visitContents = visitContentMapper.selectList(null);
            redisUtil.lSet(KEY, visitContents, RedisUtil.EXPIRETIME);
            return visitContents;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Boolean refreshRedis() {
        redisUtil.del(KEY);
        selectPutCache();
        log.info("刷新预约参观地点缓存");
        return true;
    }

    @Override
    public long checkExpire() {
        return redisUtil.getExpire(KEY);
    }
}
