package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.SignIn;
import com.zgn.fazhi.mapper.SignInMapper;
import com.zgn.fazhi.service.SignInService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
public class SignInServiceImpl extends ServiceImpl<SignInMapper, SignIn> implements SignInService {

}
