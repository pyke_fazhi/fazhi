package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.controller.AppointmentController;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.mapper.AppointmentContentMapper;
import com.zgn.fazhi.pojo.po.Appointment;
import com.zgn.fazhi.mapper.AppointmentMapper;
import com.zgn.fazhi.pojo.po.AppointmentContent;
import com.zgn.fazhi.pojo.po.IntervalInfo;
import com.zgn.fazhi.pojo.po.VisitContent;
import com.zgn.fazhi.pojo.vo.AppointmentInfo;
import com.zgn.fazhi.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class AppointmentServiceImpl extends ServiceImpl<AppointmentMapper, Appointment> implements AppointmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentServiceImpl.class);

    @Autowired
    private AppointmentContentMapper appointmentContentMapper;

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Autowired
    private VisitContentService visitContentService;

    @Autowired
    private AppointmentConfigService appointmentConfigService;

    @Autowired
    private IntervalInfoService intervalInfoService;

    @Autowired
    private SchoolService schoolService;


    /**
     * 通过日期查找预约数据
     *
     * @param date 日期
     * @return 返回预约数据
     */
    @Override
    public ArrayList<Map<String, Object>> getAppointmentByDate(String date) {
        // 创建数组存储当天预约数据
        ArrayList<Map<String, Object>> result = new ArrayList<>();
        // 通过日期、时间段查询出当天的预约情况
        // 获取全部时间段
        intervalInfoService.getAllInterval().forEach(interval -> {
            Map<String, Object> map = new HashMap<>();
            map.put("interval_id", interval.getId());
            map.put("info", interval.getInfo());
            List<Appointment> appointments =
                    appointmentMapper.selectList(
                            new QueryWrapper<Appointment>()
                                    .eq("go_date", date)
                                    .eq("interval_id", interval.getId()));
            // 如果当前时间段预约记录小于最多预约数则标记为可预约
            if (appointments.size() < appointmentConfigService.getMaxNumber()) {
                map.put("isFull", 0);
            } else {
                map.put("isFull", 1);
            }
            result.add(map);
        });
        return result;
    }

    /**
     * 通过日期和预约时间段id查找预约记录
     *
     * @param date 预约日期
     * @param iId  预约时间段id
     * @return 返回符合的记录
     */
    @Override
    public List<Appointment> getAppointmentByDateAndIid(Date date, Integer iId) {
        return appointmentMapper.selectList(
                new QueryWrapper<Appointment>()
                        .eq("go_date", date)
                        .eq("interval_id", iId));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean insertAppointment(AppointmentInfo appointment) {
        Appointment info = appointment.getInfo();
        // 判断当前日期，当前时间段预约是否满人
        List<Appointment> appointments =
                appointmentMapper.selectList(
                        new QueryWrapper<Appointment>()
                                .eq("go_date", info.getGoDate())
                                .eq("interval_id", info.getIntervalId()));
        log.info("当前预约数: {}", appointments.size());
        if (appointments.size() >= appointmentConfigService.getMaxNumber()) {
            throw new RestException(400, "预约失败，当前时间段预约已满人");
        }
        appointmentMapper.insert(info);
        AppointmentContent appointmentContent = new AppointmentContent();
        // 获取插入预约记录后的id
        appointmentContent.setAid(appointment.getInfo().getId());

        for (Integer vid : appointment.getVisitList()) {
            appointmentContent.setVid(vid);
            appointmentContentMapper.insert(appointmentContent);
        }
        return true;

    }

    /**
     * 获取所有预约数据(包括该预约记录的参观地点id
     */
    @Override
    public List<Appointment> getAllApp() {
        return appointmentMapper.selectList(null);
    }

    /**
     * 通过id删除预约数据
     *
     * @param id 需要删除预约的id
     * @return 是否删除成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delAppById(Integer id) {
        // 需要删除两张表的数据
        appointmentMapper.deleteById(id);
        appointmentContentMapper.delete(
                new QueryWrapper<AppointmentContent>()
                        .eq("aid", id));
        return true;
    }

    @Override
    public Boolean isFull(Appointment appointment) {
        // 插入前 需要判断当日日期 时间段预约是否满人
        // 通过日期、时间段id查询记录是否超过2条，大于2条则无法预约
        List<Appointment> appointmentList = getAppointmentByDateAndIid(appointment.getGoDate(), appointment.getIntervalId());
        if (appointmentList.size() >= 2) {
            // 通过iId获取时间段
            IntervalInfo intervalInfo = intervalInfoService.getIntervalInfoById(appointment.getIntervalId());
            String msg = "当前日期" + appointment.getGoDate() + "时间段: " + intervalInfo.getInfo() + "已经预约满";
            LOGGER.warn(msg);
            throw new RestException(400, msg);
        }
        return true;
    }

    @Override
    public List<Appointment> findAllByUid(Integer uid) {
        List<Appointment> appointments = appointmentMapper.selectList(new QueryWrapper<Appointment>().eq("uid", uid));
        appointments.forEach(appointment ->
                appointment
                        .setPlace(appointmentContentMapper.getContentByAid(appointment.getId()))
                        .setSchoolName(schoolService.getByUid(uid)));
        return appointments;
    }

}
