package com.zgn.fazhi.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.mapper.ExaminationPermissionMapper;
import com.zgn.fazhi.pojo.po.ExaminationPermission;
import com.zgn.fazhi.pojo.vo.ExaPermission;
import com.zgn.fazhi.pojo.vo.ExaPermissionVo;
import com.zgn.fazhi.pojo.vo.ExaPermissionVoList;
import com.zgn.fazhi.pojo.vo.OpenRestriction;
import com.zgn.fazhi.service.ExaminationPermissionService;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ydw
 */
@Service
@Slf4j
public class ExaminationPermissionServiceImpl extends ServiceImpl<ExaminationPermissionMapper, ExaminationPermission> implements ExaminationPermissionService {

    @Autowired
    private ExaminationPermissionMapper permissionMapper;

    private static final String KEY = "EXAMINATION_PERMISSION";

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public Boolean addExaPermission(ExaPermission exaPermission) {
        List<OpenRestriction> permission = exaPermission.getPermission();

        ExaminationPermission examinationPermission = new ExaminationPermission();
        examinationPermission.setRecordId(exaPermission.getRecordId());

        permission.forEach(openRestriction -> {
            // 班级id集合
            List<Integer> classIdList = openRestriction.getClassId();
            // 年级id集合
            List<Integer> gradeIdList = openRestriction.getGradeId();
            // 学校id
            Integer schoolId = openRestriction.getSchoolId();
            if (StringUtils.isEmpty(schoolId)) {
                throw new RestException(400, "题目题型配置学校id不能为空");
            }
            examinationPermission.setSchoolId(schoolId);

            // 如果年级为空，表示开放到学校单位
            if (StringUtils.isEmpty(gradeIdList)) {
                permissionMapper.insert(examinationPermission);
            }

            // 如果班级为空且年级不为空，表示开放到年级单位
            if (StringUtils.isEmpty(classIdList) && !StringUtils.isEmpty(gradeIdList)) {
                openRestriction.getGradeId().forEach(gid -> {
                    examinationPermission.setGradeId(gid);
                    permissionMapper.insert(examinationPermission);
                });
            }
            // 如果开放到班级，全字段都不能为空
            if (!StringUtils.isEmpty(classIdList)) {
                examinationPermission.setGradeId(gradeIdList.get(0));
                classIdList.forEach(cid -> {
                    examinationPermission.setClassId(cid);
                    permissionMapper.insert(examinationPermission);
                });
            }
        });
        redisUtil.del(KEY + exaPermission.getRecordId());
        return true;
    }

    @Override
    public Boolean delExaPermissionById(List<Integer> idList) {
        // 清理缓存
        try {
            permissionMapper.selectBatchIds(idList)
                    .stream()
                    .map(ExaminationPermission::getRecordId)
                    .distinct()
                    .collect(Collectors.toList())
                    .forEach(rid -> redisUtil.del(KEY + rid));
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        permissionMapper.deleteBatchIds(idList);
        return true;
    }

    @Override
    public List<Integer> selectAll(Integer schoolId) {
        return permissionMapper.selectList(
                new QueryWrapper<ExaminationPermission>()
                        .eq("school_id", schoolId)
                        .and(wrapper1 -> wrapper1.isNull("grade_id")))
                .stream()
                .map(ExaminationPermission::getRecordId)
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> selectAll(Integer schoolId, Integer gradeId) {
        return permissionMapper.selectList(
                new QueryWrapper<ExaminationPermission>()
                        .eq("school_id", schoolId)
                        .eq("grade_id", gradeId)
                        .isNull("class_id"))
                .stream()
                .map(ExaminationPermission::getRecordId)
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> selectAll(Integer schoolId, Integer gradeId, Integer classId) {
        return permissionMapper.selectList(
                new QueryWrapper<ExaminationPermission>()
                        .eq("school_id", schoolId)
                        .eq("grade_id", gradeId)
                        .eq("class_id", classId))
                .stream()
                .map(ExaminationPermission::getRecordId)
                .collect(Collectors.toList());
    }

    @Override
    public ExaPermissionVoList getExaPermissionByRid(Integer rid) {
        String key = KEY + rid;
        try {
            if (redisUtil.hasKey(key)) {
                return FastJsonUtils.toBean(redisUtil.get(KEY), ExaPermissionVoList.class);
            }
        } catch (RedisConnectionFailureException e) {
            return getExaPermissionVo(rid);
        }
        ExaPermissionVoList exaPermissionVo = getExaPermissionVo(rid);
        redisUtil.setIfAbsent(key, exaPermissionVo, RedisUtil.EXPIRETIME);
        return exaPermissionVo;
    }

    private ExaPermissionVoList getExaPermissionVo(Integer rid) {
        // 分组
        ExaPermissionVoList exaPermissionVoList = new ExaPermissionVoList();
        // class
        exaPermissionVoList.setClassList(permissionMapper.selectVoClassList(rid));
        // grade
        exaPermissionVoList.setGradeList(permissionMapper.selectVoGradeList(rid));
        // school
        exaPermissionVoList.setSchoolList(permissionMapper.selectVoSchoolList(rid));
        log.info(JSON.toJSONString(exaPermissionVoList));
        return exaPermissionVoList;
    }

}
