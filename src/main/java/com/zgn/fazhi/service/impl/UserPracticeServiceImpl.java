package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.UserPractice;
import com.zgn.fazhi.mapper.UserPracticeMapper;
import com.zgn.fazhi.service.UserPracticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
public class UserPracticeServiceImpl extends ServiceImpl<UserPracticeMapper, UserPractice> implements UserPracticeService {

}
