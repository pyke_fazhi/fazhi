package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.controller.AppointmentController;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.pojo.po.*;
import com.zgn.fazhi.mapper.UserExaminationMapper;
import com.zgn.fazhi.pojo.vo.*;
import com.zgn.fazhi.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.BelongCalendar;
import com.zgn.fazhi.utils.ExaminationUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.text.DecimalFormat;
import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class UserExaminationServiceImpl extends ServiceImpl<UserExaminationMapper, UserExamination> implements UserExaminationService {

    @Autowired
    private UserExaminationMapper userExaminationMapper;

    @Autowired
    @Lazy
    private ExaminationRecordService examinationRecordService;

    @Autowired
    private ExaminationPermissionService examinationPermissionService;

    @Autowired
    private UserInfoService userInfoService;

    @Override
    public List<UserExamination> findAllByUid(Integer uid, Integer recordId) {
        return userExaminationMapper.selectList(
                new QueryWrapper<UserExamination>()
                        .eq("uid", uid)
                        .eq("record_id", recordId));
    }


    @Override
    public int ifExamination(Integer uid, Integer rid) {
        QueryWrapper<UserExamination> userExaminationQueryWrapper = new QueryWrapper<>();
        userExaminationQueryWrapper.eq("uid", uid).and(wrapper -> wrapper.eq("record_id", rid));
        List<UserExamination> userExaminations = userExaminationMapper.selectList(userExaminationQueryWrapper);
        // 查询该次考试最大次数
        // 后期加入redis缓存策略
        ExaminationRecord record = examinationRecordService.findById(rid);
        if (userExaminations.size() < record.getTimes()) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public List<Integer> getScoreByUid(Integer uid, Integer recordId) {
        List<UserExamination> allByUid = findAllByUid(uid, recordId);
        ArrayList<Integer> scores = new ArrayList<>();
        if (allByUid.isEmpty()) {
            return null;
        }
        for (UserExamination userExamination : allByUid) {
            if (!StringUtils.isEmpty(userExamination.getAchievement1())) {
                scores.add(userExamination.getAchievement1());
            } else {
                log.error("当前考试成绩为空uid:" + uid + ",rid: " + recordId);
            }
        }
        return scores;
    }

    @Override
    public ExaGradesDate getExaDataByRid(Integer rid) {
        //平均分、正确率、参与人数、参与率
        // 通过rid查询该次考试练习的所有数据
        List<UserExamination> userExaminations =
                userExaminationMapper.selectList(
                        new QueryWrapper<UserExamination>()
                                .eq("record_id", rid));
        if (userExaminations.isEmpty()) {
            throw new RestException(400, "该次考试练习无数据");
        }
        // 获取最高分、最低分
        Integer max = userExaminations.stream().max(Comparator.comparing(UserExamination::getAchievement1)).get().getAchievement1();
        Integer min = userExaminations.stream().min(Comparator.comparing(UserExamination::getAchievement1)).get().getAchievement1();
        // 获取平均值
        double avgScore = userExaminations.stream().mapToDouble(UserExamination::getAchievement1).average().getAsDouble();
        // 计算及格率
        Integer count = (int) userExaminations.stream().filter(userExamination -> userExamination.getAchievement1() >= 60).count();
        DecimalFormat df = new DecimalFormat("0.00");
        String passRate = df.format((count / userExaminations.size()));
        // 计算参与人数
        Integer total = takePartCount(rid);
        // 计算参与率
        int takePartCount = userExaminations.size();
        int i = takePartCount * 100 / total;
        log.info("参与率 : {}", i);

        ExaGradesDate exaGradesDate = new ExaGradesDate();
        exaGradesDate.setAvg(avgScore)
                .setMaxScore(max)
                .setMinScore(min)
                .setNumOfPeople(takePartCount)
                .setPassRate(passRate)
                .setParticipationRate(i);
        return exaGradesDate;
    }

    private Integer takePartCount(Integer rid) {
        if (examinationRecordService.getById(rid).getIfPublic() == 1) {
            // 如果是公开，直接返回当前全部用户数量
            return userInfoService.countUser();
        }
        ExaPermissionVoList exaPermissionByRid = examinationPermissionService.getExaPermissionByRid(rid);
        List<ExaPermissionVo> schoolList = exaPermissionByRid.getSchoolList();
        List<ExaPermissionVo> gradeList = exaPermissionByRid.getGradeList();
        List<ExaPermissionVo> classList = exaPermissionByRid.getClassList();
        List<Integer> idList = new ArrayList<>();
        if (!StringUtils.isEmpty(schoolList)) {
            for (ExaPermissionVo exaPermissionVo : schoolList) {
                // 查询所有学校id，查找是该学校的所有人的id
                idList.addAll(userInfoService.getIdListBySchoolId(exaPermissionVo.getSchoolId()));
            }
        }
        if (!StringUtils.isEmpty(gradeList)) {
            for (ExaPermissionVo exaPermissionVo : schoolList) {
                // 查询该学校和该年级
                idList.addAll(userInfoService.getIdListByGradeId(exaPermissionVo.getSchoolId(), exaPermissionVo.getGradeId()));
            }
        }
        if (!StringUtils.isEmpty(classList)) {
            for (ExaPermissionVo exaPermissionVo : schoolList) {
                // 查询班级
                idList.addAll(userInfoService.getIdListByClassId(exaPermissionVo.getClassId()));
            }
        }
        // 去重计数返回
        return (int) idList.stream().distinct().count();
    }
}
