package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.AppointmentContent;
import com.zgn.fazhi.mapper.AppointmentContentMapper;
import com.zgn.fazhi.service.AppointmentContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
public class AppointmentContentServiceImpl extends ServiceImpl<AppointmentContentMapper, AppointmentContent> implements AppointmentContentService {

    @Autowired
    private AppointmentContentMapper appointmentContentMapper;

    /**
     * 查询所有可参观地点
     * @return
     */
    @Override
    public List<AppointmentContent> getAllContent() {
        return appointmentContentMapper.selectList(null);
    }
}
