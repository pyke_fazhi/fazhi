package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.controller.IndexController;
import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.WebInfo;
import com.zgn.fazhi.mapper.WebInfoMapper;
import com.zgn.fazhi.service.WebInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class WebInfoServiceImpl extends ServiceImpl<WebInfoMapper, WebInfo> implements WebInfoService {

    @Autowired
    private WebInfoMapper webInfoMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public WebInfo getWebInfo() {
        return webInfoMapper.selectOne(null);
    }

    @Override
    public Boolean saveWebInfo(Integer views) {
        webInfoMapper.updateById(
                getWebInfo().setTimes(views));
        return true;
    }

    /**
     * 访问人数自增
     *
     * @return 返回访问人数
     */
    @Override
    public Integer incr() {
        try {
            // 访问人数递增1 ~ 3
            Integer views = (int) redisUtil.incr("views", ThreadLocalRandom.current().nextInt(1, 4));
            if ((views % 75) <= 3) {
                saveWebInfo(views);
            }
            return views;
        } catch (RedisConnectionFailureException e) {
            // 如果redis挂掉，向数据库查询访问人数并自增
            WebInfo webInfo = getWebInfo();
            Integer views = webInfo.getTimes();
            views++;
            saveWebInfo(views);
            return views;
        }
    }
}
