package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.config.RedisConfig;
import com.zgn.fazhi.pojo.po.Picture;
import com.zgn.fazhi.pojo.po.Video;
import com.zgn.fazhi.mapper.VideoMapper;
import com.zgn.fazhi.pojo.vo.FileUploadForm;
import com.zgn.fazhi.service.FileUploadService;
import com.zgn.fazhi.service.VideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.validation.constraints.NotNull;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class VideoServiceImpl extends ServiceImpl<VideoMapper, Video> implements VideoService {

    private static final String KEY = "videoResKey";

    private static final String VIDEO_PATTERN = "([^\\s]+(\\.(?i)(AVI|mov|rmvb|rm|FLV|mp4|3GP))$)";

    @Autowired
    private FileUploadService fileUploadService;

    @Autowired
    private VideoMapper videoMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean videoUpload(FileUploadForm fileUploadForm) {
        Integer typeId = fileUploadForm.getTypeId();
        String url = fileUploadService.upload(fileUploadForm.getUploadFile(),
                "video", typeId, VIDEO_PATTERN,
                fileUploadForm.getId());
        Video video = new Video();
        video.setCreateTime(new Date())
                .setInfo(fileUploadForm.getInfo())
                .setTypeId(typeId)
                .setTitle(fileUploadForm.getTitle())
                .setUrl(url)
                // 视频截帧
                .setScreenshot(screenshot(fileUploadForm.getUploadFile()));
        videoMapper.insert(video);

        // 清理缓存
        try {
            redisUtil.del(KEY + typeId);
        } catch (RedisConnectionFailureException ignored){}
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delById(Integer id) {
        Video video = videoMapper.selectById(id);
        // 通过id获取路径
        fileUploadService.delete(video.getUrl());
        videoMapper.deleteById(id);
        // ,查询出该视频的分类,清理缓存
        try {
            redisUtil.del(KEY + video.getTypeId());
        } catch (RedisConnectionFailureException ignored){}
        return true;
    }

    @Override
    public List<Video> listByTid(Integer tid) {
        String key = KEY + tid;
        try {
            if (redisUtil.hasKey(key)){
                return FastJsonUtils.toList(redisUtil.get(key), Video.class);
            }
        } catch (RedisConnectionFailureException e){
            e.printStackTrace();
            return videoMapper.selectList(new QueryWrapper<Video>().eq("type_id", tid));
        }

        List<Video> videoList = videoMapper.selectList(new QueryWrapper<Video>().eq("type_id", tid));
        redisUtil.setIfAbsent(key, videoList, RedisUtil.EXPIRETIME);
        return videoList;
    }

    @Override
    public String screenshot(MultipartFile uploadFile) {
        try {
            //放入采集器
            FFmpegFrameGrabber fFmpegFrameGrabber = new FFmpegFrameGrabber(new ByteArrayInputStream(uploadFile.getBytes()));
            //启动
            fFmpegFrameGrabber.start();
            //截取图片
            BufferedImage bufferedImage = new Java2DFrameConverter().getBufferedImage(fFmpegFrameGrabber.grabImage());

            //创建一个ByteArrayOutputStream
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            //输出 可以不用
            ImageIO.write(bufferedImage, "jpg", os);
            InputStream input = new ByteArrayInputStream(os.toByteArray());

            // 截取文件名
            String fileName = "file/video/screenshot/" +
                    System.currentTimeMillis() +
                    // 获取区间的随机数
                    ThreadLocalRandom.current().nextInt(100, 10000) + ".jpg";
            log.info("截图路径: {}", fileName);
            // 上传阿里oss
            try {
                fileUploadService.upload(input, fileName);
            } finally {
                input.close();
            }
            fFmpegFrameGrabber.stop();
            return fileName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer addLiveVideo(Video video) {
        videoMapper.insert(video);

        // 清理缓存
        try {
            redisUtil.del(KEY + video.getTypeId());
        } catch (RedisConnectionFailureException ignored){}
        return video.getId();
    }
}
