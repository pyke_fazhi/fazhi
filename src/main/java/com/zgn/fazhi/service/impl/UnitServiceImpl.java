package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.mapper.UnitMapper;
import com.zgn.fazhi.pojo.po.Unit;
import com.zgn.fazhi.service.UnitService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
public class UnitServiceImpl extends ServiceImpl<UnitMapper, Unit> implements UnitService {

    @Autowired
    private UnitMapper unitMapper;

    @Override
    public Boolean addUnit(Unit unit) {
        unitMapper.insert(unit);
        return true;
    }

    @Override
    public Boolean delUnitById(Integer id) {
        unitMapper.deleteById(id);
        return true;
    }

    @Override
    public List<Unit> findAllUnit() {
        return unitMapper.selectList(null);
    }

    @Override
    public Unit findById(Integer id) {
        return unitMapper.selectById(id);
    }

    @Override
    public Unit findByTitle(String title) {
        QueryWrapper<Unit> unitQueryWrapper = new QueryWrapper<>();
        unitQueryWrapper.eq("title", title);
        return unitMapper.selectOne(unitQueryWrapper);
    }
}
