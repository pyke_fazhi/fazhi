package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.pojo.po.Wrong;
import com.zgn.fazhi.mapper.WrongMapper;
import com.zgn.fazhi.service.WrongService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
public class WrongServiceImpl extends ServiceImpl<WrongMapper, Wrong> implements WrongService {

    @Autowired
    private WrongMapper wrongMapper;


    @Override
    public List<Wrong> findByUid(Integer uid) {
        QueryWrapper<Wrong> wrongQueryWrapper = new QueryWrapper<>();
        wrongQueryWrapper.eq("user_id", uid);
        return wrongMapper.selectList(wrongQueryWrapper);
    }

    @Override
    public Boolean addWrong(Integer uid, Integer pid) {
        Wrong wrong = new Wrong();
        wrong.setUserId(uid);
        wrong.setPracticeId(pid);
        wrongMapper.insert(wrong);
        return true;
    }

    @Override
    public Boolean delWrong(Integer uid, Integer pid) {
        // 先查看当前用户是否有该练习题
        QueryWrapper<Wrong> wrongQueryWrapper = new QueryWrapper<>();
        wrongQueryWrapper.eq("practice_id", pid).and(wrapper -> wrapper.eq("user_id", uid));
        Wrong wrong = wrongMapper.selectOne(wrongQueryWrapper);
        if (StringUtils.isEmpty(wrong)) {
            throw new RestException(400, "不存在该练习题");
        }
        wrongMapper.delete(wrongQueryWrapper);
        return true;
    }
}
