package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.mapper.LiveAudienceMapper;
import com.zgn.fazhi.pojo.po.LiveAudience;
import com.zgn.fazhi.service.LiveAudienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 21/08/2021 8:38 PM
 */

@Service
public class LiveAudienceServiceImpl extends ServiceImpl<LiveAudienceMapper, LiveAudience> implements LiveAudienceService {

    @Autowired
    private LiveAudienceMapper mapper;

    @Override
    public LiveAudience selectByUidRecordId(Integer uid, Integer recordId) {
        return mapper.selectOne(
                new QueryWrapper<LiveAudience>()
                        .eq("uid", uid)
                        .eq("record_id", recordId));
    }
}
