package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.mapper.RankInfoMapper;
import com.zgn.fazhi.pojo.po.RankInfo;
import com.zgn.fazhi.pojo.po.UserTime;
import com.zgn.fazhi.mapper.UserTimeMapper;
import com.zgn.fazhi.pojo.vo.UserTimeData;
import com.zgn.fazhi.service.RankInfoService;
import com.zgn.fazhi.service.UserInfoService;
import com.zgn.fazhi.service.UserTimeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalInt;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
public class UserTimeServiceImpl extends ServiceImpl<UserTimeMapper, UserTime> implements UserTimeService {

    @Autowired
    private UserTimeMapper userTimeMapper;

    @Autowired
    private RankInfoService rankInfoService;

    @Override
    public String getRankByTimeLength(Integer uid) {
        // 所有等级所需的时间
        List<RankInfo> rankList = rankInfoService.getRank();

        // 查询用户使用时长
        Integer userTime = userTimeMapper.timeLength(uid);
        if (StringUtils.isEmpty(userTime)) {
            // 当前用户使用时长数据为空则返回最低等级
            return rankList.stream()
                    .min(Comparator.comparing(RankInfo::getTimeLength))
                    .get()
                    .getName();
        }
        return rankList.stream()
                .filter(time -> time.getTimeLength() <= userTime)
                .max(Comparator.comparing(RankInfo::getTimeLength))
                .get()
                .getName();
    }

    @Override
    public Boolean saveUserTime(Integer uid, Integer time) {
        // 用户现在使用时长
        UserTime userTime1 = userTimeMapper.selectOne(new QueryWrapper<UserTime>().eq("uid", uid));
        if (StringUtils.isEmpty(userTime1)) {
            // 如果为空则插入
            userTimeMapper.insert(
                    new UserTime()
                            .setUid(uid)
                            .setTimeLength(time));
        } else {
            // 如果有则更新
            userTimeMapper.updateById(
                    userTime1.setTimeLength(
                            userTime1.getTimeLength() + time));
        }
        return true;
    }

    @Override
    public List<UserTimeData> getUserTimeData() {
        return userTimeMapper.selectAllData();
    }
}
