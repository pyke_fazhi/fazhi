package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.mapper.AppointmentConfigMapper;
import com.zgn.fazhi.pojo.po.AppointmentConfig;
import com.zgn.fazhi.service.AppointmentConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ydw
 */
@Service
@Slf4j
public class AppointmentConfigServiceImpl extends ServiceImpl<AppointmentConfigMapper, AppointmentConfig> implements AppointmentConfigService {

    @Autowired
    private AppointmentConfigMapper appointmentConfigMapper;

    @Override
    public Integer getMaxNumber() {
        AppointmentConfig appointmentConfig = appointmentConfigMapper.selectOne(null);
        return appointmentConfig.getMaxNumber();
    }
}
