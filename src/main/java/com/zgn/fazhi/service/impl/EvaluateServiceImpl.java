package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.Evaluate;
import com.zgn.fazhi.mapper.EvaluateMapper;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.EvaluateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class EvaluateServiceImpl extends ServiceImpl<EvaluateMapper, Evaluate> implements EvaluateService {

    private static final String KEY = "evaluateKey";

    @Autowired
    private EvaluateMapper evaluateMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public List<Evaluate> getAll() {
        return evaluateMapper.selectList(null);
    }

    @Override
    public Boolean addEvaluate(Evaluate evaluate, Integer uid) {
        isEvaluate(uid);
        // 没有评价则查询数据库查询是否有评价记录
        // 如果数据库没有记录，则插入记录，并且写入缓存
        evaluateMapper.insert(evaluate);
        redisUtil.mark(KEY, uid, true);
        return true;
    }

    @Override
    public Boolean getEvaluate(Integer uid) {
        return isEvaluate(uid);
    }

    private boolean isEvaluate(Integer uid) {
        Boolean container = redisUtil.container(KEY, uid);
        log.info("当前用户id:{}是否评价:{}", uid, container);
        // 为false的情况只有两种 1、没有缓存 2、没有评价
        // 如果未评价或没有缓存，则向数据库查询记录
        if (container || !StringUtils.isEmpty(evaluateMapper.selectOne(new QueryWrapper<Evaluate>().eq("uid", uid)))) {
            throw new RestException(400, "当前用户已经评价了");
        }
        return true;
    }

    @Override
    public Boolean refresh() {
        // 删除缓存，重新从数据库中查询再写入缓存中
        redisUtil.del(KEY);
        getAll().forEach(evaluate -> redisUtil.mark(KEY, evaluate.getUid(), true));
        return true;
    }
}
