package com.zgn.fazhi.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.pojo.po.*;
import com.zgn.fazhi.mapper.ExaminationRecordMapper;
import com.zgn.fazhi.pojo.vo.*;
import com.zgn.fazhi.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.BelongCalendar;
import com.zgn.fazhi.utils.ExaminationUtils;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotEmpty;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class ExaminationRecordServiceImpl extends ServiceImpl<ExaminationRecordMapper, ExaminationRecord> implements ExaminationRecordService {

    @Autowired
    private ExaminationRecordMapper examinationRecordMapper;

    @Autowired
    private SubjectTypeService subjectTypeService;

    @Autowired
    private UserExaminationService userExaminationService;

    @Autowired
    private EtypeScoreService etypeScoreService;

    @Autowired
    private ExaminationService examinationService;

    @Autowired
    private ExaminationPermissionService permissionService;

    @Autowired
    private ExaminationUtils examinationUtils;

    @Autowired
    private UserExaminationScoreService userExaminationScoreService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private GeneralExaminationService generalExaminationService;

    @Autowired
    private ExaminationAnswerService examinationAnswerService;

    private static final String KEY = "examinationRecordKey";

    private static final String SUBKEY = "subExaminationRecordKey";

    private static final Logger LOGGER = LoggerFactory.getLogger(ExaminationRecordServiceImpl.class);


    @Override
    public void addRecord(ExaminationRecord examinationRecord) {
        examinationRecordMapper.insert(examinationRecord);
    }

    @Override
    public List<ExaminationRecord> findAllExamination() {
        return examinationRecordMapper.selectList(null);
    }

    @Override
    public ExaminationRecord findById(Integer id) {
        return examinationRecordMapper.selectById(id);
    }

    /**
     * 开始一次统考
     * 需要向两张表插入，开启事务
     *
     * @param record 统考的基本设置
     * @return 是否开启成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addExaminationRecord(AddRecord record) {
        // 获取该次统考的基本设置，写入数据库
        ExaminationRecord examinationRecord = record.getExaminationRecord();
        // 做合法判断
        // 获取统考开始时间和结束时间
        long start = examinationRecord.getStart().getTime();
        long end = examinationRecord.getEnd().getTime();
        // 开始时间必须小于结束时间
        if (start >= end) {
            throw new RestException(400, "开始时间必须小于结束时间");
        }
        // 考试时长不能为空且小于等于0
        if (examinationRecord.getLengthTime() == null || examinationRecord.getLengthTime() <= 0) {
            throw new RestException(400, "考试时长不能为空且小于等于0");
        }
        // 获取统考题型，检查是否在数据库题型范围内
        List<EtypeScore> etypeScore = record.getEtypeScore();
        // 获取数据库所有题型
        List<SubjectType> allSubjectType = subjectTypeService.getAllSubjectType();
        // 非空判断
        if (allSubjectType.isEmpty()) {
            throw new RestException(400, "数据库中题型数据为空");
        }
        boolean isType = false;
        out:
        try {
            for (EtypeScore score : etypeScore) {
                for (SubjectType subjectType : allSubjectType) {
                    if (score.getTid().equals(subjectType.getId())) {
                        isType = true;
                        break out;
                    }
                }
            }
        } catch (NullPointerException e) {
            log.error("当前题型配置为空");
            throw new RestException(400, "题型配置为空");
        }
        if (!isType) {
            throw new RestException(400, "不在数据库题型范围内");
        }
        Integer ifPublic = examinationRecord.getIfPublic();
        if (ifPublic != 1 && ifPublic != 0) {
            throw new RestException(400, "公开参数错误");
        }
        Integer ifPractice = examinationRecord.getIfpractice();
        if (ifPractice != 1 && ifPractice != 0) {
            throw new RestException(400, "选择标明练习/考试参数错误");
        }

        // 校验合法结束，开始插入数据库
        addRecord(examinationRecord);
        // 如果为公开则刷新缓存
        if (examinationRecord.getIfPublic() == 1) {
            redisUtil.lSet(KEY + ":1:" + ifPractice, examinationRecord);
        }
        // 获取该次考试/练习id
        Integer id = examinationRecord.getId();
        // 向该次考试/练习插入题型设置
        for (EtypeScore score : etypeScore) {
            score.setEid(id);
            etypeScoreService.insertEtype(score);
        }
        return true;
    }

    @Override
    public ExaRecordMap getExaminationByUid(UserInfo userInfo, Integer ifPractice) {
        // 查询缓存查询出所有的考试练习
        List<ExaminationRecord> examinationRecords = selectExaInfo(userInfo, ifPractice);
        LOGGER.info("当前用户所有考试练习: " + examinationRecords);
        // 分组
        return integration(examinationRecords, userInfo.getId());
    }

    public List<ExaminationRecord> selectExa(UserInfo userInfo) {
        // 获取当前用户的学校id
        Integer schoolId = userInfo.getSchoolId();
        // 获取当前用户的年级id
        Integer gradeId = userInfo.getGradeId();
        List<ExaminationRecord> examinationRecords = new ArrayList<>();
        // 查询考试/练习权限
        log.info("学校查询");
        // 先通过学校id且年级为空条件 获取所有开放于该学校的,
        List<Integer> idList = permissionService.selectAll(schoolId);
        log.info("查询出所有记录id: {}", idList);
        if (!idList.isEmpty()) {
            List<ExaminationRecord> examinationRecords1 = listByIds(idList);
            examinationRecords.addAll(examinationRecords1);
        }
        log.info("年级查询");
        // 通过学校id和年级id且班级为空的条件获取
        idList = permissionService.selectAll(schoolId, gradeId);
        if (!idList.isEmpty()) {
            List<ExaminationRecord> examinationRecords1 = listByIds(idList);
            examinationRecords.addAll(examinationRecords1);
        }
        log.info("级别查询");
        // 通过学校id、年级id、班级id条件查找
        idList = permissionService.selectAll(schoolId, gradeId, userInfo.getClassId());
        if (!idList.isEmpty()) {
            List<ExaminationRecord> examinationRecords1 = listByIds(idList);
            examinationRecords.addAll(examinationRecords1);
        }
        return examinationRecords;
    }

    public ExaRecordMap integration(List<ExaminationRecord> collect, Integer uid) {
        ArrayList<ExaRecord> start = new ArrayList<>();
        ArrayList<ExaRecord> end = new ArrayList<>();
        collect.forEach(System.out::println);
        log.info("当前学生所有记录: {}", collect);
        collect.forEach(record -> {
            LOGGER.info("当前遍历到的所有考试 :" + collect);
            ExaRecord exaRecord = new ExaRecord();
            exaRecord.setEnd(record.getEnd())
                    .setStart(record.getStart())
                    .setLength(record.getLengthTime())
                    .setRecordId(record.getId())
                    .setTitle(record.getTitle());
            // 查询当前用户练习次数是否上限
            exaRecord.setIfParticipate(userExaminationService.findAllByUid(uid, record.getId()).size() >= record.getTimes() ? 0 : 1);
            // 分出进行中和非进行中的数据并整合
            if (BelongCalendar.calendar(record.getStart(), record.getEnd())) {
                start.add(exaRecord);
            } else {
                end.add(exaRecord);
            }
        });
        LOGGER.info("当前进行中的考试 ： " + start);
        LOGGER.info("当前未进行中的考试 ： " + end);
        ExaRecordMap exaRecordMap = new ExaRecordMap();
        exaRecordMap.setStartList(start);
        exaRecordMap.setNoStartList(end);
        return exaRecordMap;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserExaGrades submitExamination(SubmitExamination submitExamination) {
        // 需要追加判断 如果是练习需要返回练习的题目正确错误和答案解析情况，并且能添加到错题集
        UserExaGrades userExaGrades = new UserExaGrades();
        Integer ifpractice = findById(submitExamination.getRecordId()).getIfpractice();
        Integer recordId = submitExamination.getRecordId();
        Integer uid = submitExamination.getUid();
        // 校验是否在考试时间内
        if (!scope(submitExamination.getEnd(), recordId)) {
            throw new RestException(400, "请在合法时间范围内提交考试");
        }
        // 有人可能绕过前端计时器发送超时的时间,
        // 解决方案： 开始考试当前用户获取考题时，记录其开始时间，提交时获取开始时间结合结束时间，判断是否超时
//        redisUtil.del(SUBKEY + recordId);
        // 获取用户所有答案
        List<Answer> answers = submitExamination.getAnswer();
        Map<Integer, Examination> idExaminationMap;
        // 查询缓存
        if (redisUtil.hasKey(SUBKEY + recordId)) {
            log.info("进入缓存");
            Object o = redisUtil.get(SUBKEY + recordId);
            log.info("缓存: {}", o);
            idExaminationMap = JSONObject.parseObject(JSON.toJSONString(o), new TypeReference<Map<Integer, Examination>>() {
            });
        } else {
            log.info("查询数据库");
            // 通过idList从数据库查询 转成 map  id : examination
            idExaminationMap =
                    examinationService
                            .listByIds(
                                    answers.stream()
                                            .map(Answer::getPid)
                                            .collect(Collectors.toList()))
                            .stream()
                            .collect(Collectors.toMap(Examination::getId, o -> o));
            // 放入缓存
            Object o = JSON.toJSON(idExaminationMap);
            log.info("放入缓存: {}", o);
            redisUtil.set(SUBKEY + recordId, o, RedisUtil.EXPIRETIME);
        }
        // 从缓存中获取该次统考的所有考题，缓存没有则从数据库中获取放到缓存中
        List<AnswerAnalysis> answerAnalyses = new ArrayList<>();
        String key = "examination";
        // 记录各个题型正确和错误题数
        for (Answer answer : answers) {
            // 题目id
            Integer eid = answer.getPid();
            // 通过题目id查找标准答案对比，判断是否正确
            Examination examination = idExaminationMap.get(eid);
            LOGGER.info("当前题目: " + examination);
            Boolean ifCorrect = false;
            // 题型id
            Integer typeId = examination.getTypeId();
            if (answer.getOption().equals(examination.getAnswer())) {
                // 正确key 自增1
                redisUtil.incr("correct:" + uid + ":" + typeId, 1);
                ifCorrect = true;
            } else {
                // 错误key 自增1
                redisUtil.incr("error:" + uid + ":" + typeId, 1);
            }
            if (ifpractice == 1) {
                // 如果为练习、需要返回答案解析
                AnswerAnalysis answerAnalysis = new AnswerAnalysis();
                answerAnalysis.setEid(answer.getPid())
                        .setTid(examination.getTypeId())
                        .setAnswer(examination.getAnswer())
                        .setUserAnswer(answer.getOption())
                        .setIfCorrect(ifCorrect);
                if (typeId != 3) {
                    answerAnalysis.setOptions(examinationAnswerService
                            .selectByEid(eid)
                            .stream()
                            .map(ExaminationAnswer::getAnswerOption)
                            .collect(Collectors.toList()));
                }
                answerAnalyses.add(answerAnalysis);
            }
        }
        // 遍历所有题型获取各个题型正确错误数量
        int score = 0;
        DecimalFormat df = new DecimalFormat("0.00");
        // 通过记录id获得全部题型、和题型分数
        for (EtypeScore typeScore : etypeScoreService.findAllById(recordId)) {
            // 获取题型id
            Integer tid = typeScore.getTid();
            // 获取该题型正确数量
            String correct = "correct:" + uid + ":" + tid;
            Object correctObject = redisUtil.get(correct);
            Integer correctCount = 0;
            if (!StringUtils.isEmpty(correctObject)) {
                correctCount = Integer.parseInt(correctObject.toString());
            }
            LOGGER.info("当前题型" + tid + "正确题数:" + correctCount);
            int tScore = correctCount * typeScore.getScore();
            score += tScore;
            LOGGER.info("当前题型" + tid + " 得分: " + tScore);
            // 错误题数
            String error = "error:" + uid + ":" + tid;
            Object errorObject = redisUtil.get(error);
            Integer errorCount = 0;
            if (!StringUtils.isEmpty(errorObject)) {
                errorCount = Integer.parseInt(errorObject.toString());
            }
            LOGGER.info("当前题型" + tid + "错误题数:" + errorCount);
            // 处理用户各个题型正确率
            // 计算正确率
            int total = errorCount + correctCount;
            String format = df.format((float) correctCount / total);
            double v = Double.parseDouble(format);
            LOGGER.info("该题型 " + tid + "正确率为:" + v);
            userExaminationScoreService.insertUserExaminationScore(
                    new UserExaminationScore()
                            .setCorrect(correctCount)
                            .setError(errorCount)
                            .setRid(recordId)
                            .setTid(tid)
                            .setScore(tScore)
                            .setAccuracy(v));
            // 清理该题型的缓存
            redisUtil.del(correct);
            redisUtil.del(error);
        }
        LOGGER.info("当前用户总分为 ：" + score);
        // 插入当前用户考试成绩
        UserExamination userExamination = new UserExamination();
        // 插入总分
        userExamination.setAchievement1(score)
                .setStart(submitExamination.getStart())
                .setEnd(submitExamination.getEnd())
                .setRecordId(submitExamination.getRecordId())
                .setUid(submitExamination.getUid());
        userExaminationService.save(userExamination);
        userExaGrades.setGrades(score);
        if (ifpractice == 1) {
            userExaGrades.setAnswerAnalyses(answerAnalyses);
        }
        return userExaGrades;
    }

    /**
     * 判断提交时间是否在该统考范围内
     *
     * @param end      提交时间
     * @param recordId 统考id
     * @return 是否在范围内
     */
    private boolean scope(Date end, Integer recordId) {

        // 通过统考id查找该次考试开始时间和结束时间
        ExaminationRecord record = findById(recordId);
        log.info("提交时间: {}", end);
        return BelongCalendar.inCalendar(record.getStart(), record.getEnd(), end);

    }

    public List<ExaminationRecord> selectExaInfo(UserInfo userInfo, Integer ifPractice) {
        List<ExaminationRecord> examinationRecords = new ArrayList<>();
        LOGGER.info("当前用户 : " + userInfo);
        // 查询是否为学生
        if (userInfo.getRoleId().equals(roleService.getRoleByNameZh("学生").getId())) {
            // 查询条件为公开且为练习/考试的记录
            examinationRecords.addAll(selectPublicRecord(ifPractice));

            // 查询未公开的且符合当前身份条件的考试练习
            // 根据当前用户信息查找有权限的考试练习
            examinationRecords.addAll(selectExa(userInfo));
        } else {
            // 如果不是学生直接返回所有考试/练习记录
            examinationRecords = findAllExamination(ifPractice);
            LOGGER.info("当前不是学生，返回全部考试练习 : " + examinationRecords);
        }
        examinationRecords.forEach(System.out::println);
        return examinationRecords;
    }

    @Override
    public List<ExaminationRecord> getAllExa(Integer ifPractice, UserInfo userInfo) {
        Role admin = roleService.findByName("admin");
        QueryWrapper<ExaminationRecord> wrapper = new QueryWrapper<>();
        wrapper.eq("ifpractice", ifPractice);
        if (admin.getId().equals(userInfo.getRoleId())) {
            return examinationRecordMapper.selectList(wrapper);
        } else {
            wrapper.eq("uid", userInfo.getId());
            return examinationRecordMapper.selectList(wrapper);
        }
    }

    @Override
    public Boolean delRecord(Integer rid) {
        // 如果删除的是公开的，需要删除缓存
        ExaminationRecord examinationRecord = examinationRecordMapper.selectById(rid);
        if (examinationRecord.getIfPublic() == 1) {
            redisUtil.del(KEY + ":1:" + examinationRecord.getIfpractice());
        }
        // 删除缓存
        redisUtil.del(GeneralExaminationServiceImpl.KEY + rid);
        // 需要删除该考试练习相关的数据
        examinationRecordMapper.deleteById(rid);
        // 删除配置
        etypeScoreService.delByEid(rid);
        generalExaminationService.delByRid(rid);
        return true;
    }

    @Override
    public Boolean refreshCache() {
        // 删除公开的考试练习
        redisUtil.del(KEY + ":1:" + 1);
        redisUtil.del(KEY + ":1:" + 0);
        return true;
    }

    /**
     * 查询条件为公开且为练习/考试的记录
     */
    private List<ExaminationRecord> selectPublicRecord(Integer ifPractice) {
        // 键为 key:是否公开:是否为练习, 存放公开的考试练习记录缓存
        String key = KEY + ":1:" + ifPractice;
        // 查询缓存
        if (redisUtil.hasKey(key)) {
            log.info("进入缓存区");
            List<ExaminationRecord> list = FastJsonUtils.toList(redisUtil.lGetAll(key), ExaminationRecord.class);
            log.info("缓存: {}", list);
            return list;
        }
        // 如果没有缓存则查询数据库并放入缓存 加双重检测锁
        Lock lock = new ReentrantLock();
        lock.lock();
        try {
            if (redisUtil.hasKey(key)) {
                return redisUtil.lGetAll(key);
            }
            log.info("进入数据库");
            // 从数据库查询
            List<ExaminationRecord> examinationRecords =
                    examinationRecordMapper.selectList(
                            new QueryWrapper<ExaminationRecord>()
                                    .eq("if_public", 1)
                                    .eq("ifpractice", ifPractice));
            // 加入缓存
            redisUtil.lSet(key, examinationRecords, RedisUtil.EXPIRETIME);
            return examinationRecords;
        } finally {
            lock.unlock();
        }
    }

    List<ExaminationRecord> findAllExamination(Integer ifPractice) {
        return examinationRecordMapper.selectList(
                new QueryWrapper<ExaminationRecord>()
                        .eq("ifpractice", ifPractice));
    }

}
