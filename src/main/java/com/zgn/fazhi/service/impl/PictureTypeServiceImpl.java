package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.mapper.FileTypeMapper;
import com.zgn.fazhi.pojo.po.FileType;
import com.zgn.fazhi.pojo.po.PictureType;
import com.zgn.fazhi.mapper.PictureTypeMapper;
import com.zgn.fazhi.pojo.po.VisitContent;
import com.zgn.fazhi.service.PictureTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class PictureTypeServiceImpl extends ServiceImpl<PictureTypeMapper, PictureType> implements PictureTypeService {

    private static final String KEY = "imageTypeKey";

    @Autowired
    private PictureTypeMapper pictureTypeMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public List<PictureType> getAllPictureType() {
        // 查询缓存再查询数据库
        try {
            if (hasKey()) {
                return redisUtil.lGet(KEY, 0, -1);
            }
        } catch (RedisConnectionFailureException e) {
            e.printStackTrace();
            return pictureTypeMapper.selectList(null);
        }
        return selectPutCache();
    }

    @Override
    public PictureType getAllPictureTypeById(Integer id) {
        return pictureTypeMapper.selectById(id);
    }

    @Override
    public Boolean addPictureType(PictureType pictureType) {
        pictureTypeMapper.insert(pictureType);
        redisUtil.lHasSet(KEY, pictureType);
        return true;
    }

    @Override
    public Boolean deletePictureTypeById(Integer id) {
        redisUtil.lRemove(KEY, 1, pictureTypeMapper.selectById(id));
        if (hasKey()) {
            pictureTypeMapper.deleteById(id);
        }
        return true;
    }

    @Override
    public Boolean updatePictureType(PictureType pictureType) {
        pictureTypeMapper.updateById(pictureType);
        if (hasKey()) {
            redisUtil.lRemove(KEY, 1, pictureTypeMapper.selectById(pictureType.getId()));
            redisUtil.lSet(KEY, pictureType);
        }
        return true;
    }

    @Override
    public Boolean refreshCache() {
        redisUtil.del(KEY);
        redisUtil.lSet(KEY, pictureTypeMapper.selectList(null), RedisUtil.EXPIRETIME);
        return true;
    }

    private List<PictureType> selectPutCache() {
        Lock lock = new ReentrantLock();
        // 从数据库中获取返回并写在缓存中
        lock.lock();
        try {
            if (redisUtil.hasKey(KEY)){
                return redisUtil.lGetAll(KEY);
            }
            List<PictureType> pictureTypes = pictureTypeMapper.selectList(null);
            redisUtil.lSet(KEY, pictureTypes, RedisUtil.EXPIRETIME);
            return pictureTypes;
        } finally {
            lock.unlock();
        }
    }

    private Boolean hasKey() {
        return redisUtil.hasKey(KEY);
    }

}
