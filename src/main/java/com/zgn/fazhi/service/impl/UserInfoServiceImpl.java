package com.zgn.fazhi.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.listener.UserInfoListener;
import com.zgn.fazhi.pojo.po.*;
import com.zgn.fazhi.mapper.UserInfoMapper;
import com.zgn.fazhi.pojo.vo.UserExcel;
import com.zgn.fazhi.pojo.vo.UserInfoSchool;
import com.zgn.fazhi.result.Result;
import com.zgn.fazhi.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

    @Autowired
    private RoleService roleService;

    @Autowired
    @Lazy
    private UserInfoMapper userMapper;

    @Autowired
    private SchoolService schoolService;

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private UserGradeService userGradeService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private com.zgn.fazhi.utils.StringUtils stringUtils;

    private static final String KEY = "userInfoKey:";

    @Override
    public String addUser(UserInfo user) {

        // 没有用户名邮箱、手机号唯一标识，需要手动生成用户名返回给用户
        String password = user.getPassword();
        String username = user.getUsername();
        if (StringUtils.isEmpty(username)) {
            // 如果username为空则生成六位数作为用户名
            do {
                username = stringUtils.getRandomUsername();
            } while (!StringUtils.isEmpty(findByUsername(username)));
        }

        // 生成16位数随机盐，注册时进行盐值加密
        String salt = new SecureRandomNumberGenerator().nextBytes().toString();
        // 散列次数
        int times = 1024;
        // 获取md5+散列+盐值加密后的密码
        String encodedPassword = new SimpleHash("md5", password, salt, times).toString();

        user.setSalt(salt)
                .setPassword(encodedPassword)
                .setUsername(username)
                .setCreateTime(new Date());
        userMapper.insert(user);
        // 加入缓存
        selectPutCache(user.getId());
        return username;
    }


    @Override
    public List<UserInfoSchool> selectRoleId(int id) {
        return userMapper.RoleInfo(id);
    }


    @Override
    public Boolean batchDelete(List<Integer> list) {
        userMapper.deleteBatchIds(list);
        // 删除缓存
        list.forEach(id -> {
            redisUtil.del(KEY + id);
        });
        return true;
    }


    @Override
    public Boolean updateUser(UserInfo userInfo) {
        userMapper.updateById(userInfo);
        // 刷新缓存
        selectPutCache(userInfo.getId());
        return true;
    }


    @Override
    public List<UserInfoSchool> selectAll() {
        return userMapper.getInfo();
    }

    @Override
    public UserInfoSchool selectByUsername(String username) {
        return userMapper.ByName(username);
    }

    /**
     * 批量添加用户信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addUserList(List<UserExcel> userExcelList, Integer uid) {
        log.info("开始导入用户数据");
        for (UserExcel userExcel : userExcelList) {
            UserInfo userInfo = new UserInfo();
            // 自动生成六位数作为用户名
            String username;
            do {
                username = stringUtils.getRandomUsername();
            } while (!StringUtils.isEmpty(findByUsername(username)));
            userInfo.setUsername(username);
            userExcel.setUsername(username);
            // 设置昵称
            userInfo.setName(userExcel.getName());
            // 密码
            // 生成16位数随机盐，注册时进行盐值加密
            String salt = new SecureRandomNumberGenerator().nextBytes().toString();
            // 散列次数
            int times = 1024;
            // 获取md5+散列+盐值加密后的密码
            String encodedPassword = new SimpleHash("md5", userExcel.getPassword(), salt, times).toString();
            userInfo.setPassword(encodedPassword);
            userInfo.setSalt(salt);
            // 如果为数字
            String role = userExcel.getRole();
            if (isNumber(role)) {
                log.info("当前role为数值，直接插入");
                userInfo.setRoleId(Integer.valueOf(role));
            } else {
                log.info("当前role为str，正在前往数据库查找对应角色id");
                Role roleByNameZh = roleService.getRoleByNameZh(role);
                log.info(roleByNameZh.toString());
                // 角色（需要判断，是角色id还是角色名称）
                userInfo.setRoleId(roleByNameZh.getId());
            }

            String schoolName = userExcel.getSchool();
            if (!StringUtils.isEmpty(schoolName)) {
                // 如果为数字
                if (isNumber(schoolName)) {
                    log.info("当前school为数值，直接插入");
                    userInfo.setSchoolId(Integer.valueOf(schoolName));
                } else {
                    log.info("当前school为str，正在前往数据库查找对应学校id");
                    School school1 = schoolService.findByName(schoolName);
                    // 学校 (需要判断，是学校id还是学校名称)
                    userInfo.setSchoolId(school1.getId());
                }
            }
            // 设置学校
            if (!StringUtils.isEmpty(userExcel.getSchool())) {
                School school = schoolService.findByName(userExcel.getSchool());
                userInfo.setSchoolId(school.getId());
            }

            // 设置年级
            if (!StringUtils.isEmpty(userExcel.getGrade())) {
                UserGrade userGrade = userGradeService.selectByName(userExcel.getGrade());
                userInfo.setGradeId(userGrade.getId());
            }

            // 设置班级
            if (!StringUtils.isEmpty(userExcel.getClassName())) {
                ClassInfo classInfo = classInfoService.selectByName(userExcel.getClassName());
                userInfo.setClassId(classInfo.getId());
            }
            userMapper.insert(userInfo);
            log.info("完成插入用户 : " + userInfo);
            // 需要将该用户放入redis返回前端
            redisUtil.lSet("userExcel:" + uid, userExcel);
        }
    }

    @Override
    public List<UserInfo> findAllByRoleId(Integer roleId) {
        return userMapper.selectList(new QueryWrapper<UserInfo>().eq("role_id", roleId));
    }

    @Override
    public Boolean updateUserRoleId(Integer uid, Integer roleId) {
        // 查询数据库是否有该角色id
        List<Role> allRole = roleService.getAllRole();
        Boolean isRole = false;
        for (Role role : allRole) {
            if (role.getId().equals(roleId)) {
                isRole = true;
                break;
            }
        }
        if (!isRole) {
            throw new RestException(400, "不存在该角色id");
        }
        UserInfo userInfo = new UserInfo();
        userInfo.setRoleId(roleId);
        userInfo.setId(uid);
        userMapper.updateById(userInfo);
        // 刷新缓存
        selectPutCache(uid);
        return true;
    }

    @Override
    public UserInfo findByUsername(String name) {
        QueryWrapper<UserInfo> userInfoQueryWrapper = new QueryWrapper<>();
        userInfoQueryWrapper.eq("username", name);
        return userMapper.selectOne(userInfoQueryWrapper);
    }

    @Override
    public Boolean updatePassword(Integer uid, String password) {
        // 修改密码需要生成新的盐，和重新加上hash散列签名
        UserInfo userInfo = new UserInfo();
        userInfo.setId(uid);
        // 生成16位数随机盐，注册时进行盐值加密
        String salt = new SecureRandomNumberGenerator().nextBytes().toString();
        // 散列次数
        int times = 1024;
        // 获取md5+散列+盐值加密后的密码
        String encodedPassword = new SimpleHash("md5", password, salt, times).toString();
        userInfo.setPassword(encodedPassword);
        userInfo.setSalt(salt);
        userMapper.updateById(userInfo);
        // 刷新缓存
        selectPutCache(uid);
        return true;
    }

    @Override
    public List<UserInfo> findAll() {
        return userMapper.selectList(null);
    }

    public boolean isNumber(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            System.out.println("异常：\"" + str + "\"不是数字/整数...");
            return false;
        }
    }

    @Override
    public UserInfo findByUid(Integer uid) {
        try {
            // 先查询缓存，缓存没有则查询数据库返回
            if (redisUtil.hasKey(KEY + uid)) {
                UserInfo userInfo = FastJsonUtils.toBean(redisUtil.get(KEY + uid), UserInfo.class);
                ;
                return userInfo;
            } else {
                return selectPutCache(uid);
            }
        } catch (RedisConnectionFailureException e) {
            e.printStackTrace();
            return userMapper.selectById(uid);
        }
    }

    UserInfo selectPutCache(Integer uid) {
        // 缓存没有则向数据库查询
        UserInfo userInfo = userMapper.selectById(uid);
        // 保存缓存
        redisUtil.set(KEY + uid, userInfo, RedisUtil.EXPIRETIME);
        return userInfo;
    }

    @Override
    public String retrieve(String answer, String username) {
        Integer uid = userMapper.retrieve(answer, username);
        if (StringUtils.isEmpty(uid)) {
            throw new RestException(400, "验证信息错误");
        }
        // 生成密钥保存redis 并返回
        String key = "RETRIEVE@" + ThreadLocalRandom.current().nextInt(10086);
        // 设置有效期5分钟
        redisUtil.set(key, uid, 5 * 60);
        return key;
    }

    @Override
    public Boolean updatePasswordByKey(String key, String password) {
        Object o = redisUtil.get(key);
        if (StringUtils.isEmpty(o)) {
            throw new RestException(400, "密钥无效");
        }
        int uid = Integer.parseInt(o.toString());
        userMapper.updateById(new UserInfo().setPassword(password).setId(uid));
        return true;
    }

    @Override
    public String getQuestion(String username) {
        return userMapper.getQuestion(username);
    }

    @Override
    public List<Integer> getIdListBySchoolId(Integer schoolId) {
        return userMapper.getIdListDynamic(schoolId, null, null);
    }

    @Override
    public List<Integer> getIdListByGradeId(Integer schoolId, Integer gradeId) {
        return userMapper.getIdListDynamic(schoolId, gradeId, null);
    }

    @Override
    public List<Integer> getIdListByClassId(Integer classId) {
        return userMapper.getIdListDynamic(null, null, classId);
    }

    @Override
    public Integer countUser() {
        return userMapper.selectCount(null);
    }
}
