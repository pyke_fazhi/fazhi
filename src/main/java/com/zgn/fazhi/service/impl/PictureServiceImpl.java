package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.pojo.po.Picture;
import com.zgn.fazhi.mapper.PictureMapper;
import com.zgn.fazhi.pojo.vo.FileUploadForm;
import com.zgn.fazhi.service.FileUploadService;
import com.zgn.fazhi.service.PictureService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class PictureServiceImpl extends ServiceImpl<PictureMapper, Picture> implements PictureService {

    private static final String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp|jpeg))$)";

    @Autowired
    private FileUploadService fileUploadService;

    @Autowired
    private PictureMapper pictureMapper;

    @Autowired
    private RedisUtil redisUtil;

    private static final String KEY = "pictureResKey";

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean pictureUpload(FileUploadForm imageUploadForm) {
        Integer typeId = imageUploadForm.getTypeId();
        // 上传阿里OSS,返回上传文件路径
        String url = fileUploadService.upload(imageUploadForm.getUploadFile(),
                "image", typeId, IMAGE_PATTERN, imageUploadForm.getId());
        // 上传数据库
        Picture picture = new Picture();
        picture.setInfo(imageUploadForm.getInfo())
                .setTitle(imageUploadForm.getTitle())
                .setTypeId(typeId)
                .setUrl(url);
        pictureMapper.insert(picture);
        redisUtil.del(KEY + imageUploadForm.getTypeId());
        return true;
    }

    @Override
    public Boolean delById(Integer id) {
        Picture picture = pictureMapper.selectById(id);
        String key = KEY + picture.getTypeId();
        // 通过id获取路径
        fileUploadService.delete(picture.getUrl());
        pictureMapper.deleteById(id);
        redisUtil.del(key);
        return true;
    }

    @Override
    public List<Picture> listByTid(Integer tid) {
        String key = KEY + tid;
        try {
            if (redisUtil.hasKey(key)){
                return FastJsonUtils.toList(redisUtil.get(KEY), Picture.class);
            }
        } catch (RedisConnectionFailureException e){
            e.printStackTrace();
            return pictureMapper.selectList(new QueryWrapper<Picture>().eq("type_id", tid));
        }
        List<Picture> pictures = pictureMapper.selectList(new QueryWrapper<Picture>().eq("type_id", tid));
        redisUtil.setIfAbsent(key, pictures, RedisUtil.EXPIRETIME);
        return pictures;
    }
}
