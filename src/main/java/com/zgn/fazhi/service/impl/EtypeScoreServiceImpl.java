package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.pojo.po.EtypeScore;
import com.zgn.fazhi.mapper.EtypeScoreMapper;
import com.zgn.fazhi.service.EtypeScoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class EtypeScoreServiceImpl extends ServiceImpl<EtypeScoreMapper, EtypeScore> implements EtypeScoreService {

    @Autowired
    private EtypeScoreMapper etypeScoreMapper;

    @Autowired
    private RedisUtil redisUtil;

    private static final String KEY = "etypeScoreKey";

    @Override
    public void insertEtype(EtypeScore etypeScore) {
        String key = KEY + etypeScore.getEid();
        if (redisUtil.hasKey(key)){
            redisUtil.del(key);
        }
        // 通过id插入
        etypeScoreMapper.insert(etypeScore);
    }

    @Override
    public List<EtypeScore> findAllById(Integer eid) {
        String key = KEY + eid;
        if (redisUtil.hasKey(key)){
            return FastJsonUtils.toList(redisUtil.get(key), EtypeScore.class);
        } else {
            // 查找数据库，加入缓存
            List<EtypeScore> etypeScoreList = etypeScoreMapper.selectList(
                    new QueryWrapper<EtypeScore>()
                            .eq("eid", eid));
            redisUtil.setIfAbsent(key, etypeScoreList, RedisUtil.EXPIRETIME);
            return etypeScoreList;
        }
    }

    @Override
    public EtypeScore findByEidAndTid(Integer eid, Integer tid) {
        return etypeScoreMapper.selectOne(
                new QueryWrapper<EtypeScore>()
                        .eq("eid", eid)
                        .eq("tid", tid));
    }

    @Override
    public Boolean delByEid(Integer eid) {
        etypeScoreMapper.delete(
                new QueryWrapper<EtypeScore>()
                        .eq("eid", eid));
        redisUtil.del(KEY + eid);
        return true;
    }
}
