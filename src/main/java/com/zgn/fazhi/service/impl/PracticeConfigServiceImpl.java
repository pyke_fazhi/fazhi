package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.mapper.PracticeConfigMapper;
import com.zgn.fazhi.pojo.po.PracticeConfig;
import com.zgn.fazhi.service.PracticeConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ydw
 */
@Service
public class PracticeConfigServiceImpl extends ServiceImpl<PracticeConfigMapper, PracticeConfig> implements PracticeConfigService {

    @Autowired
    private PracticeConfigMapper practiceConfigMapper;

    @Override
    public List<PracticeConfig> getAllConfig() {
        return practiceConfigMapper.selectList(null);
    }

    @Override
    public int delPraConfig(Integer id) {
        practiceConfigMapper.deleteById(id);
        return 1;
    }

    @Override
    public int addPraConfig(PracticeConfig practiceConfig) {
        practiceConfigMapper.insert(practiceConfig);
        return 1;
    }

    @Override
    public int updatePraConfig(PracticeConfig practiceConfig) {
        practiceConfigMapper.updateById(practiceConfig);
        return 1;
    }
}
