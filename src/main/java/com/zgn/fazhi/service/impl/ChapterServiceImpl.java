package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.mapper.ChapterMapper;
import com.zgn.fazhi.pojo.po.Chapter;
import com.zgn.fazhi.service.ChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChapterServiceImpl extends ServiceImpl<ChapterMapper, Chapter> implements ChapterService {

    @Autowired
    private ChapterMapper chapterMapper;

    @Override
    public List<Chapter> getAllByUid(Integer uid) {
        return chapterMapper.selectList(
                new QueryWrapper<Chapter>()
                        .eq("unit_id", uid));
    }

    @Override
    public Chapter getByTitle(String title, Integer unitId) {
        return chapterMapper.selectOne(
                new QueryWrapper<Chapter>()
                        .eq("title", title)
                        .eq("unit_id", unitId));
    }

    @Override
    public List<Chapter> selectAll() {
        return chapterMapper.selectList(null);
    }
}
