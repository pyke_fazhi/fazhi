package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.mapper.SecretGuardMapper;
import com.zgn.fazhi.pojo.po.SecretGuard;
import com.zgn.fazhi.service.SecretGuardService;
import org.springframework.stereotype.Service;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 20/08/2021 11:42 AM
 */

@Service
public class SecretGuardServiceImpl extends ServiceImpl<SecretGuardMapper, SecretGuard> implements SecretGuardService {



}
