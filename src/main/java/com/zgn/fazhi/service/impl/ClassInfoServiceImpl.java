package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.mapper.ClassInfoMapper;
import com.zgn.fazhi.pojo.po.ClassInfo;
import com.zgn.fazhi.pojo.po.School;
import com.zgn.fazhi.pojo.po.UserGrade;
import com.zgn.fazhi.service.ClassInfoService;
import com.zgn.fazhi.service.SchoolService;
import com.zgn.fazhi.service.UserGradeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ClassInfoServiceImpl extends ServiceImpl<ClassInfoMapper, ClassInfo> implements ClassInfoService {

    @Autowired
    private ClassInfoMapper classInfoMapper;

    @Autowired
    private SchoolService schoolService;

    @Autowired
    private UserGradeService userGradeService;

    @Override
    public List<ClassInfo> getAllClassInfo() {
        return classInfoMapper.selectList(null);
    }

    @Override
    public Boolean addClassInfo(ClassInfo classInfo) {
        classInfoMapper.insert(classInfo);
        return true;
    }

    @Override
    public Boolean updateClassInfoById(ClassInfo classInfo) {
        classInfoMapper.updateById(classInfo);
        return true;
    }

    @Override
    public Boolean delClassInfo(Integer id) {
        classInfoMapper.deleteById(id);
        return true;
    }

    @Override
    public List<ClassInfo> getAllClassBySnameAndGname(String sName, String gName) {
        School school = schoolService.findByName(sName);
        UserGrade userGrade = userGradeService.selectByName(gName);
        return getAllClassBySidGid(school.getId(), userGrade.getId());
    }

    @Override
    public ClassInfo selectByName(String name) {
        return classInfoMapper.selectOne(
                new QueryWrapper<ClassInfo>()
                        .eq("name", name));
    }

    @Override
    public List<ClassInfo> getAllClassBySidGid(Integer sid, Integer gid) {
        return classInfoMapper.selectList(
                new QueryWrapper<ClassInfo>()
                        .eq("school_id", sid)
                        .eq("grade_id", gid));
    }
}
