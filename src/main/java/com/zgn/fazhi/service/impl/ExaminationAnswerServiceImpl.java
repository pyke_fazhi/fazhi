package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.pojo.po.ExaminationAnswer;
import com.zgn.fazhi.mapper.ExaminationAnswerMapper;
import com.zgn.fazhi.pojo.po.PracticeAnswer;
import com.zgn.fazhi.service.ExaminationAnswerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class ExaminationAnswerServiceImpl extends ServiceImpl<ExaminationAnswerMapper, ExaminationAnswer> implements ExaminationAnswerService {


    @Autowired
    private ExaminationAnswerMapper examinationAnswerMapper;

    @Autowired
    private RedisUtil redisUtil;

    private static final String KEY = "examinationAnswerKey";

    @Override
    public Boolean addAnswerListAdmin(Map<Integer, String> map, int id) {
        log.info("开始插入答案 id = " + id);
        for (int i = 6; i < map.size(); i++) {
            insertAnswer(id, map, i);
        }
        return true;
    }

    @Override
    public Boolean addAnswerListTeacher(Map<Integer, String> map, int id) {
        log.info("开始插入答案 id = " + id);
        for (int i = 4; i < map.size(); i++) {
            insertAnswer(id, map, i);
        }
        return true;
    }

    @Override
    public Boolean delAnswerListByEid(Integer eid) {
        redisUtil.del(KEY + eid);
        examinationAnswerMapper.delete(new QueryWrapper<ExaminationAnswer>().eq("eid", eid));
        return true;
    }

    @Override
    public Boolean delAnswerById() {
        return null;
    }

    /**
     * 插入选项
     *
     * @param id  考题id
     * @param map 考题数据
     * @param i   当前下标
     */
    private void insertAnswer(Integer id, Map<Integer, String> map, int i) {
        ExaminationAnswer examinationAnswer = new ExaminationAnswer();
        examinationAnswer.setEid(id);
        examinationAnswer.setAnswerOption(map.get(i));
        examinationAnswerMapper.insert(examinationAnswer);
        log.info("完成插入答案  : " + examinationAnswer);
    }

    @Override
    public List<ExaminationAnswer> selectByEid(Integer eid) {
        String key = KEY + eid;
        // 查询缓存
        if (redisUtil.hasKey(key)) {
            return FastJsonUtils.toList(redisUtil.lGetAll(key), ExaminationAnswer.class);
        }
        List<ExaminationAnswer> examinationAnswers =
                examinationAnswerMapper
                        .selectList(
                                new QueryWrapper<ExaminationAnswer>()
                                        .eq("eid", eid));

        // 加入缓存
        redisUtil.setIfAbsent(key, examinationAnswers, RedisUtil.EXPIRETIME);
        return examinationAnswers;
    }



}
