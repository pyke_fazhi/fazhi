package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.pojo.po.UserFile;
import com.zgn.fazhi.mapper.UserFileMapper;
import com.zgn.fazhi.pojo.vo.UserFileData;
import com.zgn.fazhi.service.UserFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
public class UserFileServiceImpl extends ServiceImpl<UserFileMapper, UserFile> implements UserFileService {

    @Autowired
    private UserFileMapper userFileMapper;

    @Autowired
    private UserInfoService userInfoService;

    @Override
    public UserFile findByUid(Integer uid) {
        UserFile userFile = userFileMapper.selectOne(
                new QueryWrapper<UserFile>()
                        .select("length")
                        .eq("uid", uid));
        if (StringUtils.isEmpty(userFile)) {
            // 如果没有该用户数据则创建插入, 返回带有id的数据
            UserFile userFile1 = new UserFile();
            userFile1.setLength(0)
                    .setUid(uid);
            userFileMapper.insert(userFile1);
            return userFile1;
        } else {
            return userFile;
        }
    }

    @Override
    public List<UserFileData> getFileTimeData() {
        UserFileData userFileData = new UserFileData();
        List<UserFileData> userFileDataList = new ArrayList<>();
        userFileMapper.selectList(null).forEach(userFile -> {
            Integer uid = userFile.getUid();
            userFileData
                    .setUid(uid)
                    .setTime(userFile.getLength())
                    .setName(userInfoService.getById(uid).getName());
            userFileDataList.add(userFileData);
        });
        return userFileDataList;
    }
}
