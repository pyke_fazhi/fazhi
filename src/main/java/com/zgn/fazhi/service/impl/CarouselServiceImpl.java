package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.Carousel;
import com.zgn.fazhi.mapper.CarouselMapper;
import com.zgn.fazhi.service.CarouselService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-26
 */
@Service
public class CarouselServiceImpl extends ServiceImpl<CarouselMapper, Carousel> implements CarouselService {

}
