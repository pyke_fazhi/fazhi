package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.RankInfo;
import com.zgn.fazhi.mapper.RankInfoMapper;
import com.zgn.fazhi.service.RankInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class RankInfoServiceImpl extends ServiceImpl<RankInfoMapper, RankInfo> implements RankInfoService {

    @Autowired
    private RankInfoMapper rankInfoMapper;

    @Autowired
    private RedisUtil redisUtil;

    private static final String KEY = "rankInfoKey";

    @Override
    public List<RankInfo> getRank() {
        try {
            if (redisUtil.hasKey(KEY)){
                return FastJsonUtils.toList(redisUtil.get(KEY), RankInfo.class);
            }
        } catch (RedisConnectionFailureException e){
            return rankInfoMapper.selectList(null);
        }
        List<RankInfo> rankInfos = rankInfoMapper.selectList(null);
        redisUtil.setIfAbsent(KEY, rankInfos, RedisUtil.EXPIRETIME);
        return rankInfos;
    }

    @Override
    public Boolean updateRank(RankInfo rankInfo) {
        rankInfoMapper.updateById(rankInfo);
        redisUtil.del(KEY);
        return true;
    }

    @Override
    public Boolean deleteRank(Integer id) {
        rankInfoMapper.deleteById(id);
        redisUtil.del(KEY);
        return true;
    }

    @Override
    public Boolean addRank(RankInfo rankInfo) {
        rankInfoMapper.insert(rankInfo);
        redisUtil.del(KEY);
        return true;
    }
}
