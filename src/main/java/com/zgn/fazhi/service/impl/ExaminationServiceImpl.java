package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.factory.ResultFactory;
import com.zgn.fazhi.pojo.po.*;
import com.zgn.fazhi.mapper.ExaminationMapper;
import com.zgn.fazhi.pojo.vo.ExaminationOption;
import com.zgn.fazhi.pojo.vo.ExaminationUnit;
import com.zgn.fazhi.pojo.vo.Grades;
import com.zgn.fazhi.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.utils.BelongCalendar;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import com.zgn.fazhi.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.*;
import java.util.stream.Collectors;


/**
 * 考题
 *
 * @author ydw
 */
@Service
@Slf4j
public class ExaminationServiceImpl extends ServiceImpl<ExaminationMapper, Examination> implements ExaminationService {

    @Autowired
    private ExaminationMapper examinationMapper;

    @Lazy
    @Autowired
    private ExaminationRecordService examinationRecordService;

    @Autowired
    private UserExaminationService userExaminationService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private ExaminationAnswerService examinationAnswerService;

    private String key = "examination";

    //根据类型查找题目
    @Override
    public List<Examination> selectByType(int id) {
        QueryWrapper<Examination> examinationQueryWrapper = new QueryWrapper<>();
        examinationQueryWrapper.eq("type_id", id);
        return examinationMapper.selectList(examinationQueryWrapper);
    }

    //查找所有题目
    @Override
    public List<Examination> selectAllPaper() {
        return examinationMapper.selectList(null);
    }

    //根据Id查找题目
    @Override
    public Examination selectPaperById(int id) {
        QueryWrapper<Examination> examinationQueryWrapper = new QueryWrapper<>();
        examinationQueryWrapper.eq("id", id);
        return examinationMapper.selectOne(examinationQueryWrapper);
    }

    //增加题目
    @Override
    public Result addPaper(Examination examination) {
        examinationMapper.insert(examination);
        // 添加到缓存中
        redisUtil.hSet(key, examination.getId().toString(), examination);
        return ResultFactory.buildSuccessResult("添加成功");
    }

    //根据Id修改试卷
    @Override
    public Result updatePaperById(Examination examination) {
        examinationMapper.updateById(examination);
        // 更新缓存
        redisUtil.hSet(key, examination.getId().toString(), examination);
        return ResultFactory.buildSuccessResult("修改试卷成功");
    }


    @Override
    public HashMap<String, List<Grades>> findExaminationByUid(Integer uid) {
        // 临时存储数据,后面将对其分类
        ArrayList<Grades> grades2 = new ArrayList<>();
        // 查询所有统考记录
        List<ExaminationRecord> allExamination = examinationRecordService.findAllExamination();
        // 没有考试则返回null
        if (allExamination.isEmpty()){
            return null;
        }
        // 查询该用户是否对这些统考有考试记录
        for (ExaminationRecord examinationRecord : allExamination) {
            Date start = examinationRecord.getStart();
            Date end1 = examinationRecord.getEnd();
            Grades grades1 = new Grades();
            // 设置考试的基本信息
            grades1.setUid(uid)
                    .setEid(examinationRecord.getId())
                    .setTitle(examinationRecord.getTitle())
                    .setStart(start)
                    .setEnd(end1)
                    .setLength(examinationRecord.getLengthTime());
            List<UserExamination> grades = userExaminationService.findAllByUid(uid, examinationRecord.getId());
            // 如果没有成绩或考试次数小于标准次数说明未参加考试,标记未可考试
            if (grades.isEmpty() || grades.size() < examinationRecord.getTimes()) {
                grades1.setIfExamination(1);
            } else {
                // 考试次数用完，显示当前最高成绩
                grades1.setIfExamination(0);
                if (grades.size() == 1){
                    UserExamination userExamination = grades.get(0);
                    grades1.setScore(userExamination.getAchievement1())
                            // 设置考试所用时长
                            .setUseLength((int) (userExamination.getStart().getTime() - userExamination.getEnd().getTime()));
                } else {
                    // 所有成绩中取最高分数, 后续完善
                    grades1.setScore(-1);
                }
            }
            grades2.add(grades1);
        }
        // 对所有考试进行分类
        return classification(grades2);
    }

    @Override
    public Boolean delExaminationById(Integer id) {
        examinationMapper.deleteById(id);
        // 删除缓存
        redisUtil.hdel(key, id.toString());
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addExaminationListAdmin(List<Map<Integer, String>> list) {
        // 遍历十条考题数据
        for (Map<Integer, String> map : list) {
            Integer unitId = unitService.findByTitle(map.get(2)).getId();
            Examination examination = new Examination();
            examination.setTitle(map.get(0))
                    .setTypeId(Integer.parseInt(map.get(1)))
                    // 通过单元/章节 title 查询章节id
                    .setUnitId(unitId)
                    .setChapterId(chapterService.getByTitle(map.get(3), unitId).getId())
                    .setAnswer(map.get(4))
                    .setAnalysis(map.get(5))
                    .setCreateTime(new Date());
            examinationMapper.insert(examination);
            Integer id = examination.getId();
            // 插入考题的选项
            if (map.size() > 6){
                examinationAnswerService.addAnswerListAdmin(map, id);
            }
            // 加入缓存中
            redisUtil.hSet(key, id.toString(), examination);
        }
        // 刷新选项缓存
        flushOption();
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addExaminationListTeacher(List<Map<Integer, String>> list, Integer tid) {

        for (Map<Integer, String> map : list) {
            Examination examination = new Examination();
            examination.setTitle(map.get(0))
                    .setTypeId(Integer.parseInt(map.get(1)))
                    .setAnswer(map.get(2))
                    .setAnalysis(map.get(3))
                    .setTeacherId(tid)
                    .setCreateTime(new Date());
            examinationMapper.insert(examination);
            Integer id = examination.getId();
            // 插入考题选项
            if (map.size() > 4){
                examinationAnswerService.addAnswerListTeacher(map, id);
            }
            // 加入缓存中
            redisUtil.hSet(key, id.toString(), examination);
        }
        // 刷新选项缓存
        flushOption();
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addExaminationTeacher(Integer tid, ExaminationOption examination) {
        Examination examination1 = examination.getExamination();
        // 判断是否为章节单元是否为空，为空则为上传教师私有题库，否则上传公有题库
        if (StringUtils.isEmpty(examination1.getUnitId())){
            examination1.setTeacherId(tid);
        }
        // 上传题目
        examination1.setCreateTime(new Date());
        examinationMapper.insert(examination1);
        Integer id = examination1.getId();
        // 判断题型, 如果为判断则不用上传选项
        if (examination1.getTypeId() != 3){
            LinkedList<ExaminationAnswer> examinationAnswers = new LinkedList<>();
            examination.getOptions().forEach(option -> examinationAnswers.add(new ExaminationAnswer().setAnswerOption(option).setEid(id)));
            examinationAnswerService.saveBatch(examinationAnswers);
        }
        return true;
    }

    @Override
    public List<Examination> selectAllByTid(Integer tid) {
        QueryWrapper<Examination> examinationQueryWrapper = new QueryWrapper<>();
        examinationQueryWrapper.eq("teacher_id", tid);
        return examinationMapper.selectList(examinationQueryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delTeacherExaById(Integer eid, Integer tid) {
        // 判断该id考题是否为该教师所有
        Examination examination = examinationMapper.selectById(eid);
        if (tid.equals(examination.getTeacherId())){
            examinationMapper.deleteById(eid);
            // 还需要删除对应的考题选项
            examinationAnswerService.delAnswerListByEid(eid);
            return true;
        } else {
            throw new RestException(400, "无法删除非该教师所有的考题");
        }
    }

    @Override
    public List<Integer> getRandomList(ExaminationtTypeOption examinationtTypeOption) {
        // 获取题型id、单元、章节、题数向数据库中随机抽取题目
        return examinationMapper.getRandomList(examinationtTypeOption.getType(),
                examinationtTypeOption.getUnitId(),
                examinationtTypeOption.getChapter(),
                examinationtTypeOption.getNumber());
    }

    @Override
    public List<Integer> getRandomListCidNull(Integer typeId, Integer unitId, Integer number) {
        return examinationMapper.getRandomList2(typeId, unitId, number);
    }

    @Override
    public List<Integer> getRandomListTidNull(Integer typeId, Integer unitId, Integer chapterId, Integer number) {
        return examinationMapper.getRandomList(typeId, unitId, chapterId, number);
    }

    @Override
    public List<Integer> getRandomListTidNull(Integer typeId, Integer unitId, Integer number) {
        return examinationMapper.getRandomListTidNull(typeId, unitId, number);
    }

    @Override
    public List<Integer> getRandomListByUid(Integer uid, Integer typeId, Integer number) {
        log.info("查询数据库typeId: {}", typeId);
        return examinationMapper.getRandomListByUid(uid, typeId, number);
    }

    /**
     * 分类
     */
    public HashMap<String, List<Grades>> classification(ArrayList<Grades> grades){
        // 创建装数据的map,分为进行中和结束后
        HashMap<String, List<Grades>> result = new HashMap<>();
        // 进行中的考试
        ArrayList<Grades> processing = new ArrayList<>();
        // 结束后的考试
        ArrayList<Grades> end = new ArrayList<>();
        for (Grades grade : grades) {
            // 判断是否在开始和结束时间范围内
            if (BelongCalendar.calendar(grade.getStart(), grade.getEnd())) {
                // 如果在时间范围内，加入到processing中
                processing.add(grade);
            } else {
                // 不在时间范围内加入 end中
                end.add(grade);
            }
        }
        result.put("processing", processing);
        result.put("end", end);
        return result;
    }

    @Override
    public Boolean delExaCache(List<Integer> idList) {
        idList.forEach(id -> {
            redisUtil.hdel(key, id.toString());
            // 删除该题的选项
            redisUtil.del("exaOption:" + id);
        });
        return true;
    }

    public void flushOption(){
        // 刷新题目选项缓存
        // 获取所有选项，放入redis缓存中
        List<ExaminationAnswer> optionList = examinationAnswerService.list();
        Map<Integer, List<ExaminationAnswer>> options = optionList.stream().collect(Collectors.groupingBy(ExaminationAnswer::getEid));
        String ket1 = "exaOption:";
        options.keySet().forEach(eid -> {
            redisUtil.del(ket1 + eid);
            redisUtil.lSet(ket1 + eid, options.get(eid));
        });
    }


    @Override
    public List<ExaminationUnit> getListByUnit(Integer unitId) {
        return examinationMapper.getListByUnit(unitId);
    }

    @Override
    public List<ExaminationUnit> getListByChapter(Integer chapterId) {
        return examinationMapper.getListByChapter(chapterId);
    }
}
