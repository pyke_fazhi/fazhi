package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.mapper.PermissionMapper;
import com.zgn.fazhi.pojo.po.Permission;
import com.zgn.fazhi.pojo.vo.PermissionList;
import com.zgn.fazhi.service.EducationService;
import com.zgn.fazhi.service.PermissionService;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private EducationService educationService;

    private static final String KEY = "permissionResKey";

    @Autowired
    private RedisUtil redisUtil;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addPermission(PermissionList permissionList) {
        Integer resourcesTid = permissionList.getResourcesTid();
        Integer typeId = permissionList.getTypeId();
        // 判断是否公开
        Integer ifPublic = permissionList.getIfPublic();
        Permission permission = new Permission();
        permission.setIfPublic(1)
                .setResourcesTid(resourcesTid)
                .setResid(permissionList.getResid())
                .setTypeId(typeId);
        if (ifPublic == 1) {
            // 插入公开记录
            permissionMapper.insert(permission);
        } else if (ifPublic == 0) {
            // 不公开，获取角色列表插入
            permissionList.getEducationIdList().forEach(eid -> {
                permission.setEducationId(eid);
                permissionMapper.insert(permission);
            });
        } else {
            throw new RestException(400, "公开参数错误!");
        }
        // 刷新缓存
        delCache(resourcesTid, typeId);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updatePermission(PermissionList permissionList) {
        Integer resourcesTid = permissionList.getResourcesTid();
        Integer resid = permissionList.getResid();
        // 清空原有数据，插入该次数据
        delPermission(resourcesTid, resid);
        // 刷新缓存
        delCache(resourcesTid, resid);
        return addPermission(permissionList);
    }

    @Override
    public Boolean delPermission(Integer resTid, Integer resId) {
        // 通过资源id获取该资源的分类id
        delCache(resTid, permissionMapper.selectOne(getWrapper(resTid, resId)).getTypeId());
        permissionMapper.delete(getWrapper(resTid, resId));
        return true;
    }

    @Override
    public List<String> getByTidAndRid(Integer resTid, Integer resId) {
        List<Permission> permissions = permissionMapper.selectList(getWrapper(resTid, resId));
        if (StringUtils.isEmpty(permissions)) {
            return null;
        } else {
            List<String> eNameList = new ArrayList<>();
            permissions.forEach(permission -> {
                // 通过eid查询该学历名
                eNameList.add(educationService.getById(permission.getEducationId()).getName());
            });
            return eNameList;
        }
    }

    private QueryWrapper<Permission> getWrapper(Integer resTid, Integer resId) {
        return new QueryWrapper<Permission>()
                .eq("resources_tid", resTid)
                .eq("resid", resId);
    }

    @Override
    public List<Integer> selectByTidAndEid(Integer tid, Integer eid, Integer typeId) {
        LinkedList<Integer> idList = new LinkedList<>();
        // 查询公共资源
        idList.addAll(
                selectPublicByCache(tid, typeId, 1)
                        .stream()
                        .map(Permission::getResid)
                        .collect(Collectors.toList()));
        // 筛选非公开中符合条件的
        // 如果eid为空表示查询所有私有资源
        if (StringUtils.isEmpty(eid)) {
            idList.addAll(
                    selectPublicByCache(tid, typeId, 0)
                            .stream()
                            .map(Permission::getResid)
                            .distinct()
                            .collect(Collectors.toList()));
        } else {
            idList.addAll(
                    selectPublicByCache(tid, typeId, 0)
                            .stream()
                            .filter(o -> o.getEducationId().equals(eid))
                            .map(Permission::getResid)
                            .collect(Collectors.toList()));
        }
        return idList;
    }

    /**
     * 根据条件获取资源id
     *
     * @param tid      资源分类id ， 例如video、file
     * @param typeId   详细分类id
     * @param ifPublic 是否公开
     * @return 资源id集合
     */
    private List<Permission> selectPublicByCache(Integer tid, Integer typeId, Integer ifPublic) {
        String key = KEY + ":" + tid + ":" + typeId + ":" + ifPublic;
        try {
            if (redisUtil.hasKey(key)) {
                return FastJsonUtils.toList(redisUtil.get(key), Permission.class);
            }
        } catch (RedisConnectionFailureException e) {
            return selectPublic(tid, typeId, ifPublic);
        }
        List<Permission> list = selectPublic(tid, typeId, ifPublic);
        redisUtil.setIfAbsent(key, list, RedisUtil.EXPIRETIME);
        return list;
    }

    private List<Permission> selectPublic(Integer tid, Integer typeId, Integer ifPublic) {
        return permissionMapper.selectList(
                new QueryWrapper<Permission>()
                        .eq("resources_tid", tid)
                        .eq("type_id", typeId)
                        .eq("if_public", ifPublic));
    }

    private Boolean delCache(Integer tid, Integer typeId) {
        String keyPrefix = KEY + ":" + tid + ":" + typeId + ":";
        // 清理公共资源
        redisUtil.del(keyPrefix + 1);
        // 清理非公共资源
        redisUtil.del(keyPrefix + 0);
        return true;
    }

}
