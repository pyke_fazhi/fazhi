package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.mapper.FileTypeMapper;
import com.zgn.fazhi.mapper.VisitContentMapper;
import com.zgn.fazhi.pojo.po.FileType;
import com.zgn.fazhi.pojo.po.PictureType;
import com.zgn.fazhi.pojo.po.VisitContent;
import com.zgn.fazhi.service.FileTypeService;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author ydw
 */
@Service
@Slf4j
public class FileTypeServiceImpl extends ServiceImpl<FileTypeMapper, FileType> implements FileTypeService {

    @Autowired
    private FileTypeMapper fileTypeMapper;

    @Autowired
    private RedisUtil redisUtil;

    private static final String KEY = "fileTypeKey";

    @Override
    public List<FileType> getAllFileType() {
        // 查询缓存
        try {
            if (redisUtil.hasKey(KEY)) {
                return redisUtil.lGet(KEY, 0, -1);
            }
        } catch (RedisConnectionFailureException e) {
            log.error("redis连接错误");
            return fileTypeMapper.selectList(null);
        }
        return selectPutCache();
    }

    @Override
    public FileType getAllFileTypeById(Integer id) {
        return fileTypeMapper.selectById(id);
    }

    @Override
    public Boolean addFileType(FileType fileType) {
        // 向缓存中添加
        fileTypeMapper.insert(fileType);
        redisUtil.lHasSet(KEY, fileType);
        return true;
    }

    @Override
    public Boolean deleteFileTypeById(Integer id) {
        if (hasKey()) {
            redisUtil.lRemove(KEY, 1, fileTypeMapper.selectById(id));
        }
        fileTypeMapper.deleteById(id);
        return true;
    }

    @Override
    public Boolean updateFileType(FileType fileType) {
        fileTypeMapper.updateById(fileType);
        if (hasKey()) {
            redisUtil.lRemove(KEY, 1, fileTypeMapper.selectById(fileType.getId()));
            redisUtil.lSet(KEY, fileType);
        }
        return true;
    }

    private List<FileType> selectPutCache() {
        Lock lock = new ReentrantLock();
        // 从数据库中获取返回并写在缓存中
        lock.lock();
        try {
            if (hasKey()){
                return redisUtil.lGetAll(KEY);
            }
            List<FileType> fileTypes = fileTypeMapper.selectList(null);
            redisUtil.lSet(KEY, fileTypes, RedisUtil.EXPIRETIME);
            return fileTypes;
        } finally {
            lock.unlock();
        }
    }

    Boolean hasKey() {
        return redisUtil.hasKey(KEY);
    }

}
