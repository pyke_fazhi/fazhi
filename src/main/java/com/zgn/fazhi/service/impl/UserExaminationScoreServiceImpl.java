package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.UserExaminationScore;
import com.zgn.fazhi.mapper.UserExaminationScoreMapper;
import com.zgn.fazhi.pojo.vo.UserExaGrade;
import com.zgn.fazhi.service.UserExaminationScoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
public class UserExaminationScoreServiceImpl extends ServiceImpl<UserExaminationScoreMapper, UserExaminationScore> implements UserExaminationScoreService {

    @Autowired
    private UserExaminationScoreMapper userExaminationScoreMapper;

    @Override
    public Boolean insertUserExaminationScore(UserExaminationScore userExaminationScore) {
        userExaminationScoreMapper.insert(userExaminationScore);
        return true;
    }

    @Override
    public List<UserExaGrade> getCurrentExaDateByUid(Integer uid, Integer rid) {
        return userExaminationScoreMapper.getCurrentExaDateByUid(uid, rid);
    }

    @Override
    public List<UserExaGrade> getCurrentExaDateListByUid(Integer uid) {
        return userExaminationScoreMapper.getCurrentExaDateListByUid(uid);
    }
}
