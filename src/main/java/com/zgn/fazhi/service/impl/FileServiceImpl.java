package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.pojo.po.File;
import com.zgn.fazhi.mapper.FileMapper;
import com.zgn.fazhi.pojo.vo.FileUploadForm;
import com.zgn.fazhi.service.FileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.service.FileUploadService;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class FileServiceImpl extends ServiceImpl<FileMapper, File> implements FileService {

    private static final String FILE_PATTERN = "([^\\s]+(\\.(?i)(pdf|word|excel|ppt|doc|docx|xlsx|pptx))$)";

    @Autowired
    private FileMapper fileMapper;

    @Autowired
    private FileUploadService fileUploadService;

    @Autowired
    private RedisUtil redisUtil;

    private static final String KEY = "fileResKey";

    @Override
    @Transactional(rollbackFor = Exception.class)//发生异常时进行回滚
    public Boolean fileUpload(FileUploadForm fileUploadForm) {
        Integer typeId = fileUploadForm.getTypeId();
        // 上传阿里OSS,返回上传文件路径
        String url = fileUploadService.upload(fileUploadForm.getUploadFile(),
                "document", typeId, FILE_PATTERN, fileUploadForm.getId());
        // 上传数据库
        File file = new File();
        file.setInfo(fileUploadForm.getInfo());
        file.setName(fileUploadForm.getTitle());
        file.setTypeId(fileUploadForm.getTypeId());
        file.setUrl(url);
        fileMapper.insert(file);
        redisUtil.del(KEY + fileUploadForm.getTypeId());
        return true;
    }

    @Override
    public Boolean delById(Integer id) {
        File file = fileMapper.selectById(id);
        fileUploadService.delete(file.getUrl());
        fileMapper.deleteById(id);
        redisUtil.del(KEY + file.getTypeId());
        return true;
    }

    @Override
    public List<File> listByTid(Integer tid) {
        String key = KEY + tid;
        try {
            if (redisUtil.hasKey(key)){
                return FastJsonUtils.toList(redisUtil.get(key), File.class);
            }
        } catch (RedisConnectionFailureException e){
            e.printStackTrace();
            return fileMapper.selectList(new QueryWrapper<File>().eq("type_id",tid));
        }
        List<File> files = fileMapper.selectList(new QueryWrapper<File>().eq("type_id", tid));
        redisUtil.setIfAbsent(key, files, RedisUtil.EXPIRETIME);
        return files;
    }
}
