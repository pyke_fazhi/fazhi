package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.pojo.po.Role;
import com.zgn.fazhi.mapper.RoleMapper;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.service.UserInfoService;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
@Slf4j
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    @Lazy
    private UserInfoService userInfoService;

    @Autowired
    private RedisUtil redisUtil;

    private static final String KEY = "userRoleKey";

    /**
     * 查找所有角色
     *
     * @return 返回角色集合
     */
    @Override
    public List<Role> getAllRole() {
        return roleMapper.selectList(null);
    }

    @Override
    public Role getRoleByNameZh(String name) {
        return roleMapper.selectOne(new QueryWrapper<Role>().eq("name_zh", name));
    }

    @Override
    public Boolean addRole(Role role) {
        // 检查是否重复
        if (!StringUtils.isEmpty(
                roleMapper.selectOne(
                        new QueryWrapper<Role>()
                                .eq("name", role.getName())
                                .or()
                                .eq("name_zh", role.getNameZh())
                )
        )) {
            throw new RestException(400, "角色名或角色中文名已存在");
        }
        // 没有重复则插入
        roleMapper.insert(role);
        try {
            redisUtil.hSet(KEY, role.getId().toString(), role);
        } catch (RedisConnectionFailureException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delRole(List<UserInfo> userInfoList, Integer roleId) {
        // 开启事务
        // 将用户角色id改为0
        for (UserInfo userInfo : userInfoList) {
            userInfoService.updateUserRoleId(userInfo.getId(), 0);
        }
        // 删除角色
        roleMapper.deleteById(roleId);
        try {
            redisUtil.hdel(KEY, roleId.toString());
        } catch (RedisConnectionFailureException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public Role findByName(String name) {
        return roleMapper.selectOne(new QueryWrapper<Role>().eq("name", name));
    }

    @Override
    public Boolean updateRoleById(Role role) {
        if (StringUtils.isEmpty(role.getId())) {
            throw new RestException(400, "id为空，修改失败");
        }
        roleMapper.updateById(role);
        try {
            redisUtil.hSet(KEY, role.getId().toString(), role);
        } catch (RedisConnectionFailureException e) {
            e.printStackTrace();
        }
        return true;
    }


    @Override
    public String findRnameByUid(Integer uid) {
        // 通过用户id查询角色id,再通过角色id查询角色
        return selectPutCache(
                userInfoService
                        .findByUid(uid)
                        .getRoleId())
                .getName();
    }

    Role selectPutCache(Integer rid) {
        String stringRid = rid.toString();
        try {
            if (redisUtil.hHasKey(KEY, stringRid)) {
                return FastJsonUtils.toBean(redisUtil.hget(KEY, stringRid), Role.class);
            } else {
                // 加入缓存
                Role role = roleMapper.selectById(rid);
                redisUtil.hSet(KEY, stringRid, role);
                return role;
            }
        } catch (RedisConnectionFailureException e) {
            log.error("redis连接错误");
            return roleMapper.selectById(rid);
        }
    }

}
