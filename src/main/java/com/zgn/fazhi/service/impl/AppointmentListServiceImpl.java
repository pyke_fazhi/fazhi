package com.zgn.fazhi.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.listener.AppointmentListListener;
import com.zgn.fazhi.listener.TeacherDataListener;
import com.zgn.fazhi.mapper.AppointmentListMapper;
import com.zgn.fazhi.pojo.po.AppointmentList;
import com.zgn.fazhi.service.AppointmentListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 21/08/2021 11:36 AM
 */

@Service
public class AppointmentListServiceImpl extends ServiceImpl<AppointmentListMapper, AppointmentList> implements AppointmentListService {

    @Autowired
    private AppointmentListMapper appointmentListMapper;

    @Override
    public Boolean uploadExcel(Integer aid, MultipartFile file) {
        try {
            EasyExcel.read(file.getInputStream(), new AppointmentListListener(this, aid))
                    .headRowNumber(1).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public Boolean addList(List<Map<Integer, String>> list, Integer aid) {
        list.forEach(map ->
                appointmentListMapper
                        .insert(new AppointmentList()
                                .setAid(aid)
                                .setMember(map.get(0)))
        );
        return true;
    }
}
