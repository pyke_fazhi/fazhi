package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.TestAccuracy;
import com.zgn.fazhi.mapper.TestAccuracyMapper;
import com.zgn.fazhi.service.TestAccuracyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
public class TestAccuracyServiceImpl extends ServiceImpl<TestAccuracyMapper, TestAccuracy> implements TestAccuracyService {

}
