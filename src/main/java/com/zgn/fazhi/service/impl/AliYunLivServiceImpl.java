package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.config.AliYunLiveConfig;
import com.zgn.fazhi.config.AliyunConfig;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.mapper.LiveInfoMapper;
import com.zgn.fazhi.pojo.po.LiveInfo;
import com.zgn.fazhi.pojo.po.LiveRecord;
import com.zgn.fazhi.pojo.po.Permission;
import com.zgn.fazhi.pojo.po.Video;
import com.zgn.fazhi.pojo.vo.ApiLiveModel;
import com.zgn.fazhi.pojo.vo.ApiSaveLiveModel;
import com.zgn.fazhi.pojo.vo.PermissionList;
import com.zgn.fazhi.pojo.vo.UserLiveInfo;
import com.zgn.fazhi.service.AliYunLiveService;
import com.zgn.fazhi.service.LiveRecordService;
import com.zgn.fazhi.service.PermissionService;
import com.zgn.fazhi.service.VideoService;
import com.zgn.fazhi.utils.AliYunLiveUtil;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AliYunLivServiceImpl implements AliYunLiveService {

    /**
     * 推流事件
     */
    private static final String PUSH = "publish";

    /**
     * 点赞key
     */
    private static final String LIKE_KEY = "LIKE_KEY:";

    /**
     * 超时时间 十分钟
     */
    private static final Integer TIMEOUT = 60 * 10 * 1000;

    /**
     * 断流事件
     */
    private static final String CUTOFF = "publish_done";

    /**
     * 拉流key刷新时间 29分钟
     */
    private static final long EXPIRED_TIME = 60 * 29;

    @Autowired
    private LiveInfoMapper liveInfoMapper;

    @Autowired
    private AliYunLiveConfig aliyunLiveConfig;

    @Autowired
    private RedisUtil redisUtil;

    private static final String KEY = "onLiveKey";

    @Autowired
    private AliyunConfig aliyunConfig;

    @Autowired
    private VideoService videoService;

    @Autowired
    private LiveRecordService liveRecordService;

    @Autowired
    private PermissionService permissionService;

    @Override
    public void aliyunLiveCallback(ApiLiveModel model) {
        try {
            // 根据回调接口 publish_done：关闭直播 publish 开启直播
            if (model != null) {
                //获取直播状态值
                String action = model.getAction().get(0);
                //获取直播房间号
                Integer houseId = Integer.parseInt(model.getId().get(0));

                if (PUSH.equals(action)) {
                    // 开启直播业务处理
                    startLiveHandel(houseId);
                } else if (CUTOFF.equals(action)) {
                    // 结束直播业务处理
                    endLiveHandel(houseId);
                }
                // 刷新缓存
                redisUtil.del(KEY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveLive(String sourceId) {
        try {
            //生成推流地址
            String pushUrl = AliYunLiveUtil.createPushUrl(sourceId, aliyunLiveConfig);
            log.info("生成推流地址: {}", pushUrl);

            //生成播流地址
            String pullUrl = AliYunLiveUtil.createPullUrl(sourceId, aliyunLiveConfig, 3);
            log.info("生成播流地址: {}", pullUrl);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public LiveInfo getLiveInfo(Integer uid) {
        LiveInfo liveInfo = liveInfoMapper.selectOne(new QueryWrapper<LiveInfo>().eq("uid", uid));
        // 查询数据库判断是否第一次直播, 如果不是则返回上次直播间信息, 是则返回null
        if (StringUtils.isEmpty(liveInfo)) {
            // 如果为空，则为该用户新建直播间，并返回
            liveInfo = new LiveInfo().setUid(uid);
            liveInfoMapper.insert(liveInfo);
            // 生成房间号后添加直播截图地址
            liveInfo.setScreenshot("liveScreenshot/fazhi/" + liveInfo.getId() + ".jpg");
            liveInfoMapper.updateById(liveInfo);
        }
        return liveInfo;
    }

    @Override
    public String startLive(Integer uid) {
        // 通过uid获取直播间id
        LiveInfo liveInfo = liveInfoMapper.selectOne(new QueryWrapper<LiveInfo>().eq("uid", uid));
        if (StringUtils.isEmpty(liveInfo)) {
            throw new RestException(400, "初始化直播间失败");
        }
        // 如果正在直播则抛出异常
        if (liveInfo.getIfLive() == 1) {
            throw new RestException(400, "当前直播间正在直播，无法获取推流地址");
        }
        // 根据房间id生成推流地址
        return AliYunLiveUtil.createPushUrl(liveInfo.getId().toString(), aliyunLiveConfig);
    }

    @Override
    public Boolean updateLiveInfo(LiveInfo liveInfo, Integer currentUserId) {
        // 获取当前用户的直播间id，更新信息
        liveInfoMapper.updateById(
                liveInfo.setIfLive(null)
                        .setUid(null)
                        .setScreenshot(null)
                        .setId(liveInfoMapper.selectOne(
                                new QueryWrapper<LiveInfo>()
                                        .eq("uid", currentUserId))
                                .getId()));
        redisUtil.del(KEY);
        return true;
    }

    @Override
    public List<UserLiveInfo> getAllLiveIfOn() {
        try {
            if (redisUtil.hasKey(KEY)) {
                return FastJsonUtils.toList(redisUtil.get(KEY), UserLiveInfo.class);
            }
        } catch (RedisConnectionFailureException e) {
            return getOnLive();
        }
        List<UserLiveInfo> onLive = getOnLive();
        redisUtil.setIfAbsent(KEY, onLive, RedisUtil.EXPIRETIME);
        return onLive;
    }

    @Override
    public List<UserLiveInfo> getAllLive() {
        return liveInfoMapper.getAllLive();
    }

    private List<UserLiveInfo> getOnLive() {
        return getAllLive()
                .stream()
                // 筛选开播的
                .filter(liveInfo -> liveInfo.getIfLive() == 1)
                // url添加前缀
                .peek(liveInfo -> liveInfo.setScreenshot(aliyunConfig.getUrlPrefix() + liveInfo.getScreenshot()))
                .collect(Collectors.toList());
    }

    @Override
    public String getPullUrl(Integer lId) {
        String key = KEY + ":" + lId;
        // 查询缓存
        try {
            if (redisUtil.hasKey(key)) {
                return redisUtil.get(key).toString();
            }
        } catch (RedisConnectionFailureException e) {
            return AliYunLiveUtil.createPullUrl(lId.toString(), aliyunLiveConfig, 3);
        }
        // 微信小程序直播只支持rtmp
        String pullUrl = AliYunLiveUtil.createPullUrl(lId.toString(), aliyunLiveConfig, 3);
        redisUtil.setIfAbsent(key, pullUrl, EXPIRED_TIME);
        // 生成播流地址
        return pullUrl;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveliveRecord(ApiSaveLiveModel model) {
        // 获取直播间id
        // 通过roomId获取直播标题和简介
        LiveInfo liveInfo = liveInfoMapper.selectOne(
                new QueryWrapper<LiveInfo>()
                        .eq("id", Integer.parseInt(model.getStream())));
        // 保存到视频分区 的录播分类
        String uri = model.getUri();
        Integer videoId = videoService.addLiveVideo(
                new Video()
                        .setScreenshot(uri + "?x-oss-process=video/snapshot,t_7000,f_jpg,w_0,h_0,m_fast")
                        .setUrl(uri)
                        .setTitle(liveInfo.getTitle())
                        .setInfo(liveInfo.getInfo())
                        .setTypeId(1)
                        .setCreateTime(new Date()));
        log.info("录制视频保存成功id: {}", videoId);
        // 添加资源权限公开
        permissionService.addPermission(
                new PermissionList()
                        .setIfPublic(1)
                        .setTypeId(1)
                        .setResourcesTid(1)
                        .setResid(videoId));
        return true;
    }

    /**
     * 判断直播是否过期
     *
     * @param end 结束时间
     * @return 是否过期
     */
    private Boolean isLiveExpired(Date end) {
        return System.currentTimeMillis() - end.getTime() >= TIMEOUT;
    }

    /**
     * 通过直播间id获取最后一次直播记录信息
     *
     * @param liveRoomId 房间id
     * @return 直播记录
     */
    private LiveRecord getLastLiveRecord(Integer liveRoomId) {
        return liveRecordService.getById(liveInfoMapper.selectById(liveRoomId).getRecordId());
    }

    /**
     * 直播开启事件处理
     *
     * @param houseId 直播间id
     */
    private void startLiveHandel(Integer houseId) {
        log.info("开启直播状态,房间号: {}", houseId);
        // 获取该直播间上次直播记录, 如果有则判断是否超过过期时间，如果超过则视为新的直播，否则延续上次直播
        LiveRecord record = getLastLiveRecord(houseId);
        Integer recordId = null;
        if (StringUtils.isEmpty(record) || isLiveExpired(record.getLiveEnd())) {
            // 创建新的直播记录,返回记录id
            recordId = liveRecordService.saveRecord(
                    new LiveRecord()
                            .setLiveStart(new Date())
                            .setLiveId(houseId));
        }
        // 标记该直播间为开播状态
        liveInfoMapper.updateById(
                new LiveInfo()
                        .setId(houseId)
                        .setIfLive(1)
                        // 设置最后一次开播记录
                        .setRecordId(recordId));
    }

    /**
     * 结束直播事件处理
     *
     * @param houseId 直播间id
     */
    private void endLiveHandel(Integer houseId) {
        String key = LIKE_KEY + houseId;
        log.info("关闭直播状态,房间号: {}", houseId);
        // 获取该直播间最后一次直播记录，写入结束时间
        LiveRecord record = getLastLiveRecord(houseId);
        Integer likeDb = record.getLike();
        // 从缓存中获取该次直播记录点赞数写入数据库
        liveRecordService.updateById(
                record.setLiveEnd(new Date())
                        // 设置点赞数
                        .setLike(StringUtils.isEmpty(likeDb) ? 0 : likeDb + Integer.parseInt(redisUtil.get(key).toString())));
        // 清理缓存
        redisUtil.del(key);
        // 标记该直播间为下播状态
        liveInfoMapper.updateById(
                new LiveInfo()
                        .setId(houseId)
                        .setIfLive(0));
    }

    @Override
    public void likeIncr(Integer liveId) {
        // 自增 设置过期时间
        redisUtil.incr(LIKE_KEY + liveId, 1, RedisUtil.EXPIRETIME);
    }
}
