package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.mapper.WebConfigMapper;
import com.zgn.fazhi.pojo.po.WebConfig;
import com.zgn.fazhi.service.WebConfigService;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class WebConfigServiceImpl extends ServiceImpl<WebConfigMapper, WebConfig> implements WebConfigService   {

    public static final String KEY = "webConfig";

    @Autowired
    private WebConfigMapper webConfigMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public Boolean updateUsernameLength(Integer length) {
        WebConfig config = webConfigMapper.selectOne(null);
        config.setUsernameLength(length);
        webConfigMapper.updateById(config);
        // 刷新缓存
        redisUtil.set(KEY, length);
        return true;
    }

    @Override
    public Integer getUserNameLength() {
        // 查询缓存是否有
        if (redisUtil.hasKey(KEY)){
            try {
                return Integer.parseInt(redisUtil.get(KEY).toString());
            } catch (ClassCastException e){
                String message = "用户名长度配置类型转换异常";
                log.error(message);
                throw new RestException(400, message);
            }
        } else {
            // 从数据库中读取返回并放入缓存中
            Integer usernameLength = webConfigMapper.selectOne(null).getUsernameLength();
            try {
                return usernameLength;
            } finally {
                redisUtil.set(KEY, usernameLength);
            }
        }
    }
}
