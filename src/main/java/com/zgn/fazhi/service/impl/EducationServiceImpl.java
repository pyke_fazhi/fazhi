package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.mapper.EducationMapper;
import com.zgn.fazhi.pojo.po.Education;
import com.zgn.fazhi.service.EducationService;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
@Slf4j
public class EducationServiceImpl extends ServiceImpl<EducationMapper, Education> implements EducationService {

    private static final String KEY = "educationKey";

    @Autowired
    EducationMapper educationMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public List<Education> getAllEducation() {
        try {
            if (hasKey()) {
                return redisUtil.lGetAll(KEY);
            } else {
                return selectCache();
            }
        } catch (RedisConnectionFailureException e) {
            log.error("redis连接异常, 正在从数据库中查询");
            return educationMapper.selectList(null);
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addEducation(Education education) {
        educationMapper.insert(education);
        redisUtil.lHasSet(KEY, education);
        return true;
    }

    @Override
    public Boolean updateEducationById(Education education) {
        educationMapper.updateById(education);
        if (hasKey()) {
            redisUtil.lRemove(KEY, 1, educationMapper.selectById(education.getId()));
            redisUtil.lSet(KEY, education);
        }
        return true;
    }

    @Override
    public Boolean delEducation(Integer id) {
        if (hasKey()) {
            redisUtil.lRemove(KEY, 1, educationMapper.selectById(id));
        }
        educationMapper.deleteById(id);
        return true;
    }

    private List<Education> selectCache() {
        Lock lock = new ReentrantLock();
        List<Education> educations;
        lock.lock();
        try {
            educations = educationMapper.selectList(null);
            if (StringUtils.isEmpty(educations)) {
                return null;
            }
            // 加入缓存中
            redisUtil.lSet(KEY, educations, RedisUtil.EXPIRETIME);
        } finally {
            lock.unlock();
        }
        return educations;
    }

    private void refreshCache() {
        redisUtil.del(KEY);
        selectCache();
    }

    Boolean hasKey() {
        return redisUtil.hasKey(KEY);
    }

}
