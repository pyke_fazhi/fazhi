package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.config.AliyunConfig;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.mapper.ResourcesMapper;
import com.zgn.fazhi.pojo.po.*;
import com.zgn.fazhi.pojo.vo.UserTime;
import com.zgn.fazhi.service.*;
import com.zgn.fazhi.utils.RedisUtil;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ResourcesServiceImpl extends ServiceImpl<ResourcesMapper, Resources> implements ResourcesService {

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private UserGradeService userGradeService;

    @Autowired
    private VideoService videoService;

    @Autowired
    private FileService fileService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private UserVideoService userVideoService;

    @Autowired
    private UserFileService userFileService;

    @Autowired
    private AliyunConfig aliyunConfig;

    @Override
    public List getResources(Integer resTid, Integer gid, Integer typeId) {
        List<Integer> resIdList;
        if (StringUtils.isEmpty(gid)) {
            // 表示非学生
            resIdList = permissionService.selectByTidAndEid(resTid, null, typeId);
        } else {
            // 通过学历id和资源分类id和详细分类id查询资源权限id集合
            resIdList = permissionService.selectByTidAndEid(resTid, userGradeService.selectEidByGid(gid), typeId);
        }
        String urlPrefix = aliyunConfig.getUrlPrefix();
        switch (resTid) {
            case 1: {
                // 视频分类
                return videoService.listByIds(resIdList)
                        .stream()
                        .peek(video -> video
                                .setUrl(urlPrefix + video.getUrl())
                                .setScreenshot(urlPrefix + video.getScreenshot()))
                        .collect(Collectors.toList());
            }
            case 2: {
                // 图文分类
                return pictureService.listByIds(resIdList)
                        .stream()
                        .peek(picture -> picture.setUrl(urlPrefix + picture.getUrl()))
                        .collect(Collectors.toList());
            }
            case 3: {
                // 文档分类
                return fileService.listByIds(resIdList)
                        .stream()
                        .peek(file -> file.setUrl(urlPrefix + file.getUrl()))
                        .collect(Collectors.toList());
            }
            default: {
                log.warn("当前分类不存在:{}", resTid);
                throw new RestException(400, "当前分类不存在");
            }
        }
    }

    @Override
    public Boolean saveUseTime(UserTime userTime, Integer uid) {
        switch (userTime.getResTid()) {
            case 1: {
                // 观看视频结束时间
                Date end = userTime.getEnd();
                // 观看视频开始时间
                Date start = userTime.getStart();
                // 视频分类
                if (StringUtils.isEmpty(end) || StringUtils.isEmpty(start)) {
                    throw new RestException(400, "开始结束时间参数为空");
                }
                Integer useTime = (int) (end.getTime() - start.getTime());
                if (useTime <= 0) {
                    throw new RestException(400, "结束时间必须大于开始时间");
                }
                UserVideo userVideo = new UserVideo();
                userVideo.setStart(start)
                        .setEnd(end)
                        .setLength(useTime)
                        .setUid(uid)
                        .setVid(userTime.getResId());
                userVideoService.save(userVideo);
                return true;
            }
            // case 2 文图模块 需求无时长需求
            case 3: {
                // 查询该用户数据是否初始化
                UserFile userFileDB = userFileService.findByUid(uid);
                // 文档分类
                userFileDB.setLength(userFileDB.getLength() + userTime.getUseTime());
                userFileService.updateById(userFileDB);
                return true;
            }
            default: {
                throw new RestException(400, "资源分类id参数错误");
            }
        }
    }
}
