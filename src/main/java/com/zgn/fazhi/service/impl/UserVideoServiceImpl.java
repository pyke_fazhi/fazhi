package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.pojo.po.UserVideo;
import com.zgn.fazhi.mapper.UserVideoMapper;
import com.zgn.fazhi.pojo.vo.UserVideoDate;
import com.zgn.fazhi.pojo.vo.UserVideoInfo;
import com.zgn.fazhi.service.UserInfoService;
import com.zgn.fazhi.service.UserVideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
public class UserVideoServiceImpl extends ServiceImpl<UserVideoMapper, UserVideo> implements UserVideoService {

    @Autowired
    private UserVideoMapper userVideoMapper;

    @Autowired
    private UserInfoService userInfoService;

    @Override
    public List<UserVideoInfo> getUserVideoInfo() {
        // 分组 uid ：总观看时长
        Map<Integer, Integer> uidTimeMap =
                userVideoMapper
                        .selectList(null)
                        .stream()
                        .collect(Collectors.groupingBy(UserVideo::getId,
                                Collectors.summingInt(UserVideo::getLength)));


        // 保存个人数据实体类
        UserVideoInfo userVideoInfo = new UserVideoInfo();
        // 封装实体类集合
        List<UserVideoInfo> userVideoInfos = new ArrayList<>();
        uidTimeMap.keySet().forEach(uid -> {
            UserInfo userInfo = userInfoService.getById(uid);
            userVideoInfo
                    .setUid(uid)
                    .setTime(uidTimeMap.get(uid))
                    .setName(userInfo.getName());
            userVideoInfos.add(userVideoInfo);
        });
        return userVideoInfos;
    }

    @Override
    public UserVideoDate getUserVideoInfoByUid(Integer uid) {

        List<UserVideo> userVideoList =
                userVideoMapper.selectList(
                        new QueryWrapper<UserVideo>().eq("uid", uid));
        UserVideoDate userVideoDate = new UserVideoDate();
        userVideoDate
                .setUserVideo(userVideoList)
                .setTotalTime(userVideoList.stream().mapToInt(UserVideo::getLength).sum());
        return userVideoDate;
    }

    @Override
    public Boolean saveVideoTime(UserVideo userVideo) {
        long start = userVideo.getStart().getTime();
        long end = userVideo.getStart().getTime();
        if (start >= end){
            throw new RestException(400, "开始时间不能大于结束时间");
        }
        if (StringUtils.isEmpty(userVideo.getLength())){
            userVideo.setLength((int)(end - start)/10);
        }
        userVideoMapper.insert(userVideo);
        return true;
    }
}
