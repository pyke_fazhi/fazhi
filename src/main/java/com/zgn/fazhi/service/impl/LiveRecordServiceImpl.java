package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.mapper.LiveRecordMapper;
import com.zgn.fazhi.pojo.po.LiveRecord;
import com.zgn.fazhi.service.LiveRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 21/08/2021 8:34 PM
 */

@Service
public class LiveRecordServiceImpl extends ServiceImpl<LiveRecordMapper, LiveRecord> implements LiveRecordService {

    @Autowired
    private LiveRecordMapper mapper;

    @Override
    public Integer saveRecord(LiveRecord liveRecord) {
        mapper.insert(liveRecord);
        return liveRecord.getId();
    }

    @Override
    public LiveRecord getLastRecord(Integer roomId) {
        return mapper.getLastRecord(roomId);
    }
}
