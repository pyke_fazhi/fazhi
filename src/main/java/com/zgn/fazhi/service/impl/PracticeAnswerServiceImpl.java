package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.pojo.po.PracticeAnswer;
import com.zgn.fazhi.mapper.PracticeAnswerMapper;
import com.zgn.fazhi.service.PracticeAnswerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
@Service
public class PracticeAnswerServiceImpl extends ServiceImpl<PracticeAnswerMapper, PracticeAnswer> implements PracticeAnswerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PracticeAnswerServiceImpl.class);

    @Autowired
    private PracticeAnswerMapper practiceAnswerMapper;
    /**
     * 通过id插入答案
     * @param map 练习题
     * @param id 练习题主键id
     */
    @Override
    public void addAnswerListById(Map<Integer, String> map, Integer id) {
        LOGGER.info("开始插入答案 id = " + id);
        for (int i = 5; i < map.size(); i++) {
            PracticeAnswer practiceAnswer = new PracticeAnswer();
            practiceAnswer.setPid(id);
            practiceAnswer.setAnswerOption(map.get(i));
            LOGGER.info("开始插入答案 : " + practiceAnswer);
            practiceAnswerMapper.insert(practiceAnswer);
            LOGGER.info("完成插入答案  : " + practiceAnswer);
        }
    }

    @Override
    public List<PracticeAnswer> findAllByPid(Integer pid) {
        QueryWrapper<PracticeAnswer> wrapper = new QueryWrapper<>();
        wrapper.eq("pid", pid);
        return practiceAnswerMapper.selectList(wrapper);
    }
}
