package com.zgn.fazhi.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.mapper.SchoolMapper;
import com.zgn.fazhi.pojo.po.School;
import com.zgn.fazhi.service.SchoolService;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Service
@Slf4j
public class SchoolServiceImpl extends ServiceImpl<SchoolMapper, School> implements SchoolService {

    @Autowired
    private SchoolMapper schoolMapper;

    @Autowired
    private RedisUtil redisUtil;

    private static final String KEY = "schoolNameKey";

    /**
     * 获取所有学校
     *
     * @return 返回所有学校的数组
     */
    @Override
    public List<School> getAllSchool() {
        try {
            if (redisUtil.hasKey(KEY)) {
                log.info("进入缓存，过期时间:{}", redisUtil.getExpire(KEY));
                return redisUtil.lGetAll(KEY);
            }
        } catch (RedisConnectionFailureException e) {
            // redis连接错误直接查询数据库
            e.printStackTrace();
            return schoolMapper.selectList(null);
        }
        // 没有缓存则从数据库中查询
        return selectDate();
    }

    @Override
    public School findByName(String name) {
        return schoolMapper.selectOne(new QueryWrapper<School>().eq("school_name", name));
    }

    @Override
    public Boolean delById(Integer id) {
        redisUtil.lRemove(KEY, 1, schoolMapper.selectById(id));
        if (redisUtil.hasKey(KEY)) {
            schoolMapper.deleteById(id);
        }
        return true;
    }

    @Override
    public Boolean addSchool(School school) {
        schoolMapper.insert(school);
        redisUtil.lHasSet(KEY, school);
        return true;
    }

    @Override
    public Boolean updateSchoolById(School school) {
        if (StringUtils.isEmpty(school.getId())) {
            throw new RestException(400, "学校id为空");
        }
        schoolMapper.updateById(school);
        if (redisUtil.hasKey(KEY)) {
            redisUtil.lRemove(KEY, 1, schoolMapper.selectById(school.getId()));
            redisUtil.lSet(KEY, school);
        }
        return true;
    }

    @Override
    public Boolean refreshCache() {
        redisUtil.del(KEY);
        redisUtil.lSet(KEY, schoolMapper.selectList(null), RedisUtil.EXPIRETIME);
        return true;
    }

    private List<School> selectDate(){
        // 没有缓存则查询数据库并加锁, 双重检测锁
        ReentrantLock lock = new ReentrantLock();
        List<School> schools;
        lock.lock();
        try {
            // 进来再检测一次是否有缓存,有则直接返回
            if (redisUtil.hasKey(KEY)){
                return redisUtil.lGetAll(KEY);
            }
            schools = schoolMapper.selectList(null);
            if (!schools.isEmpty()) {
                redisUtil.lSet(KEY, schools, RedisUtil.EXPIRETIME);
            }
        } finally {
            lock.unlock();
        }
        return schools;
    }

    @Override
    public String getByUid(Integer uid) {
        return schoolMapper.getByUid(uid);
    }
}
