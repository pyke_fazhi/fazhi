package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.LiveAudience;
import com.zgn.fazhi.pojo.po.LiveRecord;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.service.LiveAudienceService;
import com.zgn.fazhi.service.LiveRecordService;
import com.zgn.fazhi.service.UserInfoService;
import com.zgn.fazhi.service.WebSocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 22/08/2021 1:07 PM
 */

@Service
public class WebSocketServiceImpl implements WebSocketService {

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private LiveRecordService liveRecordService;

    @Autowired
    private LiveAudienceService liveAudienceService;

    /**
     * 保存观看记录阈值，30秒
     */
    private static final Integer SAVE_THRESHOLD = 30 * 1000;

    @Override
    public UserInfo getUser(Integer uid) {
        return userInfoService.findByUid(uid);
    }

    @Override
    public Boolean saveOnLineTime(String roomId, Integer uid, Date start) {
        int liveId = Integer.parseInt(roomId);
        // 如果进入时间到结束时间不到30秒则不记录
        if (System.currentTimeMillis() - start.getTime() <= SAVE_THRESHOLD) {
            return false;
        }
        // 获取该用户在该直播间的最后一次直播记录
        LiveRecord record = liveRecordService.getLastRecord(liveId);
        // 获取该用户在该次直播记录
        LiveAudience liveAudience = liveAudienceService.selectByUidRecordId(uid, liveId);
        // 计算在线时间 单位s
        int onLineTime = (int) ((System.currentTimeMillis() - start.getTime()) / 1000);
        if (StringUtils.isEmpty(liveAudience)) {
            // 如果为空则直接插入
            liveAudienceService.save(
                    new LiveAudience()
                            .setStart(start)
                            .setEnd(new Date())
                            .setRecordId(record.getId())
                            .setUid(uid)
                            .setViewTime(onLineTime));
        } else {
            // 否则叠加观看时间
            liveAudience.setViewTime(liveAudience.getViewTime() + onLineTime);
            liveAudienceService.updateById(liveAudience);
        }
        return true;
    }
}
