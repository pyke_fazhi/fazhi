package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.UserInfo;

import java.util.Date;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 22/08/2021 1:06 PM
 */

public interface WebSocketService {

    /**
     * 通过用户id查找用户
     *
     * @param uid 用户id
     * @return 用户对象
     */
    UserInfo getUser(Integer uid);

    /**
     * 保存用户在线时间
     *
     * @param roomId 房间id
     * @param uid    用户id
     * @param start  进入直播间时间
     * @return 是否保存成功
     */
    Boolean saveOnLineTime(String roomId, Integer uid, Date start);

}
