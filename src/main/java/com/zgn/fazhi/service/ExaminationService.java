package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.Examination;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.ExaminationtTypeOption;
import com.zgn.fazhi.pojo.vo.ExaminationOption;
import com.zgn.fazhi.pojo.vo.ExaminationUnit;
import com.zgn.fazhi.pojo.vo.Grades;
import com.zgn.fazhi.result.Result;

import java.util.HashMap;
import java.util.List;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface ExaminationService extends IService<Examination> {


    List<Examination> selectByType(int id);

    List<Examination> selectAllPaper();

    Examination selectPaperById(int id);

    Result addPaper(Examination examination);

    Result updatePaperById(Examination examination);

    /**
     * 返回数据包括进行中、结束两种考试
     * 每个考试包含开始时间、结束时间、考试时长、当前用户是否可考试数据
     * @param uid 用户id
     */
    HashMap<String, List<Grades>> findExaminationByUid(Integer uid);

    Boolean delExaminationById(Integer id);

    /**
     * 管理员批量上传考题到私有题库
     * @param list 考题数组
     * @return 是否上传成功
     */
    Boolean addExaminationListAdmin(List<Map<Integer, String>> list);

    /**
     * 教师上传考题
     * @param list 考题数据
     * @param tid 教师id
     * @return 是否上传成功
     */
    Boolean addExaminationListTeacher(List<Map<Integer, String>> list, Integer tid);

    /**
     * 教师上传单个题目
     * @param tid 教师id
     * @param examination 题目
     * @return
     */
    Boolean addExaminationTeacher(Integer tid, ExaminationOption examination);

    /**
     * 查询指定教师的题库
     * @param tid 教师id
     * @return 题库
     */
    List<Examination> selectAllByTid(Integer tid);

    /**
     * 教师删除考题
     * @return 是否删除成功
     */
    Boolean delTeacherExaById(Integer eid, Integer tid);

    /**
     * 根据条件随机抽取题目
     * @param examinationtTypeOption 条件配置
     * @return 指定随机的题目id集合
     */
    List<Integer> getRandomList(ExaminationtTypeOption examinationtTypeOption);

    /**
     * 单元级别的随机抽取
     */
    List<Integer> getRandomListCidNull(Integer typeId, Integer unitId, Integer number);

    List<Integer> getRandomListTidNull(Integer typeId, Integer unitId, Integer chapterId, Integer number);

    List<Integer> getRandomListTidNull(Integer typeId, Integer unitId, Integer number);

    List<Integer> getRandomListByUid(Integer uid, Integer typeId, Integer number);

    /**
     * 批量删除考题练习题缓存
     * @param idList id集合
     * @return
     */
    Boolean delExaCache(List<Integer> idList);
    
    /**
     * 通过单元id和章节id查找所有题目
     */
    List<ExaminationUnit> getListByUnit(Integer unitId);

    List<ExaminationUnit> getListByChapter(Integer chapterId);


}
