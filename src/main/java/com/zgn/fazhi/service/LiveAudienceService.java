package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.LiveAudience;

/**
 * @author ydw
 * @version 1.0
 * @description TODO
 * @date 21/08/2021 8:37 PM
 */

public interface LiveAudienceService extends IService<LiveAudience> {

    /**
     * 观看直播用户信息
     * @param uid 用户id
     * @param recordId 直播记录id
     * @return 用户信息
     */
    LiveAudience selectByUidRecordId(Integer uid, Integer recordId);

}
