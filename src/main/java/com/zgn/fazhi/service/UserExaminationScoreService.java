package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.UserExaminationScore;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.vo.UserExaGrade;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface UserExaminationScoreService extends IService<UserExaminationScore> {


    Boolean insertUserExaminationScore(UserExaminationScore userExaminationScore);

    /**
     *  查询该用户对该次考试练习的成绩
     */
    List<UserExaGrade> getCurrentExaDateByUid(Integer uid, Integer rid);

    /**
     * 获取当前用户历次所有考试成绩
     * @return
     */
    List<UserExaGrade> getCurrentExaDateListByUid(Integer uid);

}
