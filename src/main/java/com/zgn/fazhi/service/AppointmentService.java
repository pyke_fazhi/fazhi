package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.Appointment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.AppointmentContent;
import com.zgn.fazhi.pojo.vo.AppointmentInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface AppointmentService extends IService<Appointment> {


    /**
     * 通过日期查找预约数据
     * @param date 日期
     * @return 返回预约数据
     */
    ArrayList<Map<String, Object>> getAppointmentByDate(String date);

    /**
     * 通过日期和预约时间段id查找预约记录
     * @param date 预约日期
     * @param iId 预约时间段id
     * @return 返回符合的记录
     */
    List<Appointment> getAppointmentByDateAndIid(Date date, Integer iId);

    /**
     * 插入预约记录
     * @param appointment 预约数据
     */
    Boolean insertAppointment(AppointmentInfo appointment);

    /**
     * 获取所有预约数据(包括该预约记录的参观地点
     * @return
     */
    List<Appointment> getAllApp();

    /**
     * 通过id删除预约数据
     * @param id 需要删除预约的id
     * @return 是否删除成功
     */
    Boolean delAppById(Integer id);

    /**
     * 查看该次预约是否成功
     * @param appointment 预约记录
     * @return 是否预约成功
     */
    Boolean isFull(Appointment appointment);

    /**
     * 通过uid查询用户所有预约情况
     * @return
     */
    List<Appointment> findAllByUid(Integer uid);

}
