package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.AppointmentContent;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface AppointmentContentService extends IService<AppointmentContent> {
    /**
     * 查询所有可参观地点
     * @return
     */
    List<AppointmentContent> getAllContent();

}
