package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.SecretGuard;

/**
 * @author ydw
 * @version 1.0
 * @Description TODO
 * @date 20/08/2021 11:42 AM
 */

public interface SecretGuardService extends IService<SecretGuard> {
}
