package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.Video;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.vo.FileUploadForm;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface VideoService extends IService<Video> {

    /**
     * 上传视频文件
     * @param fileUploadForm 视频文件
     */
    Boolean videoUpload(FileUploadForm fileUploadForm);

    Boolean delById(Integer id);

    List<Video> listByTid(Integer tid);

    /**
     * 视频截帧
     * @param uploadFile 视频文件
     * @return
     */
    String screenshot(MultipartFile uploadFile);

    /**
     * 添加录播分类视频
     * @param video 录播视频信息
     * @return 视频id
     */
    Integer addLiveVideo(Video video);

}
