package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.File;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.vo.FileUploadForm;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface FileService extends IService<File> {

    /**
     * 上传文件
     */
    Boolean fileUpload(FileUploadForm fileUploadForm);

    /**
     * 通过id删除文件
     * @param id 文件id
     */
    Boolean delById(Integer id);

    /**
     * 通过分类id查询
     * @param tid 分类id
     */
    List<File> listByTid(Integer tid);

}
