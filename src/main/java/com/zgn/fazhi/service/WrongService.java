package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.Wrong;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface WrongService extends IService<Wrong> {

    /**
     * 通过用户id查询该用户错题集
     * @param uid 用户id
     * @return 错题集数组
     */
    List<Wrong> findByUid(Integer uid);

    /**
     * 给用户添加错题集
     * @param uid 用户id
     * @param pid 练习题id
     * @return 是否添加成功
     */
    Boolean addWrong(Integer uid, Integer pid);

    /**
     * 删除用户错题集
     * @param uid 用户id
     * @param pid 练习题id
     * @return 是否删除成功
     */
    Boolean delWrong(Integer uid, Integer pid);

}
