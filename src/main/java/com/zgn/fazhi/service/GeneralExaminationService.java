package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.Examination;
import com.zgn.fazhi.pojo.po.GeneralExamination;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.RecordExa;
import com.zgn.fazhi.pojo.vo.AddExamination;

import java.util.List;

/**
 *  服务类
 *
 * @author wt
 * @since 2021-06-25
 */
public interface GeneralExaminationService extends IService<GeneralExamination> {

    /**
     * 通过统考id为该次统考自动组题
     * @return 是否成功
     */
    Boolean addAutoExamination(AddExamination examination, Integer uid);

    /**
     /**
     * 手动组题
     * @param eIdList 考题id的数组
     * @param eid 统考id
     * @return 是否插入成功
     */
    Boolean manual(List<Integer> eIdList, Integer eid);

    /**
     * 通过统考id查询该考试的所有考题id
     * @param eid 统考id
     * @return 该统考的所有考题id
     */
    List<GeneralExamination> findAllByEid(Integer eid);

    /**
     * 查找指定考试的所有题目
     * @param rid 统考id
     * @return 该次考试的所有考题
     */
    List<RecordExa> findAllExaminationByRid(Integer rid, Integer uid);

    /**
     * 刷新redis缓存
     */
    Boolean refreshCache();

    /**
     * 通过记录id删除所有数据
     * @return
     */
    Boolean delByRid(Integer rid);


}
