package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.ExaminationPermission;
import com.zgn.fazhi.pojo.vo.ExaPermission;
import com.zgn.fazhi.pojo.vo.ExaPermissionVo;
import com.zgn.fazhi.pojo.vo.ExaPermissionVoList;

import java.util.List;

public interface ExaminationPermissionService extends IService<ExaminationPermission> {

    /**
     * 通过id对考试或练习添加开放人群
     */
    Boolean addExaPermission(ExaPermission exaPermission);

    Boolean delExaPermissionById(List<Integer> idList);

    /**
     * 查询开放级别为学校级别
     * @param schoolId 学校id
     * @return 对该学校开放的所有考试/练习记录id
     */
    List<Integer> selectAll(Integer schoolId);

    /**
     * 查询开放级别为年级级别
     * @param schoolId 学校id
     * @param gradeId 年级id
     * @return 对该年级开放的所有考试/练习记录id
     */
    List<Integer> selectAll(Integer schoolId, Integer gradeId);

    /**
     * 查询开放级别为班级级别
     * @param schoolId 学校id
     * @param gradeId 年级id
     * @param classId 班级id
     * @return 对该班级开放的所有考试/练习记录id
     */
    List<Integer> selectAll(Integer schoolId, Integer gradeId, Integer classId);

    /**
     * 通过记录id查询开放权限
     */
    ExaPermissionVoList getExaPermissionByRid(Integer rid);

}
