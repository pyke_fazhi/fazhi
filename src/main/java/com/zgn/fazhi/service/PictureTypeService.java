package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.FileType;
import com.zgn.fazhi.pojo.po.PictureType;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface PictureTypeService extends IService<PictureType> {

    /**
     * 获取全部图片类型
     *
     */
    List<PictureType> getAllPictureType();

    /**
     * 根据id获取图片类型
     *
     */
    PictureType getAllPictureTypeById(Integer id);

    /**
     * 增加图片类型
     *
     */
    Boolean addPictureType(PictureType pictureType);

    /**
     * 根据id删除图片类型
     *
     */
    Boolean deletePictureTypeById(Integer id);

    /**
     * 修改图片类型
     *
     */
    Boolean updatePictureType(PictureType pictureType);

    /**
     * 刷新redis缓存
     * @return
     */
    Boolean refreshCache();


}
