package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.WebConfig;

public interface WebConfigService extends IService<WebConfig> {

    Boolean updateUsernameLength(Integer length);

    Integer getUserNameLength();

}
