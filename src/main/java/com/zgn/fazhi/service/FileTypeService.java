package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.File;
import com.zgn.fazhi.pojo.po.FileType;
import com.zgn.fazhi.pojo.po.VisitContent;

import java.util.List;

/**
 * @author ydw
 */
public interface FileTypeService extends IService<FileType> {

    /**
     * 获取全部文档类型
     *
     */
    List<FileType> getAllFileType();

    /**
     * 根据id获取文档类型
     *
     */
    FileType getAllFileTypeById(Integer id);

    /**
     * 增加文档类型
     *
     */
    Boolean addFileType(FileType fileType);

    /**
     * 根据id删除文档类型
     *
     */
    Boolean deleteFileTypeById(Integer id);

    /**
     * 修改文档类型
     *
     */
    Boolean updateFileType(FileType fileType);
}
