package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.Education;
import org.springframework.stereotype.Service;

import java.util.List;


public interface EducationService extends IService<Education> {

    /**
     * 获取所有学历
     * @return 返回所有学历的数组
     */
    List<Education> getAllEducation();

    /**
     * 增加学历
     */
    Boolean addEducation(Education education);

    /**
     * 更新学历
     */
    Boolean updateEducationById(Education education);

    /**
     * 根据id删除学历
     */
    Boolean delEducation(Integer id);

}
