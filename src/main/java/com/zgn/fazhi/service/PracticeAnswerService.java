package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.PracticeAnswer;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface PracticeAnswerService extends IService<PracticeAnswer> {

    /**
     * 通过id插入答案
     * @param map 练习题
     * @param id 练习题主键id
     */
    void addAnswerListById(Map<Integer,String> map, Integer id);

    /**
     * 通过练习题id查找所有选项
     * @return
     */
    List<PracticeAnswer> findAllByPid(Integer pid);

}
