package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.VisitContent;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.result.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface VisitContentService extends IService<VisitContent> {

    /**
     * 获取全部可参观地点
     */
    List<VisitContent> getAllVisitContent();

    /**
     * 通过id获取该数据
     *
     */
    VisitContent getAllVisitContentById(Integer id);


    Boolean addVisitContent(VisitContent visitContent);


    Boolean deleteVisitContentById(Integer id);


    Boolean updateVisitContent(VisitContent visitContent);

    /**
     * 刷新缓存
     * @return
     */
    Boolean refreshRedis();

    /**
     * 查看该key的过期时间
     * @return 过期时间
     */
    long checkExpire();

}
