package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.AppointmentList;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author ydw
 * @version 1.0
 * @Description TODO
 * @date 21/08/2021 11:35 AM
 */


public interface AppointmentListService extends IService<AppointmentList> {

    /**
     * 上传名单
     * @param aid 预约记录id
     * @param file excel
     * @return
     */
    Boolean uploadExcel(Integer aid, MultipartFile file);

    Boolean addList(List<Map<Integer, String>> list, Integer aid);

}
