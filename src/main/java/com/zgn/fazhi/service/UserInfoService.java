package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.Role;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.vo.UserExcel;
import com.zgn.fazhi.pojo.vo.UserInfoSchool;
import com.zgn.fazhi.result.Result;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface UserInfoService extends IService<UserInfo> {
    String addUser(UserInfo userInfo);

    List<UserInfoSchool> selectRoleId(int id);

    Boolean batchDelete(List<Integer> list);

    Boolean updateUser(UserInfo userInfo);

    List<UserInfoSchool> selectAll();

    UserInfoSchool selectByUsername(String username);


    /**
     * 批量添加用户信息
     */
    void addUserList(List<UserExcel> userExcelList, Integer uid);

    /**
     * 通过角色id查询所有用户
     * @param roleId 角色id
     * @return 拥有该角色的所有用户数组
     */
    List<UserInfo> findAllByRoleId(Integer roleId);

    /**
     * 改变用户角色
     * @param uid 用户id
     * @param roleId 需要赋予的角色id
     * @return 是否成功
     */
    Boolean updateUserRoleId(Integer uid, Integer roleId);

    /**
     * 通过用户名查找用户
     * @param name
     * @return
     */
    UserInfo findByUsername(String name);

    /**
     * 修改密码
     * @param uid 用户id
     * @param password 新密码
     * @return 是否修改成功
     */
    Boolean updatePassword(Integer uid, String password);

    /**
     * 查询所有用户
     * @return
     */
    List<UserInfo> findAll();

    /**
     * 通过用户id查询用户信息
     * @param uid 用户id
     * @return 用户信息
     */
    UserInfo findByUid(Integer uid);

    /**
     * 根据密保答案和用户名找回密码
     * @param answer 密保答案
     * @param username 用户名
     * @return
     */
    String retrieve(String answer, String username);

    /**
     * 通过回调key修改密码
     * @param key key
     * @param password 密码
     * @return
     */
    Boolean updatePasswordByKey(String key, String password);

    /**
     * 通过用户名获取密保问题
     * @param username 用户名
     * @return 密保问题
     */
    String getQuestion(String username);

    /**
     * 查询所在该学校的所有用户id
     * @param schoolId 学校id
     * @return 用户id
     */
    List<Integer> getIdListBySchoolId(Integer schoolId);

    /**
     * 通过学校id和年级id查看该学校的该年级的全部用户id
     * @param schoolId 学校id
     * @param gradeId 年级id
     * @return 用户id
     */
    List<Integer> getIdListByGradeId(Integer schoolId, Integer gradeId);

    /**
     * 通过班级id查询该班级的所有用户
     * @param classId 班级id
     * @return 用户id
     */
    List<Integer> getIdListByClassId(Integer classId);

    /**
     * 计算当前用户量
     * @return 用户数
     */
    Integer countUser();

}
