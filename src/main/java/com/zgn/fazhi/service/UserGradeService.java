package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.Education;
import com.zgn.fazhi.pojo.po.UserGrade;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserGradeService extends IService<UserGrade> {

    /**
     * 获取所有年级
     * @return 返回所有年级的数组
     */
    List<UserGrade> getAllUserGrade();

    /**
     * 增加年级
     */
    Boolean addUserGrade(UserGrade userGrade);

    /**
     * 更新年级
     */
    Boolean updateUserGradeById(UserGrade userGrade);

    /**
     * 根据id删除年级
     */
    Boolean delUserGrade(Integer id);

    /**
     * 通过年级名称查找年级
     * @param gName 年级名
     * @return
     */
    UserGrade selectByName(String gName);

    /**
     * 查询所有年级放入缓存中
     * @return 缓存key
     */
    List<UserGrade> saveCache();

    /**
     * 通过年级id查询学历id
     * @param gid 年级id
     * @return 学历id
     */
    Integer selectEidByGid(Integer gid);

}
