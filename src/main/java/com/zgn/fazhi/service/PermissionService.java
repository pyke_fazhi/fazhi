package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.Permission;
import com.zgn.fazhi.pojo.vo.PermissionList;

import java.util.List;

public interface PermissionService extends IService<Permission> {

    /**
     * 为该资源添加权限
     * @param permissionList 资源信息以及角色列表
     */
    Boolean addPermission(PermissionList permissionList);

    /**
     * 更新该资源权限
     * @param permissionList 资源信息以及角色列表
     */
    Boolean updatePermission(PermissionList permissionList);

    /**
     * 通过资源类型id和资源id删除该权限
     * @param resTid 资源类型id
     * @param resId 资源id
     */
    Boolean delPermission(Integer resTid, Integer resId);

    /**
     * 通过资源类型id和资源id查询权限列表
     * @param resTid 资源类型id
     * @param resId 资源id
     * @return 权限列表
     */
    List<String> getByTidAndRid(Integer resTid, Integer resId);

    /**
     * 通过资源分类id和学历id查询
     * @param resTid 资源分类id
     * @param eid 学历id
     * @param typeId 详细分类id
     * @return 该资源的id集合
     */
    List<Integer> selectByTidAndEid(Integer resTid, Integer eid, Integer typeId);


}
