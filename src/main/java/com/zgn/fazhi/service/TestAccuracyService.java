package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.TestAccuracy;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface TestAccuracyService extends IService<TestAccuracy> {

}
