package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.Carousel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-26
 */
public interface CarouselService extends IService<Carousel> {

}
