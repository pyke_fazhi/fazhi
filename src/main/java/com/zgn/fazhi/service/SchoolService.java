package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.School;

import java.util.List;

public interface SchoolService extends IService<School> {
    /**
     * 获取所有学校
     * @return 返回所有学校的数组
     */
    List<School> getAllSchool();

    /**
     * 通过name查找学校
     * @return 返回学校
     */
    School findByName(String name);

    /**
     * 通过id删除学校
     * @param id 学校id
     * @return 是否删除成功
     */
    Boolean delById(Integer id);

    /**
     * 添加学校
     */
    Boolean addSchool(School school);


    Boolean updateSchoolById(School school);

    Boolean refreshCache();

    /**
     * 通过用户id查询学校名
     * @param uid 用户id
     * @return 学校名
     */
    String getByUid(Integer uid);

}
