package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.ClassInfo;
import java.util.List;


public interface ClassInfoService extends IService<ClassInfo> {

    /**
     * 获取所有班级
     * @return 返回所有班级的数组
     */
    List<ClassInfo> getAllClassInfo();

    /**
     * 增加班级
     */
    Boolean addClassInfo(ClassInfo classInfo);

    /**
     * 更新班级
     */
    Boolean updateClassInfoById(ClassInfo classInfo);

    /**
     * 根据id删除班级
     */
    Boolean delClassInfo(Integer id);

    /**
     *
     * @param sName 学校名
     * @param gName 年级名
     * @return 符合条件的班级
     */
    List<ClassInfo> getAllClassBySnameAndGname(String sName, String gName);

    ClassInfo selectByName(String name);

    List<ClassInfo> getAllClassBySidGid(Integer sid, Integer gid);

}
