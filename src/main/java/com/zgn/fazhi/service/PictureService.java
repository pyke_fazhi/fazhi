package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.Picture;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.vo.FileUploadForm;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface PictureService extends IService<Picture> {

    /**
     * 上传图片
     */
    Boolean pictureUpload(FileUploadForm imageUploadForm);

    /**
     * 通过id删除图片
     * @param id 图片id
     */
    Boolean delById(Integer id);

    /**
     * 通过分类id查询
     * @param tid 分类id
     */
    List<Picture> listByTid(Integer tid);

}
