package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.UserVideo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.vo.UserVideoDate;
import com.zgn.fazhi.pojo.vo.UserVideoInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface UserVideoService extends IService<UserVideo> {

    /**
     * 获取用户观看视频总数据
     * @return
     */
    List<UserVideoInfo> getUserVideoInfo();

    UserVideoDate getUserVideoInfoByUid(Integer uid);

    /**
     * 保存用户观看视频时间
     * @param userVideo
     * @return
     */
    Boolean saveVideoTime(UserVideo userVideo);

}
