package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.Evaluate;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.result.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface EvaluateService extends IService<Evaluate> {

    List<Evaluate> getAll();

    Boolean addEvaluate(Evaluate evaluate, Integer uid);

    /**
     * 判断该用户是否评价
     * @param uid 用户id
     * @return 是否评价
     */
    Boolean getEvaluate(Integer uid);

    /**
     * 刷新缓存
     * @return
     */
    Boolean refresh();

}
