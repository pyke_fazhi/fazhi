package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.SignIn;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface SignInService extends IService<SignIn> {

}
