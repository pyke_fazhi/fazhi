package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.EtypeScore;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface EtypeScoreService extends IService<EtypeScore> {

    /**
     * 插入该次统考的题型设置
     * @param etypeScore 题型设置
     */
    void insertEtype(EtypeScore etypeScore);

    /**
     * 通过统考id查询该次统考题型设置
     * @param eid 统考id
     * @return 题型信息
     */
    List<EtypeScore> findAllById(Integer eid);

    /**
     * 通过考试/练习id和题型id查找该配置
     * @return
     */
    EtypeScore findByEidAndTid(Integer eid, Integer tid);

    /**
     * 通过记录id删除所有配置
     * @return
     */
    Boolean delByEid(Integer eid);

}
