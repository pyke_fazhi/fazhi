package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.ExaminationRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.pojo.vo.AddRecord;
import com.zgn.fazhi.pojo.vo.ExaRecordMap;
import com.zgn.fazhi.pojo.vo.SubmitExamination;
import com.zgn.fazhi.pojo.vo.UserExaGrades;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface ExaminationRecordService extends IService<ExaminationRecord> {

    /**
     * 插入统考记录
     * @param examinationRecord 需要插入的统考配置信息
     */
    void addRecord(ExaminationRecord examinationRecord);

    /**
     * 查询所有统考记录
     * @return 统考数据数组
     */
    List<ExaminationRecord> findAllExamination();

    /**
     * 通过统考id查找
     * @return
     */
    ExaminationRecord findById(Integer id);

    /**
     * 开始一次统考
     * 需要向两张表插入，开启事务
     *
     * @param record 统考的基本设置
     * @return 是否开启成功
     */
    Boolean addExaminationRecord(AddRecord record);

    /**
     * 查看该用户现有的所有考试
     */
    ExaRecordMap getExaminationByUid(UserInfo userInfo, Integer ifPractice);

    /**
     * 用户提交考试/练习
     * @param submitExamination 用户考试数据
     * @return 是否提交成功
     */
    UserExaGrades submitExamination(SubmitExamination submitExamination);

    /**
     * 管理员/教师获取所有考试练习
     * @param ifPractice 是否为练习
     * @param userInfo 当前用户
     */
    List<ExaminationRecord> getAllExa(Integer ifPractice, UserInfo userInfo);

    /**
     * 删除考试练习记录
     * @param rid 考试练习记录id
     * @return
     */
    Boolean delRecord(Integer rid);

    /**
     * 刷新缓存
     * @return
     */
    Boolean refreshCache();

}
