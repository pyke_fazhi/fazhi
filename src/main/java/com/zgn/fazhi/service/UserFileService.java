package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.UserFile;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.vo.UserFileData;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface UserFileService extends IService<UserFile> {

    /**
     * 通过用户id查找该用户使用时长
     * @param uid 用户id
     * @return 使用时长
     */
    UserFile findByUid(Integer uid);

    /**
     * 查询所有用户的浏览文件时长
     * @return
     */
    List<UserFileData> getFileTimeData();

}
