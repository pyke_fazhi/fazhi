package com.zgn.fazhi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.po.Unit;

import java.util.List;

public interface UnitService extends IService<Unit> {

    /**
     * 添加单元
     * @param unit 单元对象
     * @return 是否添加成功
     */
    Boolean addUnit(Unit unit);

    /**
     * 通过单元id删除单元
     * @param id 单元id
     * @return 是否删除成功
     */
    Boolean delUnitById(Integer id);

    /**
     * 查询所有单元
     * @return 返回单元数组数据
     */
    List<Unit> findAllUnit();

    /**
     * 通过id查询单元
     * @return 单元数据
     */
    Unit findById(Integer id);

    /**
     * 通过单元标题查询单元
     * @param title 标题
     * @return 单元数据
     */
    Unit findByTitle(String title);

}
