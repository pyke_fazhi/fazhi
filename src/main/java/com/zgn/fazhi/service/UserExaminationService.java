package com.zgn.fazhi.service;

import com.zgn.fazhi.pojo.po.UserExamination;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zgn.fazhi.pojo.vo.ExaGradesDate;
import com.zgn.fazhi.pojo.vo.SubmitExamination;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wt
 * @since 2021-06-25
 */
public interface UserExaminationService extends IService<UserExamination> {

    /**
     * 查询用户考试成绩
     * @param uid 用户id
     * @param recordId 记录id
     * @return 考试成绩
     */
    List<UserExamination> findAllByUid(Integer uid, Integer recordId);



    /**
     * 通过用户id、统考id查询用户是否还能考试
     * @param uid 用户id
     * @param rid 统考id
     * @return 是否还能考试
     */
    int ifExamination(Integer uid, Integer rid);

    /**
     * 查找该次考试的所有成绩
     * @param uid 用户id
     * @param recordId 统考id
     * @return 成绩数组
     */
    List<Integer> getScoreByUid(Integer uid, Integer recordId);

    /**
     * 通过考试/练习id获取该次练习考试的数据
     *  数据包括该次考试练习的平均分、正确率、参与人数、参与率
     */
    ExaGradesDate getExaDataByRid(Integer rid);

}
