package com.zgn.fazhi;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zgn.fazhi.mapper.AppointmentMapper;
import com.zgn.fazhi.mapper.PracticeAnswerMapper;
import com.zgn.fazhi.mapper.UserInfoMapper;
import com.zgn.fazhi.pojo.po.Appointment;
import com.zgn.fazhi.pojo.po.PracticeAnswer;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.utils.BelongCalendar;
import com.zgn.fazhi.utils.RedisUtil;
import com.zgn.fazhi.utils.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@SpringBootTest
class FazhiApplicationTests {

    @Autowired
    private PracticeAnswerMapper practiceAnswerMapper;

    @Autowired
    private AppointmentMapper appointmentMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Test
    void contextLoads() {

        PracticeAnswer practiceAnswer = new PracticeAnswer();
        practiceAnswer.setPid(1);
        practiceAnswer.setAnswerOption("躺平");
        practiceAnswerMapper.insert(practiceAnswer);


    }

    @Test
    void test1() throws ParseException {
        List<String> fetureDate = getFetureDate(1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
        for (String s : fetureDate) {
            Appointment appointment = new Appointment();
            appointment.setGoDate(simpleDateFormat.parse(s));
//            Date date = simpleDateFormat.parse(result);
            appointmentMapper.insert(appointment);
        }
        System.out.println(fetureDate);
    }

    // 获取后past天的日期
    public static List<String> getFetureDate(int past) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        ArrayList<String> dateList = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR));
        // 加入当天数据
        dateList.add(format.format(calendar.getTime()));
        past--;
        for (int i = 0; i < past; i++) {
//            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + past);
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 1);
            Date today = calendar.getTime();
//            String result = format.format(today);
            dateList.add(format.format(today));
        }

//        System.out.println("今天 : " + today);


        // str转date
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");//注意月份是MM
//        Date date = simpleDateFormat.parse(result);
//        System.out.println(date);
//        Log.e(null, result);
        return dateList;
    }

    @Test
    public void test2(){
        redisUtil.hSet("test","1","A");
        String test = (String) redisUtil.hget("test", "1");
        System.out.println(test);
    }

    @Test
    public void test3(){
        HashMap<String, Object> map = new HashMap<>();
        map.put("1","A");
        map.put("3","B");
        map.put("4","AC");
        redisUtil.hmSet("test1", map);
        String test1 = (String) redisUtil.hget("test1", "1");
        String test2 = (String) redisUtil.hget("test1", "4");
        String test3 = (String) redisUtil.hget("test1", "3");
        System.out.println(test1);
        System.out.println(test2);
        System.out.println(test3);
    }

    /**
     * 测试时间差
     * @throws ParseException
     */
    @Test
    public void test4() throws ParseException {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d1 = df.parse("2004-03-26 11:30:40");
        Date d2 = df.parse("2004-03-26 11:30:24");
        System.out.println(d1);
        System.out.println(d2);
        long l = d1.getTime() - d2.getTime();
        // 返回的是毫秒时间差
        System.out.println(l);
        System.out.println(l/1000);

    }

    @Test
    public void test5() throws ParseException {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d1 = df.parse("2004-03-26 11:30:24");
        Date d2 = df.parse("2004-03-26 11:30:40");
        System.out.println(d1.getTime());
        System.out.println(d2.getTime());
        long l = d1.getTime() - d2.getTime();
        System.out.println(Math.abs(l));
        // 返回的是毫秒时间差
        System.out.println(l);
        System.out.println(l/1000);

    }

    @Test
    public void test6() throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d1 = df.parse("2004-03-26 11:30:24");
        Date d2 = df.parse("2004-03-26 11:30:40");
        Date d3 = df.parse("2004-03-26 11:30:41");
        boolean b = BelongCalendar.inCalendar(d1, d2, d3);
        System.out.println(b);

    }
    @Test
    public void test7() throws ParseException {
        int a=9;
        int b=7;
        DecimalFormat df=new DecimalFormat("0.00");

        String format = df.format((float) a / b);

    }

    @Test
    public void test8() {
        QueryWrapper<Appointment> appointmentQueryWrapper = new QueryWrapper<>();
        appointmentQueryWrapper.eq("go_date", "2021/06/27");
        List<Appointment> appointments = appointmentMapper.selectList(appointmentQueryWrapper);
        System.out.println(appointments);
    }

    @Test
    public void test10() {
        /**
         * 只返回满足条件的其中一条语句即可
         */
            QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<UserInfo>();
//            queryWrapper.in("id",Arrays.asList(1,2)).last("limit 1");
            queryWrapper.in("pid",Arrays.asList(1,2));
            List<UserInfo> userInfoList = userInfoMapper.selectList(queryWrapper);
            userInfoList.forEach(System.out::println);
    }


}
