package com.zgn.fazhi;

import com.zgn.fazhi.pojo.vo.Answer;
import com.zgn.fazhi.utils.FastJsonUtils;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@Slf4j
public class RedisTest {

    @Autowired
    private RedisUtil redisUtil;

    private static final String KEY = "redisUnitTest";
    private static final String KEY2 = "redisUnitTest";

    @Test
    void test1() throws InterruptedException {
        // 测试一个不存在的key调用是否过期返回值
        log.info("不存在的key是否存在: {}", redisUtil.hasKey(KEY));
        log.info("不存在的key是否过期: {}", redisUtil.isExpire(KEY));
        redisUtil.set(KEY, "test", 3);
        log.info("存在的key且没有过期, 是否过期:{}", redisUtil.isExpire(KEY));
        TimeUnit.SECONDS.sleep(3);
        log.info("存在的key且过期，查看是否过期:{}", redisUtil.isExpire(KEY));
        log.info("存在的key且过期, 查看是否存在:{}", redisUtil.hasKey(KEY));
        redisUtil.set(KEY, "test", 3);
        log.info("存在的key过期后重新赋值且设置时间是否有效: {}, {}", redisUtil.isExpire(KEY), redisUtil.hasKey(KEY));
        redisUtil.set(KEY2, "test");
        log.info("存在的key没有设置过期时间, 查看是否过期:{}", redisUtil.isExpire(KEY2));

        redisUtil.del(KEY);
        redisUtil.del(KEY2);
        /**
         * 总结： 一个key设置过期时间，并且过期后，调用haskey返回false，表示没有该key
         *       给过期的key设置值且不设置过期时间不会刷新过期时间，也不会重置该key为永久保存
         */
    }

    @Test
    void listTest() throws InterruptedException {
        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        integers.add(3);
        redisUtil.lSet(KEY, integers, 3);
        TimeUnit.SECONDS.sleep(4);
        redisUtil.lRemove(KEY, 1, 1);
        log.info("list过期获取: {}", redisUtil.lGetAll(KEY));
        log.info("list过期后删除后: {}", redisUtil.lGet(KEY, 0, -1));
        log.info("过期后删除查看是否存在: {}", redisUtil.hasKey(KEY));
        redisUtil.lSet(KEY, 1, 1000);
        log.info("过期后添加值后，查看是否存在:{}, 查看是否有值:{}", redisUtil.hasKey(KEY), redisUtil.lGetAll(KEY));
        log.info("过期后重新添加并设置过期时间后查看过期时间: {}", redisUtil.getExpire(KEY));
        redisUtil.lSet(KEY, 2, 2000);
        log.info("如果未过期，添加元素且设置过期时间: {}, {}", redisUtil.lGetAll(KEY), redisUtil.getExpire(KEY));
        redisUtil.lSet(KEY, 3);
        TimeUnit.SECONDS.sleep(2);
        log.info("如果未过期，添加元素且不设置过期时间: {}, {}", redisUtil.lGetAll(KEY), redisUtil.getExpire(KEY));
        redisUtil.del(KEY);
    }

    @Test
    void hashMapTest() throws InterruptedException {
        redisUtil.hSet(KEY, "1", 1, 2);
        log.info("当前该键过期时间: {}，该map的是否存在: {}", redisUtil.getExpire(KEY), redisUtil.hHasKey(KEY, "1"));
        TimeUnit.SECONDS.sleep(2);
        redisUtil.hSet(KEY, "2", 2, 5);
        log.info("当前该键过期时间: {}，该map的是否存在: {}", redisUtil.getExpire(KEY), redisUtil.hHasKey(KEY, "1"));
        TimeUnit.SECONDS.sleep(5);
        log.info("当前该键过期时间: {}，该map的是否存在: {}", redisUtil.getExpire(KEY), redisUtil.hHasKey(KEY, "1"));
    }

    @Test
    void hashMapTest2(){
        redisUtil.hSet(KEY, "1", 1);
        redisUtil.hSet(KEY, "2", 2);
        Map hmget = redisUtil.hmget(KEY);
        System.out.println(hmget);
        redisUtil.del(KEY);
    }

    @Test
    void testRedis1(){
        ArrayList<Answer> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, new Answer().setOption("AB"));
        boolean listTest3 = redisUtil.setIfAbsent("listTest", arrayList, 20);
        Collections.addAll(arrayList, new Answer().setOption("AC"));
        boolean listTest1 = redisUtil.setIfAbsent("listTest", arrayList, 20);
        Object listTest = redisUtil.get("listTest");
        long listTest2 = redisUtil.getExpire("listTest");
        log.info("过期时间 :{}", listTest2);
        log.info("是否插入成功: {}", listTest3);
        log.info("是否更新成功: {}", listTest1);
        log.info("redis取出{}", listTest);
        List<Answer> answers = FastJsonUtils.toList(listTest, Answer.class);
        answers.forEach(System.out::println);
        redisUtil.del("listTest");
    }

}
