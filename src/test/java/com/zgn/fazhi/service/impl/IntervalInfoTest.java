package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.IntervalInfo;
import com.zgn.fazhi.pojo.po.VisitContent;
import com.zgn.fazhi.service.IntervalInfoService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
public class IntervalInfoTest {

    @Autowired
    IntervalInfoService intervalInfoService;
    /**
     * 测试预约接口
     */
    @Test
    void getAllVisitContent(){
        log.info(intervalInfoService.getAllInterval().toString());
        // 添加参观地点
        IntervalInfo visitContent = new IntervalInfo();
        visitContent.setInfo("6666");
        intervalInfoService.addIntervalInfo(visitContent);
        log.info("添加后");
        log.info(intervalInfoService.getAllInterval().toString());
    }

    @Test
    void update(){
        IntervalInfo visitContent = new IntervalInfo();
        visitContent.setId(9);
        visitContent.setInfo("7777");
        intervalInfoService.updateIntervalInfo(visitContent);
        log.info("更新成功:{}", visitContent);
        intervalInfoService.deleteIntervalInfoById(9);
        log.info("删除成功后: {}", intervalInfoService.getAllInterval().toString());
    }

    @Test
    void refresh(){
        intervalInfoService.refreshRedis();
        log.info(intervalInfoService.getAllInterval().toString());
    };
}
