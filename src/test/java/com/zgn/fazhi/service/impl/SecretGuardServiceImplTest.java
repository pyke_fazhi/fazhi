package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.service.SecretGuardService;
import com.zgn.fazhi.service.UserInfoService;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class SecretGuardServiceImplTest {

    @Autowired
    private UserInfoService userInfoService;

    @Test
    void test1(){
        String admin = userInfoService.getQuestion("admin");
        log.info("admin密保: {}", admin);
    }

}