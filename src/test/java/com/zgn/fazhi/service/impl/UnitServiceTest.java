package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.Unit;
import com.zgn.fazhi.service.UnitService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
public class UnitServiceTest {

    @Autowired
    private UnitService unitService;

    @Test
    void getUnit(){
        unitService.list().forEach(System.out::println);
    }

    @Test
    void addUnit() {
        Unit unit = new Unit();
        unit.setTitle("第二单元");
        unitService.addUnit(unit);
        unit.setTitle("第三单元");
        unitService.addUnit(unit);
        unit.setTitle("第四单元");
        unitService.addUnit(unit);
        unit.setTitle("第五单元");
        unitService.addUnit(unit);

    }


}
