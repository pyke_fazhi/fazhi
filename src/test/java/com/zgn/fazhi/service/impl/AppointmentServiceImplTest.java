package com.zgn.fazhi.service.impl;

import com.alibaba.fastjson.JSON;
import com.zgn.fazhi.pojo.po.Appointment;
import com.zgn.fazhi.pojo.vo.AppointmentInfo;
import com.zgn.fazhi.service.AppointmentService;
import com.zgn.fazhi.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;


@SpringBootTest
@Slf4j
class AppointmentServiceImplTest {

    @Autowired
    private AppointmentService appointmentService;

    @Test
    void getAppointmentByDate() {
        ArrayList<Map<String, Object>> appointmentByDate = appointmentService.getAppointmentByDate("2021-08-11");
        System.out.println(JSON.toJSON(appointmentByDate));
    }

    @Test
    void getAppointmentByDateAndIid() {
    }

    @Test
    void insertAppointment() throws ParseException {
        Appointment appointment = new Appointment();
        appointment.setUid(35)
                .setIntervalId(1)
                .setTeamNumber(40)
                .setPhone("13554721619")
                .setGoDate(DateUtils.toDate("2021-08-12"));
        AppointmentInfo appointmentInfo = new AppointmentInfo();
        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(1);
        appointmentInfo.setInfo(appointment)
                .setVisitList(integers);
        appointmentService.insertAppointment(appointmentInfo);
    }

    @Test
    void getAllApp() {
        appointmentService.getAllApp().forEach(System.out::println);
    }

    @Test
    void delAppById() {
        appointmentService.delAppById(5);
        getAllApp();
    }

    @Test
    void isFull() {
    }

    @Test
    void findAllByUid() {
        appointmentService.findAllByUid(1).forEach(System.out::println);
    }
}