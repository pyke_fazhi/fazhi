package com.zgn.fazhi.service.impl;

import com.alibaba.fastjson.JSON;
import com.zgn.fazhi.pojo.po.ExaminationRecord;
import com.zgn.fazhi.pojo.po.ExaminationtTypeOption;
import com.zgn.fazhi.pojo.po.RecordExa;
import com.zgn.fazhi.pojo.vo.AddExamination;
import com.zgn.fazhi.pojo.vo.Answer;
import com.zgn.fazhi.pojo.vo.SubmitExamination;
import com.zgn.fazhi.pojo.vo.UserExaGrades;
import com.zgn.fazhi.service.ExaminationRecordService;
import com.zgn.fazhi.service.GeneralExaminationService;
import com.zgn.fazhi.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class GeneralExaminationServiceImplTest {

    @Autowired
    private GeneralExaminationService generalExaminationService;

    @Autowired
    private ExaminationRecordService examinationRecordService;

    /**
     * 自动抽题
     */
    @Test
    void addAutoExamination() {
        // 组题会首先将 手动组题的题目装上，再从自动组题配置中组题
        AddExamination addExamination = new AddExamination();
        // 添加手动组题id集合
        ArrayList<Integer> eidList = new ArrayList<>();
        //
        Collections.addAll(eidList, 381);
        addExamination.setExaminationIdList(eidList);

        // 添加自动组题配置
        List<ExaminationtTypeOption> examinationtTypeOptions = new ArrayList<>();
        ExaminationtTypeOption option = new ExaminationtTypeOption();
        // 添加1多选 从第一单元 第一章节
        option.setNumber(2)
                .setType(2)
                .setUnitId(2)
                .setChapter(3);
        examinationtTypeOptions.add(option);
        // 3判断
        ExaminationtTypeOption option1 = new ExaminationtTypeOption();
        option1.setNumber(2)
                .setType(3)
                .setUnitId(2);
        examinationtTypeOptions.add(option1);

        // 从当前用户私有题库抽题， 去掉单元id
        ExaminationtTypeOption option2 = new ExaminationtTypeOption();
        option2.setNumber(1)
                .setType(3);
        examinationtTypeOptions.add(option2);

        addExamination.setAutoConfigList(examinationtTypeOptions);
        addExamination.setRId(4);

        generalExaminationService.addAutoExamination(addExamination, 36);

    }

    /**
     * 用户获取考试练习的题目
     */
    @Test
    void getAllExaminationByUid() {
        List<RecordExa> allExaminationByRid = generalExaminationService.findAllExaminationByRid(4, 34);
        allExaminationByRid.forEach(recordExa -> {
            System.out.println(JSON.toJSON(recordExa));
        });
    }

    /**
     * 删除所有缓存
     */
    @Test
    void refreshCache() {
        generalExaminationService.refreshCache();
    }

    /**
     * 用户交卷
     */
    @Test
    void submitExamination() throws ParseException {
        LinkedList<Answer> answers = new LinkedList<>();
        Answer answer = new Answer().setPid(381).setOption("AB");
        Answer answer1 = new Answer().setPid(384).setOption("AB");
        Answer answer2 = new Answer().setPid(386).setOption("AB");
        Answer answer3 = new Answer().setPid(382).setOption("F");
        Answer answer4 = new Answer().setPid(395).setOption("F");
        Answer answer5 = new Answer().setPid(403).setOption("F");
        Collections.addAll(answers, answer, answer1, answer2, answer3, answer4, answer5);
        UserExaGrades userExaGrades = examinationRecordService.submitExamination(
                new SubmitExamination()
                        .setUid(34)
                        .setStart(DateUtils.toDateHms("2021-08-10 12:04:11"))
                        .setEnd(DateUtils.toDateHms("2021-08-10 12:05:11"))
                        .setRecordId(4)
                        .setAnswer(answers)
        );
        System.out.println(JSON.toJSON(userExaGrades));
    }

}