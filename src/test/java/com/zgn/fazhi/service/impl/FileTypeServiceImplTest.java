package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.FileType;
import com.zgn.fazhi.service.FileTypeService;
import com.zgn.fazhi.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class FileTypeServiceImplTest {

    @Autowired
    private FileTypeService fileTypeService;

    FileType fileType = new FileType();

    @Test
    void getAllFileType() {
        log.info("当前分类: {}", fileTypeService.getAllFileType());
    }

    @Test
    void getAllFileTypeById() {
    }

    @Test
    void addFileType() {
        fileType.setInfo("666");
        fileTypeService.addFileType(fileType);
        log.info("添加分类后:{}", fileTypeService.getAllFileType());
    }

    @Test
    void deleteFileTypeById() {

        fileTypeService.deleteFileTypeById(1);
        log.info("删除分类后: {}", fileTypeService.getAllFileType());
    }

    @Test
    void updateFileType() {
        fileType.setId(1);
        fileType.setInfo("777");
        fileTypeService.updateFileType(fileType);
        log.info("分类修改后: {}", fileTypeService.getAllFileType());
    }
}