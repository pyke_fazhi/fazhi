package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.exception.RestException;
import com.zgn.fazhi.pojo.po.Evaluate;
import com.zgn.fazhi.service.EvaluateService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class EvaluateServiceImplTest {

    @Autowired
    private EvaluateService evaluateService;

    @Test
    void getAll() {
        log.info("所有评价记录: {}", evaluateService.getAll());
    }

    @Test
    void addEvaluate() {

        Evaluate evaluate = new Evaluate();
        evaluate.setSatisfied(1);
        try {
            evaluateService.addEvaluate(evaluate, 1);
        } catch (RestException e){
            log.info("已评价，抛出自定义异常终止程序");
        }
        getEvaluate();
    }

    @Test
    void getEvaluate() {

        try {
            log.info("当前用户是否评价: {}", evaluateService.getEvaluate(1));
        } catch (RestException e){
            log.info("已经评价，终止该程序");
        }

    }
}