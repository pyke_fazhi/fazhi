package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.EtypeScore;
import com.zgn.fazhi.service.EtypeScoreService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

/**
 * 考试练习题目题型分数测试
 */

@SpringBootTest
@Slf4j
public class EtypeScoreTest {

    @Autowired
    private EtypeScoreService etypeScoreService;

    @Test
    void getAllEtypeScore(){
        etypeScoreService.list().forEach(System.out::println);
    }

    @Test
    void getEtypeScoreByRid(){
        log.info("当前考试练习题型配置:");
        etypeScoreService.findAllById(4).forEach(System.out::println);
    }

    /**
     * 增加配置
     */
    @Test
    void addEtypeScoreByRid(){
        EtypeScore etypeScore = new EtypeScore();
        etypeScore.setEid(4)
                .setScore(5)
                .setTid(2)
                .setNumber(3);
        etypeScoreService.save(etypeScore);
        etypeScore.setTid(3);
        etypeScoreService.save(etypeScore);
        getEtypeScoreByRid();
    }

    @Test
    void delEtypeScoreByRid(){
        etypeScoreService.delByEid(4);
        getEtypeScoreByRid();
    }

}
