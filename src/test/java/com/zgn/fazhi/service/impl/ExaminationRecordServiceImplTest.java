package com.zgn.fazhi.service.impl;

import com.alibaba.fastjson.JSON;
import com.zgn.fazhi.pojo.po.*;
import com.zgn.fazhi.pojo.vo.AddRecord;
import com.zgn.fazhi.pojo.vo.ExaPermission;
import com.zgn.fazhi.pojo.vo.ExaRecordMap;
import com.zgn.fazhi.pojo.vo.OpenRestriction;
import com.zgn.fazhi.service.*;
import com.zgn.fazhi.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;

@SpringBootTest
@Slf4j
class ExaminationRecordServiceImplTest {

    @Autowired
    private ExaminationRecordService examinationRecordService;

    @Autowired
    private ExaminationPermissionService examinationPermissionService;

    @Autowired
    private SchoolService schoolService;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private ClassInfoService classInfoService;

    @Autowired
    private EtypeScoreService etypeScoreService;


    /**
     * 开始一个公开和一个未公开的练习
     */
    @Test
    void addRecord() throws ParseException {
        AddRecord record = new AddRecord();
        ExaminationRecord examinationRecord = new ExaminationRecord();
        // 开启一次公开的练习
        examinationRecord.setStart(DateUtils.toDate("2021-08-07"))
                .setEnd(DateUtils.toDate("2021-08-20"))
                .setLengthTime(1000)
                .setTimes(1)
                .setTitle("测试1")
                .setUid(1)
                .setIfpractice(1)
                .setIfPublic(1);
        record.setExaminationRecord(examinationRecord);
        ArrayList<EtypeScore> etypeScores = new ArrayList<>();
        EtypeScore etypeScore = new EtypeScore();
        // 题目题型分数配置
        etypeScore.setTid(1)
                .setNumber(1)
                .setScore(5);
        etypeScores.add(etypeScore);
        record.setEtypeScore(etypeScores);
        // 插入
        examinationRecordService.addExaminationRecord(record);

        // 题目题型分数配置
        etypeScores.clear();
        etypeScore.setTid(2)
                .setNumber(2)
                .setScore(5);
        etypeScores.add(etypeScore);
        record.setEtypeScore(etypeScores);
        // 插入一条不公开的练习
        examinationRecord.setTitle("测试2");
        examinationRecord.setIfPublic(0);
        record.setExaminationRecord(examinationRecord);
        examinationRecordService.addExaminationRecord(record);
    }

    @Test
    void getAllRecord() {
        // 4 公开 5 不公开
        examinationRecordService.list(null).forEach(System.out::println);
    }

    /**
     * 查看考试练习的配置
     */
    @Test
    void getEtypeScore() {
        etypeScoreService.list(null).forEach(System.out::println);
    }

    @Test
    void delRecord() {
        examinationRecordService.delRecord(2);
        examinationRecordService.delRecord(3);
    }

    /**
     * 为一个未公开的练习添加开放人群限制
     */
    @Test
    void addPermission() {
        // 为一个未公开的练习添加开放人群限制
        // 开放到学校范围的
        ExaPermission exaPermission = new ExaPermission();
        ArrayList<OpenRestriction> openRestrictions = new ArrayList<>();
        OpenRestriction openRestriction = new OpenRestriction();
        openRestriction.setSchoolId(1);
        openRestrictions.add(openRestriction);
        exaPermission
                .setRecordId(5)
                .setPermission(openRestrictions);
        examinationPermissionService.addExaPermission(exaPermission);
    }

    /**
     * 查看记录的开放人群
     */
    @Test
    void getRecordPermission() {
        // 开放学校id 4
//        examinationPermissionService.getExaPermissionByRid(5).forEach(System.out::println);
    }

    /**
     * 创建两个学校测试
     */
    @Test
    void addSchool() {
        // 创建两个学校学校
        School school = new School();
        school.setSchoolName("testSchool1");
        schoolService.addSchool(school);

        school.setSchoolName("testSchool2");
        schoolService.addSchool(school);
    }

    /**
     * 查看所有学校
     */
    @Test
    void getSchool() {
        schoolService.getAllSchool().forEach(System.out::println);
    }

    /**
     * 为两个学校创建班级
     */
    @Test
    void addClass() {
        // 为两个学校创建班级
        ClassInfo classInfo = new ClassInfo();
        classInfo.setSchoolId(4)
                .setGradeId(1)
                .setName("一年级一班");
        classInfoService.addClassInfo(classInfo);

        classInfo.setSchoolId(5)
                .setGradeId(7)
                .setName("初一一班");
        classInfoService.addClassInfo(classInfo);
    }


    /**
     * 创建三个测试用户
     */
    @Test
    void addUser() {
        // 创建三个用户，一个是该开放范围，一个非开放范围的学生
        // 还有一个为教师的用户
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("username1")
                .setPassword("123")
                .setName("name1")
                .setRoleId(7)
                .setSchoolId(3)
                .setGradeId(1)
                .setClassId(1)
                .setEducationId(1);
        userInfoService.addUser(userInfo);
        userInfo.setUsername("username2")
                .setName("name2")
                .setSchoolId(4)
                .setGradeId(7)
                .setClassId(2);
        userInfoService.addUser(userInfo);
        // 创建一个教师用户
        UserInfo teacher = new UserInfo();
        teacher.setUsername("teacher1")
                .setName("tname2")
                .setPassword("123")
                .setRoleId(5);
        userInfoService.addUser(teacher);
    }

    /**
     * 查询所有用户
     */
    @Test
    void getAllUser() {
        userInfoService.list().forEach(System.out::println);
    }

    /**
     * 批量删除用户
     */
    @Test
    void delUserByIds() {
        ArrayList<Integer> integers = new ArrayList<>();
        Collections.addAll(integers, 31, 32, 33);
        userInfoService.batchDelete(integers);
        getAllUser();
    }

    /**
     * 使用三个用户分别查询当前开放的练习
     */
    @Test
    void selectExercise() {
        // 用户1 为该练习的开放人群
        ExaRecordMap examinationByUid = examinationRecordService.getExaminationByUid(userInfoService.getById(34), 1);
        ExaRecordMap examinationByUid1 = examinationRecordService.getExaminationByUid(userInfoService.getById(35), 1);
        ExaRecordMap examinationByUid2 = examinationRecordService.getExaminationByUid(userInfoService.getById(36), 1);
        log.info("处于开放人群内的视角: {}", JSON.toJSON(examinationByUid));
        log.info("不处于开放人群内的用户视角: {}", JSON.toJSON(examinationByUid1));
        log.info("不是学生的视角: {}", JSON.toJSON(examinationByUid2));
    }

    /**
     * 刷新缓存
     */
    @Test
    void refresh(){
        examinationRecordService.refreshCache();
    }



}