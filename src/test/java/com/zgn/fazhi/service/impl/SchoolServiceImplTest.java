package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.School;
import com.zgn.fazhi.service.SchoolService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class SchoolServiceImplTest {

    @Autowired
    private SchoolService schoolService;

    School school = new School();

    @Test
    void getAllSchool() {
        logInfo("过期后");
    }

    @Test
    void findByName() {
    }

    @Test
    void delById() {
        schoolService.delById(2);
        logInfo("删除");
    }

    @Test
    void addSchool() {
        school.setSchoolName("addTest");
        schoolService.addSchool(school);
        logInfo("添加");
    }

    @Test
    void updateSchoolById() {
        school.setSchoolName("updateTest");
        school.setId(2);
        schoolService.updateSchoolById(school);
        logInfo("修改");
    }

    @Test
    void refreshCache() {
        schoolService.refreshCache();
        logInfo("刷新缓存");
    }

    private void logInfo(String info){
        log.info("{}后:{}", info, schoolService.getAllSchool());
    }

}