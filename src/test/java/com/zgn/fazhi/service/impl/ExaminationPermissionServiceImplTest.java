package com.zgn.fazhi.service.impl;

import com.alibaba.fastjson.JSON;
import com.zgn.fazhi.pojo.vo.ExaPermissionVoList;
import com.zgn.fazhi.service.ExaminationPermissionService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class ExaminationPermissionServiceImplTest {

    @Autowired
    private ExaminationPermissionService examinationPermissionService;

    @Test
    void getExaPermissionByRid() {
        ExaPermissionVoList exaPermissionByRid = examinationPermissionService.getExaPermissionByRid(5);
        System.out.println(JSON.toJSON(exaPermissionByRid.getSchoolList()));
        System.out.println(JSON.toJSON(exaPermissionByRid.getGradeList()));
        System.out.println(JSON.toJSON(exaPermissionByRid.getClassList()));
    }
}