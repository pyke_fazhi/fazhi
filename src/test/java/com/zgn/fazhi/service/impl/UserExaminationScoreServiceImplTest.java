package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.vo.UserExaGrade;
import com.zgn.fazhi.service.UserExaminationScoreService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class UserExaminationScoreServiceImplTest {

    @Autowired
    private UserExaminationScoreService userExaminationScoreService;

    /**
     * 获取个人考试成绩
     */
    @Test
    void getCurrentExaDateByUid() {
        List<UserExaGrade> currentExaDateByUid = userExaminationScoreService.getCurrentExaDateByUid(1, 4);
        currentExaDateByUid.forEach(System.out::println);
    }

    @Test
    void getCurrentExaDateListByUid(){
        userExaminationScoreService.getCurrentExaDateListByUid(1).forEach(System.out::println);
    }
}