package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.PictureType;
import com.zgn.fazhi.service.PictureTypeService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class PictureTypeServiceImplTest {

    @Autowired
    private PictureTypeService pictureTypeService;

    PictureType pictureType = new PictureType();

    @Test
    void getAllPictureType() {

        log.info("当前分类: {}", pictureTypeService.getAllPictureType());

    }

    @Test
    void getAllPictureTypeById() {
    }

    @Test
    void addPictureType() {

        pictureType.setName("666");
        pictureTypeService.addPictureType(pictureType);
        logInfo("添加");

    }

    @Test
    void deletePictureTypeById() {

        pictureTypeService.deletePictureTypeById(1);
        logInfo("删除");

    }

    @Test
    void updatePictureType() {

        pictureType.setName("777");
        pictureType.setId(1);
        pictureTypeService.updatePictureType(pictureType);
        logInfo("更新");

    }

    @Test
    void refreshCache(){
        pictureTypeService.refreshCache();
        logInfo("刷新缓存");
    }

    private void logInfo(String info){
        log.info("{}后: {}", info, pictureTypeService.getAllPictureType());
    }

}