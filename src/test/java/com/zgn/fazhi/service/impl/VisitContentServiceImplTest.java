package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.VisitContent;
import com.zgn.fazhi.service.VisitContentService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class VisitContentServiceImplTest {

    @Autowired
    private VisitContentService visitContentService;

    /**
     * 测试预约接口
     */
    @Test
    void getAllVisitContent(){
        log.info(visitContentService.getAllVisitContent().toString());
        // 添加参观地点
        VisitContent visitContent = new VisitContent();
        visitContent.setName("6666");
        visitContentService.addVisitContent(visitContent);
        log.info("添加后");
        log.info(visitContentService.getAllVisitContent().toString());
    }

    @Test
    void get(){
        visitContentService.getAllVisitContent();
        log.info("过期时间: {} ", visitContentService.checkExpire());
    }

    @Test
    void update(){
        VisitContent visitContent = new VisitContent();
        visitContent.setId(11);
        visitContent.setName("7777");
        visitContentService.updateVisitContent(visitContent);
        log.info("更新成功:{}", visitContent);
        visitContentService.deleteVisitContentById(11);
        log.info("删除成功后: {}", visitContentService.getAllVisitContent().toString());
    }

    @Test
    void refresh(){
        visitContentService.refreshRedis();
        log.info(visitContentService.getAllVisitContent().toString());
    };

}