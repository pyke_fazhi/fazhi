package com.zgn.fazhi.service.impl;

import com.zgn.fazhi.pojo.po.Education;
import com.zgn.fazhi.service.EducationService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class EducationServiceImplTest {

    @Autowired
    private EducationService educationService;

    @Test
    void getAllEducation() {
        log.info("getAllEducation: {}", educationService.getAllEducation());
    }

    @Test
    void addEducation() {
        Education education = new Education();
        education.setName("test");
        educationService.addEducation(education);
        getAllEducation();
    }

    @Test
    void updateEducationById() {
        Education education = new Education();
        education.setName("updateTest");
        education.setId(4);
        educationService.updateEducationById(education);
        log.info("更新后: {}", educationService.getAllEducation());
    }

    @Test
    void delEducation() {
        educationService.delEducation(5);
        log.info("删除后: {}", educationService.getAllEducation());
    }
}