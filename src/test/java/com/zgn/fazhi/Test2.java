package com.zgn.fazhi;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.zgn.fazhi.mapper.ExaminationMapper;
import com.zgn.fazhi.mapper.LiveInfoMapper;
import com.zgn.fazhi.pojo.po.Chapter;
import com.zgn.fazhi.pojo.po.Examination;
import com.zgn.fazhi.pojo.po.ExaminationtTypeOption;
import com.zgn.fazhi.pojo.po.UserInfo;
import com.zgn.fazhi.pojo.vo.UserLiveInfo;
import com.zgn.fazhi.service.AliYunLiveService;
import com.zgn.fazhi.service.ChapterService;
import com.zgn.fazhi.service.ExaminationService;
import com.zgn.fazhi.utils.RedisUtil;
import com.zgn.fazhi.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.ByteArrayInputStream;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class Test2 {

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private RedisUtil redisUtil;

    @Test
    public void test1() {
        Chapter chapter = new Chapter();
        chapter.setTitle("test");
        chapterService.save(chapter);
    }

    @Test
    public void test2() {
        List<UserInfo> userInfos = new ArrayList<>();
        UserInfo user1 = new UserInfo();
        user1.setRoleId(1);
        UserInfo user2 = new UserInfo();
        user2.setRoleId(3);
        UserInfo user3 = new UserInfo();
        user3.setRoleId(4);
        UserInfo user4 = new UserInfo();
        user4.setRoleId(3);
        UserInfo user5 = new UserInfo();
        user5.setRoleId(4);

        userInfos.add(user1);
        userInfos.add(user2);
        userInfos.add(user3);
        userInfos.add(user4);
        userInfos.add(user5);

        List<Integer> collect = userInfos.stream().map(UserInfo::getRoleId).collect(Collectors.toList())
                .stream().distinct().collect(Collectors.toList());

        collect.forEach(System.out::println);

        List<Integer> integers = new ArrayList<>();
        Collections.addAll(integers, 1, 2, 3, 5, 6, 7);
        integers.forEach(System.out::println);
        boolean contains = integers.containsAll(collect);
        System.out.println(contains);
    }

    @Test
    public void test3() {
        List<Integer> integers = new ArrayList<>();
        Collections.addAll(integers, null, null);
        boolean empty = integers.isEmpty();
        System.out.println(empty);
    }

    @Test
    public void test4() {
        List<UserInfo> userInfos = new ArrayList<>();
        UserInfo userInfo = new UserInfo();
        userInfo.setRoleId(1)
                .setSchoolId(1);
        userInfos.add(userInfo);
        UserInfo userInfo2 = new UserInfo();
        userInfo2.setRoleId(1)
                .setSchoolId(3);
        userInfos.add(userInfo2);
        UserInfo userInfo3 = new UserInfo();
        userInfo3.setRoleId(2)
                .setSchoolId(3);
        userInfos.add(userInfo3);
        Map<Integer, Integer> collect2 = userInfos.stream().collect(Collectors.groupingBy(UserInfo::getRoleId, Collectors.summingInt(UserInfo::getSchoolId)));
        System.out.println(collect2);
    }

    @Test
    public void test5() {
//        redisUtil.hSet("test1", "exa:1:2:1")
        List<Integer> integers = new ArrayList<>();
        Collections.addAll(integers, 1, 2, 3, 4, 5, 6, 7, 8);
        System.out.println(integers);
        Collections.shuffle(integers);
        List<Integer> list = integers.subList(0, 3);
        System.out.println(list);
        System.out.println(integers);

    }

    @Test
    public void test6() {
        redisUtil.hSet("test1", "test2", 1);
        Integer i = (Integer) redisUtil.hget("test1", "test2");
        System.out.println(i);
        redisUtil.hSet("test1", "test2", 2);
        Integer i2 = (Integer) redisUtil.hget("test1", "test2");
        System.out.println(i2);
    }

    @Autowired
    private ExaminationMapper examinationMapper;

    @Autowired
    private ExaminationService examinationService;

    @Test
    public void test7() {
//        List<Integer> randomList = examinationMapper.getRandomList2(4);
//        randomList.forEach(System.out::println);
    }

    @Test
    public void test8() {
        List<Examination> examinations = examinationService.selectAllPaper();
        Collections.shuffle(examinations);
        examinations.subList(0, 10).forEach(System.out::println);
    }

    @Test
    public void test9() {
        List<Integer> integers = new ArrayList<>();
        Collections.addAll(integers, 1, 2, 3, 4, 5, 6, 7, 8, null);
        List<Integer> integers2 = new ArrayList<>();
        Collections.addAll(integers2, 1, 2, null);
        System.out.println(integers.containsAll(integers2));
    }


    @Test
    public void test10() {
        ArrayList<Integer> integers = new ArrayList<>();
        Collections.addAll(integers, 100, 150, 180);
        List<Examination> examinations = examinationService.listByIds(integers);
        examinations.forEach(System.out::println);
    }

    @Test
    public void test11() {
        Map<Integer, Integer> map = null;
        Map<Integer, Integer> map1 = new HashMap<>();
        map = map1;
        System.out.println(map);
        System.out.println(map.isEmpty());
    }

    @Test
    public void test12() {
        Examination examination = new Examination();
        examination.setAnswer("A");
        redisUtil.hSet("test", "exa1", examination);
        examination.setAnswer("B");
        redisUtil.hSet("test", "exa2", examination);
        Map<String, Examination> test = redisUtil.hmget("test");
//        test.keySet().forEach(id -> {
//            System.out.println(id + " : " + test.get(id));
//        });
        System.out.println(test);
    }

    @Test
    public void test13() {
        String key = "testincr";
        redisUtil.set(key, 1);
        redisUtil.incr(key, 1);
        System.out.println(redisUtil.get(key));
    }

    // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
    String endpoint = "yourEndpoint";
    // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    String accessKeyId = "yourAccessKeyId";
    String accessKeySecret = "yourAccessKeySecret";

    @Test
    public void test14() {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 填写字符串。
        String content = "Hello OSS";

        // 创建PutObjectRequest对象。
        // 依次填写Bucket名称（例如examplebucket）和Object完整路径（例如exampledir/exampleobject.txt）。Object完整路径中不能包含Bucket名称。
        PutObjectRequest putObjectRequest = new PutObjectRequest("examplebucket", "exampledir/exampleobject.txt", new ByteArrayInputStream(content.getBytes()));

        // 如果需要上传时设置存储类型和访问权限，请参考以下示例代码。
        // ObjectMetadata metadata = new ObjectMetadata();
        // metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
        // metadata.setObjectAcl(CannedAccessControlList.Private);
        // putObjectRequest.setMetadata(metadata);

        // 上传字符串。
        ossClient.putObject(putObjectRequest);

        // 关闭OSSClient。
        ossClient.shutdown();

    }

    @Autowired
    private StringUtils stringUtils;

    @Test
    public void test15() {

        System.out.println(stringUtils.getRandomUsername());

    }

    @Autowired
    private LiveInfoMapper liveInfoMapper;

    @Autowired
    private AliYunLiveService aliYunLiveService;

    @Test
    public void test16(){
        List<UserLiveInfo> allLiveIfOn = aliYunLiveService.getAllLiveIfOn();
        allLiveIfOn.forEach(System.out::println);
    }
}
